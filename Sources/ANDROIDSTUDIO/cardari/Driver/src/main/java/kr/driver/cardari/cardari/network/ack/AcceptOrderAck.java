package kr.driver.cardari.cardari.network.ack;

import kr.driver.cardari.cardari.common.Enums;

import static kr.driver.cardari.cardari.common.Enums.DriverState;

/**
 * Created by eklee on 2017. 11. 1..
 */

public class AcceptOrderAck extends BaseAck {
    private @DriverState int driverState;
    private @Enums.CallState String callState;

    public AcceptOrderAck() {
    }

    public AcceptOrderAck(int driverState, String callState) {
        this.driverState = driverState;
        this.callState = callState;
    }

    public AcceptOrderAck(NetResultData result, int driverState, String callState) {
        super(result);
        this.driverState = driverState;
        this.callState = callState;
    }

    public int getDriverState() {
        return driverState;
    }

    public void setDriverState(int driverState) {
        this.driverState = driverState;
    }

    public String getCallState() {
        return callState;
    }

    public void setCallState(String callState) {
        this.callState = callState;
    }
}
