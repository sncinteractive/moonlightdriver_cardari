package kr.driver.cardari.cardari.common;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.maps.model.LatLng;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import kr.driver.cardari.cardari.R;

import static kr.driver.cardari.cardari.common.Enums.DistanceUnit;

/**
 * Created by eklee on 2017. 6. 14..
 */

public class Util {
    public static boolean isStringNullOrEmpty(String string) {
        if (string == null) {
            return true;
        }

        return string.isEmpty();
    }

    // region - convert distance to string
    /**
     * 미터 단위 거리를 킬로미터 단위 문자열로 변환. 10미터 단위값은 버림
     *
     * @param distance (int)
     * @param useMeterIfLessThanOneKilometer (boolean) 1000미터 보다 작을때 미터 단위로 표시할것인지. ex)distance 900, return 900m, distance 1500, return 1.5km
     * @return string. ex)0.5km, 1km, 1.3km, 123.8km
     */
    public static final String DISTANCE_UNIT_KILOMETER = "km";
    public static final String DISTANCE_UNIT_METER = "m";

    public static String convertDistanceString(int distance) {
        return convertDistanceString(distance, false);
    }

    public static String convertDistanceString(int distance, boolean useMeterIfLessThanOneKilometer) {
        int quotient = distance / 1000;
        int remainder = distance % 1000;

        StringBuilder sb = new StringBuilder();
        if (quotient == 0 && useMeterIfLessThanOneKilometer) {
            sb.append(remainder).append(DISTANCE_UNIT_METER);
        } else {
            sb.append(quotient);
            if (remainder > 0) {
                int remainder2 = remainder / 100;
                if (remainder2 > 0) {
                    sb.append(".").append(remainder2);
                } else {
                    sb.append(".1");
                }
            }
            sb.append(DISTANCE_UNIT_KILOMETER);
        }

        return sb.toString();
    }
    // endregion - convert distance to string

    public static String convertBiddingEndTimeToString(Context context, long endTime) {
        String result = null;
        long remainTime = endTime - System.currentTimeMillis();
        if (remainTime >= 1000) {
            long totalSeconds = remainTime / 1000;
            long minute = totalSeconds / 60;
        } else {
            result = context.getString(R.string.bidding_time_over);
        }

        return result;
    }

    // region - convert time to string
    private static final String HOUR_POSTFIX = "h";
    private static final String MINUTE_POSTFIX = "m";
    private static final String SECOND_POSTFIX = "s";

    private static final String HOUR_POSTFIX2 = "시간";
    private static final String MINUTE_POSTFIX2 = "분";
    private static final String SECOND_POSTFIX2 = "초";

    public static String convertTimeToString(int timeSeconds) {
        return convertTimeToString(timeSeconds, false);
    }

    public static String convertTimeToString(int timeSeconds, boolean floorSeconds) {
        int hour = timeSeconds / 3600;
        int minute = timeSeconds % 3600 / 60;
        int second = timeSeconds % 3600 % 60;

        StringBuilder sb = new StringBuilder();
        if (hour > 0) {
            sb.append(hour).append(HOUR_POSTFIX);
        }

        if (minute > 0) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(minute).append(MINUTE_POSTFIX);
        }

        if (!floorSeconds && second > 0) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(second).append(SECOND_POSTFIX);
        }

        return sb.toString();
    }

    public static String convertTimeToString2(int timeSeconds) {
        return convertTimeToString2(timeSeconds, false);
    }

    public static String convertTimeToString2(int timeSeconds, boolean floorSeconds) {
        int hour = timeSeconds / 3600;
        int minute = timeSeconds % 3600 / 60;
        int second = timeSeconds % 3600 % 60;

        StringBuilder sb = new StringBuilder();
        if (hour > 0) {
            sb.append(hour).append(HOUR_POSTFIX2);
        }

        if (minute > 0) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(minute).append(MINUTE_POSTFIX2);
        }

        if (!floorSeconds && second > 0) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(second).append(SECOND_POSTFIX2);
        }

        return sb.toString();
    }
    // endregion - convert time to string

    // region - convert number to comma string
    private static final String CURRENCY_POSTFIX = " 원";

    public static String convertNumberToCommaString(int value) {
        return convertNumberToCommaString(value, false);
    }

    public static String convertNumberToCommaString(int value, boolean useCurrencyPostfix) {
        StringBuilder sb = new StringBuilder(NumberFormat.getInstance(Locale.US).format(value));
        if (useCurrencyPostfix)
            sb.append(CURRENCY_POSTFIX);
        return sb.toString();
    }
    // endregion - convert number to comma string

    private static final String THOUSAND_POSTFIX = "k";

    public static String convertPriceToThousandString(int price) {
        return Integer.toString(price / 1000) + THOUSAND_POSTFIX;
    }

    // region - convert date to string
//    private static final SimpleDateFormat sDayDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//    private static final SimpleDateFormat sMonthDateFormat = new SimpleDateFormat("yyyy-MM");
//
//    public static String convertDateToDayString(long time) {
//        return convertDateToDayString(new Date(time));
//    }
//
//    public static String convertDateToDayString(Date date) {
//        return sDayDateFormat.format(date);
//    }
//
//    public static String convertDateToMonthString(long time) {
//        return convertDateToMonthString(new Date(time));
//    }
//
//    public static String convertDateToMonthString(Date date) {
//        return sMonthDateFormat.format(date);
//    }

    private static final String DAY_FORMAT = "%d-%02d-%02d";
    private static final String MONTH_FORMAT = "%d-%02d";

    public static String convertYearMonthDayString(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);

        return convertYearMonthDayString(calendar);
    }

    public static String convertYearMonthDayString(Calendar calendar) {
        return String.format(Locale.KOREA, DAY_FORMAT, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
    }

    public static String convertYearMonthString(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);

        return convertYearMonthString(calendar);
    }

    public static String convertYearMonthString(Calendar calendar) {
        return String.format(Locale.KOREA, MONTH_FORMAT, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1);
    }

    public static String getYYYYMMDDHHmmss() {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    }
    // endregion - convert date to string

    // region - 두 경위도 좌표의 직선 거리 계산
    public static double distanceBetweenLocation(LatLng location1, LatLng location2, @DistanceUnit int unit) {
        return distanceBetweenLocation(location1.latitude, location1.longitude, location2.latitude, location2.longitude, unit);
    }

    public static double distanceBetweenLocation(double lat1, double lon1, double lat2, double lon2, @DistanceUnit int unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));

        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515; // mile

        if (unit == DistanceUnit.KILOMETER) {
            dist = dist * 1.609344;
        } else if (unit == DistanceUnit.METER) {
            dist = dist * 1609.344;
        }

        return dist;
    }

    public static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    public static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }
    // endregion - 두 경위도 좌표의 직선 거리 계산

    /**
     * Gson jsonarry to array or list
     */
    // #1
//    MyClass[] array = gson.fromJson(jsonString, MyClass[].class);
//    List<MyClass> list = Arrays.asList(array);
//
//    // #2
//    List<MyClass> list2 = gson.fromJson(jsonString, new TypeToken<List<MyClass>>(){}.getType());
    public static long getLocationUpdateTime(double _kmPerHour) {
        try {
            if (_kmPerHour >= 40) {
                return 1000;
            } else if (_kmPerHour >= 30) {
                return 1200;
            } else if (_kmPerHour >= 25) {
                return 1400;
            } else if (_kmPerHour >= 20) {
                return 1800;
            } else if (_kmPerHour >= 19) {
                return 1900;
            } else if (_kmPerHour >= 18) {
                return 2000;
            } else if (_kmPerHour >= 17) {
                return 2100;
            } else if (_kmPerHour >= 16) {
                return 2200;
            } else if (_kmPerHour >= 15) {
                return 2400;
            } else if (_kmPerHour >= 14) {
                return 2600;
            } else if (_kmPerHour >= 13) {
                return 2800;
            } else if (_kmPerHour >= 12) {
                return 3000;
            } else if (_kmPerHour >= 11) {
                return 3200;
            } else if (_kmPerHour >= 10) {
                return 3600;
            } else if (_kmPerHour >= 9) {
                return 4000;
            } else if (_kmPerHour >= 8) {
                return 4500;
            } else if (_kmPerHour >= 7) {
                return 5000;
            } else if (_kmPerHour >= 6) {
                return 6000;
            } else if (_kmPerHour >= 5) {
                return 7000;
            } else if (_kmPerHour >= 4) {
                return 9000;
            } else if (_kmPerHour >= 3) {
                return 12000;
            } else if (_kmPerHour >= 2) {
                return 18000;
            } else if (_kmPerHour >= 1) {
                return 36000;
            } else {
                return 60000;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 60000;
        }
    }

    public static int GetLocationServiceStatus(Context cx) {
        try {
            LocationManager lm = (LocationManager) cx.getSystemService(Context.LOCATION_SERVICE);
            if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER) && lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                return 3;
            } else if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                return 2;
            } else if (lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                return 1;
            } else { // 위치 찾기 불가
                return 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static void finishActivity(Activity _activity) {
        try {
            _activity.moveTaskToBack(true);
            _activity.finish();
            android.os.Process.killProcess(android.os.Process.myPid());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean GetWifiOnStatus(Context cx) {
        try {
            ConnectivityManager cm = (ConnectivityManager) cx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork != null) {
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                    return activeNetwork.isAvailable();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String url = urls[0];
            Bitmap mIcon11 = null;

            try {
                InputStream in = new java.net.URL(url).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                bmImage.setImageBitmap(result);
                this.bmImage.setVisibility(View.VISIBLE);
            }
        }
    }

    public static boolean isAvailableModify(long createdTime) {
        try {
            long nowTime = Calendar.getInstance().getTimeInMillis();
            int modifyInterval = 3 * 60 * 1000;

            if (nowTime >= (createdTime + modifyInterval)) {
                return true;
            }

            return false;
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }

    public static Bitmap getImageDownload(String _imgUrl) {
        Bitmap img = null;
        try {
            URL url = new URL(_imgUrl);
            URLConnection conn = url.openConnection();
            conn.connect();

            BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
            img = BitmapFactory.decodeStream(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return img;
    }

    public static int getNotificationId(String pushMsgType) {
        switch (pushMsgType) {
            case Defines.PUSH_MSG_TYPE_CATCH_CALL:
                return Defines.NOTIFICATION_ID_CATCH_CALL;
        }

        return 0;
    }

    public static void makePhoneCall(Context context, String phoneNumber) {
        try {
            if (checkPhoneCallPermission(context, Defines.CALL_PHONE_REQ_CODE)) {
                String uri = "tel:" + phoneNumber;

                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse(uri));

                context.startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean checkPhoneCallPermission(Context context, int _reqCode) {
        boolean isPermissionGranted = true;

        try {
            if (Build.VERSION.SDK_INT >= 23) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE}, _reqCode);
                    isPermissionGranted = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            isPermissionGranted = false;
        }

        return isPermissionGranted;
    }

    public static String convertToLocalPhoneNumber(String phone) {
        try {
            if (phone.startsWith("+82")) {
                phone = "0" + phone.substring(3);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return phone;
    }

    public static float pixelTodp(Context c, float pixel) {
        float density = c.getResources().getDisplayMetrics().density;
        float dp = pixel / density;
        return dp;
    }

    public static float dpToPixel(Context c, float dp) {
        float density = c.getResources().getDisplayMetrics().density;
        float pixel = dp * density;
        return pixel;
    }

    public static float convertDpToPixel(Context context, float dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public static float convertPixelsToDp(Context context, float px) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }
}
