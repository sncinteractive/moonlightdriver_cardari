package kr.driver.cardari.cardari.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by youngmin on 2015-10-28.
 */
public class DriverCertificateData implements Parcelable {
    private boolean automatic;
    private String number;

    public DriverCertificateData() {}

    public DriverCertificateData(boolean automatic, String number) {
        this.automatic = automatic;
        this.number = number;
    }

    protected DriverCertificateData(Parcel in) {
        automatic = in.readByte() != 0;
        number = in.readString();
    }

    public boolean isAutomatic() {
        return automatic;
    }

    public void setAutomatic(boolean automatic) {
        this.automatic = automatic;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public static final Creator<DriverCertificateData> CREATOR = new Creator<DriverCertificateData>() {
        @Override
        public DriverCertificateData createFromParcel(Parcel in) {
            return new DriverCertificateData(in);
        }

        @Override
        public DriverCertificateData[] newArray(int size) {
            return new DriverCertificateData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeByte((byte) (automatic ? 1 : 0));
        dest.writeString(number);
    }
}
