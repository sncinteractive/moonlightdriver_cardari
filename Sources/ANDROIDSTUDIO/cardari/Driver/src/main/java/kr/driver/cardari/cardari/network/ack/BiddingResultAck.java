package kr.driver.cardari.cardari.network.ack;

import kr.driver.cardari.cardari.common.Enums.BiddingResult;

import static kr.driver.cardari.cardari.common.Enums.*;

/**
 * Created by eklee on 2017. 11. 1..
 */

public class BiddingResultAck extends BaseAck {
    private @BiddingResult int biddingResult;

    // 낙찰된 경우 사용
    private int biddingPrice; // 낙찰가
    private @DriverState int driverState; // 기사 상태

    public BiddingResultAck() {
    }

    public BiddingResultAck(NetResultData result, @BiddingResult int biddingResult, int biddingPrice, @DriverState int driverState) {
        super(result);
        this.biddingResult = biddingResult;
        this.biddingPrice = biddingPrice;
        this.driverState= driverState;
    }

    public @BiddingResult
    int getBiddingResult() {
        return biddingResult;
    }

    public void setBiddingResult(@BiddingResult int biddingResult) {
        this.biddingResult = biddingResult;
    }

    public int getBiddingPrice() {
        return biddingPrice;
    }

    public void setBiddingPrice(int biddingPrice) {
        this.biddingPrice = biddingPrice;
    }

    public int getDriverState() {
        return driverState;
    }

    public void setDriverState(int driverState) {
        this.driverState = driverState;
    }
}
