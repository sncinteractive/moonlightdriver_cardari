package kr.driver.cardari.cardari.network.ack;

import static kr.driver.cardari.cardari.common.Enums.DriverState;

/**
 * Created by eklee on 2017. 11. 1..
 */

public class EndDrivingAck extends BaseAck {
    private @DriverState int driverState;

    public EndDrivingAck() {
    }

    public EndDrivingAck(NetResultData result, int driverState) {
        super(result);
        this.driverState = driverState;
    }

    public int getDriverState() {
        return driverState;
    }

    public void setDriverState(int driverState) {
        this.driverState = driverState;
    }
}
