package kr.driver.cardari.cardari.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.Enums.IncomeDateType;
import kr.driver.cardari.cardari.logger.DLog;
import kr.driver.cardari.cardari.network.ack.IncomeQueryAck;
import kr.driver.cardari.cardari.view.adapter.DailyIncomeListAdapter;

/**
 * Created by eklee on 2017. 11. 21..
 */

public class DailyIncomeFragment extends Fragment implements IncomeDetailFragment.EventListener {
    public interface EventListener {
        void needToShowLoadingProgressbar();

        void needToHideLoadingProgressbar();
    }

    private static final String TAG = "DailyIncomeFragment";

    @BindView(R.id.completion_list_recycler_view) RecyclerView mCompletionListRecyclerView;
    @BindView(R.id.penalty_list_recycler_view) RecyclerView mPenaltyListRecyclerView;

    private Unbinder mUnBinder;
    private EventListener mEventListener;

    private DailyIncomeListAdapter mDailyIncomeCompletionListAdapter;
    private DailyIncomeListAdapter mDailyIncomePenaltyListAdapter;

    public static DailyIncomeFragment createInstance() {
        return new DailyIncomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_daily_income, container, false);
        mUnBinder = ButterKnife.bind(this, rootView);

        initialize();
        initializeView();

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            if (Build.VERSION.SDK_INT >= 23) {
                mEventListener = (EventListener) context;
            }
        } catch (Exception e) {
            DLog.e(TAG, "onAttach - " + context.toString() + " not implemented event listener", e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            if (Build.VERSION.SDK_INT < 23) {
                mEventListener = (EventListener) activity;
            }
        } catch (Exception e) {
            DLog.e(TAG, "onAttach - " + activity.toString() + " not implemented event listener", e);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mEventListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnBinder.unbind();
    }

    private void initialize() {
        try {

            // 완료콜 리스트
            mDailyIncomeCompletionListAdapter = new DailyIncomeListAdapter();
            mCompletionListRecyclerView.setAdapter(mDailyIncomeCompletionListAdapter);
            LinearLayoutManager completionListLayoutManager = new LinearLayoutManager(getContext());
            completionListLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            mCompletionListRecyclerView.setLayoutManager(completionListLayoutManager);

            //패널티콜 리스트
            mDailyIncomePenaltyListAdapter = new DailyIncomeListAdapter();
            mPenaltyListRecyclerView.setAdapter(mDailyIncomePenaltyListAdapter);
            LinearLayoutManager penaltyListLayoutManager = new LinearLayoutManager(getContext());
            penaltyListLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            mPenaltyListRecyclerView.setLayoutManager(penaltyListLayoutManager);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeView() {
        // income detail fragment
        Fragment fragment = IncomeDetailFragment.createInstance(IncomeDateType.DAILY, this);
        getChildFragmentManager().beginTransaction().replace(R.id.fragment_income_detail_frame, fragment).commit();
    }

    @Override
    public void needToShowLoadingProgressbar() {
        try {
            mEventListener.needToShowLoadingProgressbar();
        } catch (Exception e) {
            DLog.e(TAG, "needToShowLoadingProgressbar exception", e);
        }
    }

    @Override
    public void needToHideLoadingProgressbar() {
        try {
            mEventListener.needToHideLoadingProgressbar();
        } catch (Exception e) {
            DLog.e(TAG, "needToHideLoadingProgressbar exception", e);
        }
    }

    @Override
    public void onReceivedIncomeQueryAck(IncomeQueryAck ack) {
        try {
            mDailyIncomeCompletionListAdapter.setDataList(ack.getCompletionList());
            mCompletionListRecyclerView.scrollToPosition(0);

            mDailyIncomePenaltyListAdapter.setDataList(ack.getPenaltyList());
            mPenaltyListRecyclerView.scrollToPosition(0);
        } catch (Exception e) {
            DLog.e(TAG, "onReceivedIncomeQueryAck exception", e);
        }
    }
}