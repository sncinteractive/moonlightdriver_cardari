package kr.driver.cardari.cardari.view.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Spinner;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.driver.cardari.cardari.MyApp;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.AES256Cipher;
import kr.driver.cardari.cardari.common.CommonDialog;
import kr.driver.cardari.cardari.common.Defines;
import kr.driver.cardari.cardari.common.Util;
import kr.driver.cardari.cardari.data.InsuranceAPIData;
import kr.driver.cardari.cardari.logger.DLog;
import kr.driver.cardari.cardari.network.NetClient;
import kr.driver.cardari.cardari.network.NetResponseCallback;
import kr.driver.cardari.cardari.network.ack.BaseAck;
import kr.driver.cardari.cardari.network.ack.RegisterAck;

import static kr.driver.cardari.cardari.common.Enums.NetResultCode;

public class RegisterActivity extends AppCompatActivity {
    private static final String TAG = "RegisterActivity";

    private Context mContext;

    private long mLastPressTime;

    @BindView(R.id.driverName) EditText mDriverName;
    @BindView(R.id.socialNumber1) EditText mSocialNumber1;
    @BindView(R.id.socialNumber2) EditText mSocialNumber2;
    @BindView(R.id.phone) EditText mPhone;
    @BindView(R.id.phone1) EditText mPhone1;
    @BindView(R.id.licenseType) Spinner mLicenseType;
    @BindView(R.id.licenseNumber) EditText mLicenseNumber;
    @BindView(R.id.email) EditText mEmail;

    private String mAuthPhoneNumber;
    private String mDi;
    private String mCi;
    private String mUniqueId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_register);
            ButterKnife.bind(this);

            mContext = this;

            mAuthPhoneNumber = "";
            mCi = "";
            mDi = "";
            mUniqueId = UUID.randomUUID().toString();

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

//            getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                public void onGlobalLayout() {
//                    setBackground();
//                }
//            });

            mPhone.setText(Util.convertToLocalPhoneNumber(MyApp.getInstance().getUserPhoneNumber()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // sejung84 : sip가 보여질 때 배경화면이 줄어드는 것을 방지하기 위해 background를 별도로 설정한다.
//    private void setBackground() {
//        try {
//            RelativeLayout bg = (RelativeLayout) findViewById(R.id.LAYOUT_CONT_BG);
//            if (bg.getBackground() == null) {
//                Rect r = new Rect();
//                View view = getWindow().getDecorView();
//                view.getWindowVisibleDisplayFrame(r);
//                int height = r.bottom - r.top - 600;
//
//                r = null;
//
//                if (height <= 0) {
//                    return;
//                }
//
//                bg.setBackgroundResource(R.drawable.register_bg);
//                RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(-1, height);
//                bg.setLayoutParams(param);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    @OnClick(R.id.btnRegister)
    public void onClickBtnRegister(View view) {
        try {
            CommonDialog.showDialogWithListener(mContext,"회원가입", "회원 가입을 하시겠습니까?", "가입", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(!checkParameters()) {
                        return;
                    }

                    register();
                }
            }, "취소", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btnPhoneAuth)
    public void onClickPhoneAuth(View view) {
        try {
            mAuthPhoneNumber = "";
            mCi = "";
            mDi = "";

            startActivityForResult(new Intent(RegisterActivity.this, OkNameWebViewActivity.class), Defines.PHONE_AUTH_REQ_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkParameters() {
        try {
            String driverName = mDriverName.getText().toString();
            String socialNumber1 = mSocialNumber1.getText().toString();
            String socialNumber2 = mSocialNumber2.getText().toString();
            String phone = mPhone.getText().toString();
            String licenseType = String.valueOf(mLicenseType.getSelectedItem());
            String licenseNumber = mLicenseNumber.getText().toString();

            if(driverName.isEmpty()) {
                CommonDialog.showSimpleDialog(mContext, "이름을 입력해 주세요.");
                return false;
            }

            if(socialNumber1.isEmpty() || socialNumber1.length() < 6) {
                CommonDialog.showSimpleDialog(mContext, "주민번호 앞 6자리를 입력해 주세요.");
                return false;
            }

            if(socialNumber2.isEmpty() || socialNumber2.length() < 7) {
                CommonDialog.showSimpleDialog(mContext, "주민번호 뒤 7자리를 입력해 주세요.");
                return false;
            }

            // TODO : 입력한 휴대폰 번호와 시스템에서 조회한 휴대폰 번호가 다를 경우 처리는?
            if(phone.isEmpty() || phone.length() < 11) {
                CommonDialog.showSimpleDialog(mContext, "휴대폰 번호를 입력해 주세요.");
                return false;
            }

            // TODO : 휴대폰 인증 문제로 잠시 주석처리
//            if(mAuthPhoneNumber.isEmpty() || mCi.isEmpty() || mDi.isEmpty()) {
//                CommonDialog.showSimpleDialog(mContext, "휴대폰 본인 인증을 진행 후 등록해 주세요.");
//                return false;
//            }

            if(licenseType.isEmpty()) {
                CommonDialog.showSimpleDialog(mContext, "운전면허 종류를 선택해 주세요.");
                return false;
            }

            if(licenseNumber.isEmpty()) {
                CommonDialog.showSimpleDialog(mContext, "운전면허 번호를 입력해 주세요.");
                return false;
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }

    public void register() {
        try {
            String driverName = mDriverName.getText().toString();
            String socialNumber1 = mSocialNumber1.getText().toString();
            String socialNumber2 = mSocialNumber2.getText().toString();
            String phone = mPhone.getText().toString();
            String phone1 = mPhone1.getText().toString();
            String licenseType = String.valueOf(mLicenseType.getSelectedItem());
            String licenseNumber = mLicenseNumber.getText().toString();
            String email = mEmail.getText().toString();

            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;

            RequestParams requestParams = new RequestParams();
            requestParams.add("uniqueId", mUniqueId);
            requestParams.add("name", driverName);
            requestParams.add("socialNumber", socialNumber1 + socialNumber2);
            requestParams.add("phone", phone);
            requestParams.add("phone1", phone1);
            requestParams.add("licenseType", licenseType);
            requestParams.add("licenseNumber", licenseNumber);
            requestParams.add("email", email);
            requestParams.add("macAddr", "");
            requestParams.add("gcmId", MyApp.getInstance().getRegistrationId());
            requestParams.add("version", version);
            requestParams.add("lat", String.valueOf(MyApp.getInstance().getCurLocation().latitude));
            requestParams.add("lng", String.valueOf(MyApp.getInstance().getCurLocation().longitude));

            NetClient.post(this, NetClient.URL_REGISTER, RegisterAck.class, requestParams, new NetResponseCallback(new RegisterResponseListener()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class RegisterResponseListener implements NetResponseCallback.NetResponseListener {
        @Override
        public void onResponse(BaseAck result) {
            try {
                if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
//                    startActivity(new Intent(RegisterActivity.this, MainActivity.class));
//                    finish();
                    RegisterAck ack = (RegisterAck) result;
                    requestUnderWriting(ack);
                } else {
                    CommonDialog.showSimpleDialog(mContext, "회원가입에 실패했습니다.\n다시 시도해주세요.");
                }
            } catch (Exception e) {
                DLog.e(TAG, "RegisterResponseListener::onResponse exception", e);
            }
        }
    }

    private void requestUnderWriting(final RegisterAck registerData) {
        try {
            String driverName = mDriverName.getText().toString();
            String socialNumber1 = mSocialNumber1.getText().toString();
            String socialNumber2 = mSocialNumber2.getText().toString();
            String socialNumber = socialNumber1 + socialNumber2;
            String phone = mPhone.getText().toString();
            String phone1 = mPhone1.getText().toString();
            String licenseType = String.valueOf(mLicenseType.getSelectedItem());
            String licenseNumber = mLicenseNumber.getText().toString();
            String email = mEmail.getText().toString();

            JSONArray requestDataArr = new JSONArray();
            JSONObject requestData = new JSONObject();

            requestData.put("company_number", Defines.INSURANCE_COMPANY_NUMBER);
            requestData.put("biz_driver_id", mUniqueId);
            requestData.put("cell", AES256Cipher.AES_Encode(phone, Defines.INSURANCE_CRYPT_KEY));
            requestData.put("cell2", AES256Cipher.AES_Encode(phone1, Defines.INSURANCE_CRYPT_KEY));
            requestData.put("name", driverName);
            requestData.put("social_number", AES256Cipher.AES_Encode(socialNumber, Defines.INSURANCE_CRYPT_KEY));
            requestData.put("license_type", licenseType);
            requestData.put("license_number", licenseNumber);
            requestData.put("email", email);
            requestData.put("memo", "");

            requestDataArr.put(requestData);

            RequestParams requestParams = new RequestParams();
            requestParams.add("apiKey", Defines.INSURANCE_API_KEY);
            requestParams.add("request_data", requestDataArr.toString());

            NetClient.postInsuranceUnderWriting(mContext, requestParams, new NetClient.InsuranceAPIResponseCallback() {
                @Override
                public void onResponse(InsuranceAPIData result) {
                    if (result != null) {
                        if(result.getMessage().equals("success") && result.getReturn_num().equals("1")) {
                            requestSaveReqUnderWritingLog(registerData.getDriverId());
                        } else {
                            CommonDialog.showSimpleDialog(mContext, "보험 심사 제출에 실패했습니다.\n관리자에게 문의해주세요.");/**/
                        }
                    } else {
                        CommonDialog.showSimpleDialog(mContext, "보험 심사 제출에 실패했습니다.\n관리자에게 문의해주세요.");/**/
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestSaveReqUnderWritingLog(String driverId) {
        try {
            RequestParams requestParams = new RequestParams();
            requestParams.add("driverId", driverId);

            NetClient.post(this, NetClient.URL_SAVE_REQ_UNDER_WRITING_LOG, BaseAck.class, requestParams, new NetResponseCallback(new SaveReqUnderWritingLogResponseListener()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class SaveReqUnderWritingLogResponseListener implements NetResponseCallback.NetResponseListener {
        @Override
        public void onResponse(BaseAck result) {
            try {
                if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
                    startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                    finish();
                } else {
                    CommonDialog.showSimpleDialog(mContext, "회원가입에 실패했습니다.\n다시 시도해주세요.");
                }
            } catch (Exception e) {
                DLog.e(TAG, "RegisterResponseListener::onResponse exception", e);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            switch (requestCode) {
                case Defines.PHONE_AUTH_REQ_CODE:
                    String ret = "";

                    if(data != null) {
                        ret = data.getStringExtra(Defines.EXTRA_KEY_PHONE_AUTH_RESULT);
                        mAuthPhoneNumber = data.getStringExtra(Defines.EXTRA_KEY_PHONE_AUTH_PHONE_NUMBER);
                        mCi = data.getStringExtra(Defines.EXTRA_KEY_PHONE_AUTH_CI);
                        mDi = data.getStringExtra(Defines.EXTRA_KEY_PHONE_AUTH_DI);
                    }

                    if(!ret.equals("OK")) {
                        mAuthPhoneNumber = "";
                        mCi = "";
                        mDi = "";
                    }

                    mPhone.setText(Util.convertToLocalPhoneNumber(mAuthPhoneNumber));

                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        try {
            long pressTime = System.currentTimeMillis();

            if (pressTime - mLastPressTime <= Defines.DOUBLE_PRESS_INTERVAL) {
                new AlertDialog.Builder(mContext)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setCancelable(false)
                    .setTitle("앱 종료")
                    .setMessage("앱을 종료하시겠습니까?")
                    .setPositiveButton("종료", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Util.finishActivity(RegisterActivity.this);
                        }
                    })
                    .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mLastPressTime = System.currentTimeMillis();
                        }
                    })
                    .show();
            } else {
                super.onBackPressed();

                mLastPressTime = pressTime;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
