package kr.driver.cardari.cardari.network.ack;

/**
 * Created by eklee on 2017. 11. 1..
 */
public class BaseAck {
    private NetResultData result;

    public BaseAck() {
    }

    public BaseAck(NetResultData result) {
        this.result = result;
    }

    public NetResultData getResult() {
        return result;
    }

    public void setResult(NetResultData result) {
        this.result = result;
    }
}
