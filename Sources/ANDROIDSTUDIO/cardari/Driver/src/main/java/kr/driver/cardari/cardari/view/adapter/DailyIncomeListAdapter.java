package kr.driver.cardari.cardari.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.Util;
import kr.driver.cardari.cardari.data.DailyIncomeData;
import kr.driver.cardari.cardari.logger.DLog;

/**
 * Created by eklee on 2017. 11. 7..
 */

public class DailyIncomeListAdapter extends RecyclerView.Adapter {
    private static final String TAG = "DailyIncomeListAdapter";

    private List<DailyIncomeData> mDailyIncomeDataList;

    public DailyIncomeListAdapter() {
    }

    public void setDataList(List<DailyIncomeData> dailyIncomeDataList) {
        mDailyIncomeDataList = dailyIncomeDataList;

        notifyDataSetChanged();
    }

    public DailyIncomeData getItem(int position) {
        try {
            return mDailyIncomeDataList.get(position);
        } catch (Exception e) {
            DLog.e(TAG, "getItem exception", e);
        }

        return null;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_daily_income_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).bind(mDailyIncomeDataList.get(position));
    }

    @Override
    public int getItemCount() { return (mDailyIncomeDataList != null ? mDailyIncomeDataList.size() : 0); }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.sequence_text_view) TextView mSequenceTextView;
        @BindView(R.id.source_point_text_view) TextView mSourcePointTextView;
        @BindView(R.id.end_point_text_view) TextView mEndPointTextView;
        @BindView(R.id.elapsed_time_text_view) TextView mElapsedTimeTextView;
        @BindView(R.id.charge_text_view) TextView mChargeTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(DailyIncomeData data) {
            try {
                mSequenceTextView.setText(Integer.toString(data.getSequence()));
                mSourcePointTextView.setText(data.getStartPointName());
                mEndPointTextView.setText(data.getEndPointName());
                mElapsedTimeTextView.setText(Util.convertTimeToString(data.getElapsedTime()));
                mChargeTextView.setText(Util.convertNumberToCommaString(data.getCharge(), true));
            } catch (Exception e) {
                DLog.e(TAG, "ViewHolder::Bind exception", e);
            }
        }
    }
}
