package kr.driver.cardari.cardari.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.Enums;
import kr.driver.cardari.cardari.common.Util;
import kr.driver.cardari.cardari.logger.DLog;
import kr.driver.cardari.cardari.network.ack.IncomeQueryAck;

/**
 * Created by eklee on 2017. 11. 21..
 */

public class MonthlyIncomeFragment extends Fragment implements IncomeDetailFragment.EventListener {
    public interface EventListener {
        void needToShowLoadingProgressbar();

        void needToHideLoadingProgressbar();
    }

    private static final String TAG = "MonthlyIncomeFragment";

    @BindView(R.id.completion_total_count_text_view) TextView mCompletionTotalCountTextView;
    @BindView(R.id.completion_total_charge_text_view) TextView mCompletionTotalChargeTextView;

    @BindView(R.id.penalty_total_count_text_view) TextView mPenaltyTotalCountTextView;
    @BindView(R.id.penalty_total_charge_text_view) TextView mPenaltyTotalChargeTextView;

    private Unbinder mUnBinder;
    private EventListener mEventListener;

    public static MonthlyIncomeFragment createInstance() {
        return new MonthlyIncomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_monthly_income, container, false);
        mUnBinder = ButterKnife.bind(this, rootView);

        initializeView();

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            if (Build.VERSION.SDK_INT >= 23) {
                mEventListener = (EventListener) context;
            }
        } catch (Exception e) {
            DLog.e(TAG, "onAttach - " + context.toString() + " not implemented event listener", e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            if (Build.VERSION.SDK_INT < 23) {
                mEventListener = (EventListener) activity;
            }
        } catch (Exception e) {
            DLog.e(TAG, "onAttach - " + activity.toString() + " not implemented event listener", e);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mEventListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnBinder.unbind();
    }

    private void initializeView() {
        // income detail fragment
        Fragment fragment = IncomeDetailFragment.createInstance(Enums.IncomeDateType.MONTHLY, this);
        getChildFragmentManager().beginTransaction().replace(R.id.fragment_income_detail_frame, fragment).commit();

        mCompletionTotalCountTextView.setText(null);
        mCompletionTotalChargeTextView.setText(null);

        mPenaltyTotalCountTextView.setText(null);
        mPenaltyTotalChargeTextView.setText(null);
    }

    @Override
    public void needToShowLoadingProgressbar() {
        try {
            mEventListener.needToShowLoadingProgressbar();
        } catch (Exception e) {
            DLog.e(TAG, "needToShowLoadingProgressbar exception", e);
        }
    }

    @Override
    public void needToHideLoadingProgressbar() {
        try {
            mEventListener.needToHideLoadingProgressbar();
        } catch (Exception e) {
            DLog.e(TAG, "needToHideLoadingProgressbar exception", e);
        }
    }

    @Override
    public void onReceivedIncomeQueryAck(IncomeQueryAck ack) {
        mCompletionTotalCountTextView.setText(Util.convertNumberToCommaString(ack.getTotalCount()));
        mCompletionTotalChargeTextView.setText(Util.convertNumberToCommaString(ack.getCompletionTotalAmount(), true));

        mPenaltyTotalCountTextView.setText(Util.convertNumberToCommaString(ack.getPenaltyTotalCount()));
        mPenaltyTotalChargeTextView.setText(Util.convertNumberToCommaString(ack.getPenaltyTotalAmount(), true));
    }
}