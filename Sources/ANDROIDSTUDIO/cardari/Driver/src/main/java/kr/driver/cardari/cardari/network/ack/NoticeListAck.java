package kr.driver.cardari.cardari.network.ack;

import java.util.ArrayList;

import kr.driver.cardari.cardari.data.NoticeData;

/**
 * Created by youngmin on 2016-08-18.
 */
public class NoticeListAck extends BaseAck {
	private ArrayList<NoticeData> notices;

	public NoticeListAck() {}

	public NoticeListAck(ArrayList<NoticeData> notices) {
		this.notices = notices;
	}

	public NoticeListAck(NetResultData result, ArrayList<NoticeData> notices) {
		super(result);
		this.notices = notices;
	}

	public ArrayList<NoticeData> getNotices() {


		return notices;
	}

	public void setNotices(ArrayList<NoticeData> notices) {
		this.notices = notices;
	}
}
