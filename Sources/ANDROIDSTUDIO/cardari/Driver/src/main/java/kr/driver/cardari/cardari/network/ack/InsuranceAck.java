package kr.driver.cardari.cardari.network.ack;

/**
 * Created by eklee on 2017. 11. 1..
 */
public class InsuranceAck {
    private String Message;
    private String return_num;

    public InsuranceAck() {
    }

    public InsuranceAck(String message, String return_num) {
        Message = message;
        this.return_num = return_num;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getReturn_num() {
        return return_num;
    }

    public void setReturn_num(String return_num) {
        this.return_num = return_num;
    }
}
