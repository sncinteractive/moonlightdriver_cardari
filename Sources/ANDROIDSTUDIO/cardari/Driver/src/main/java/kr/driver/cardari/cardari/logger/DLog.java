package kr.driver.cardari.cardari.logger;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.LinkedBlockingQueue;

public class DLog {
    public enum LogType {Error, Warning, Debug, Info, Verbose}

    private static boolean sEnableADBLog = false;
    private static LinkedBlockingQueue<String> sFileLogQueue;

    private static final SimpleDateFormat sLogDateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSSS");

    public static void enableADBLog(boolean enable) {
        sEnableADBLog = enable;
    }

    public static void setFileLogQueue(LinkedBlockingQueue<String> queue) {
        sFileLogQueue = queue;
    }

    /**
     * adb log only if enabled, no stack trace, no file writing
     *
     * @param type
     * @param tag
     * @param message
     */
    public static void adbLog(LogType type, String tag, String message) {
        if (!sEnableADBLog) {
            return;
        }

        switch (type) {
            case Error: {
                Log.e(tag, message);
            }
            break;
            case Warning: {
                Log.w(tag, message);
            }
            break;
            case Debug: {
                Log.d(tag, message);
            }
            break;
            case Info: {
                Log.i(tag, message);
            }
            break;
            case Verbose: {
                Log.v(tag, message);
            }
            break;
            default:
                break;
        }
    }

    public static void e(String tag, String message) {
        e(tag, message, null);
    }

    public static void e(String tag, String message, Throwable exception) {
        if (!sEnableADBLog && sFileLogQueue == null) {
            return;
        }

        if (exception != null) {
            String finalMessage = message + generateStackTraceMessage(exception);
            if (sEnableADBLog) {
                Log.e(tag, finalMessage);
                exception.printStackTrace();
            }

            if (sFileLogQueue != null) {
                addFileLogQueue("Error", tag, finalMessage);
            }
        } else {
            if (sEnableADBLog) {
                Log.e(tag, message);
            }

            if (sFileLogQueue != null) {
                addFileLogQueue("Error", tag, message);
            }
        }
    }

    public static void w(String tag, String message) {
        w(tag, message, null);
    }

    public static void w(String tag, String message, Throwable exception) {
        if (!sEnableADBLog && sFileLogQueue == null) {
            return;
        }

        if (exception != null) {
            String finalMessage = message + generateStackTraceMessage(exception);
            if (sEnableADBLog) {
                Log.w(tag, finalMessage);
                exception.printStackTrace();
            }

            if (sFileLogQueue != null) {
                addFileLogQueue("Warning", tag, finalMessage);
            }
        } else {
            if (sEnableADBLog) {
                Log.w(tag, message);
            }

            if (sFileLogQueue != null) {
                addFileLogQueue("Warning", tag, message);
            }
        }
    }

    public static void d(String tag, String message) {
        d(tag, message, null);
    }

    public static void d(String tag, String message, Throwable exception) {
        if (!sEnableADBLog && sFileLogQueue == null) {
            return;
        }

        if (exception != null) {
            String finalMessage = message + generateStackTraceMessage(exception);
            if (sEnableADBLog) {
                Log.d(tag, finalMessage);
                exception.printStackTrace();
            }

            if (sFileLogQueue != null) {
                addFileLogQueue("Debug", tag, finalMessage);
            }
        } else {
            if (sEnableADBLog) {
                Log.d(tag, message);
            }

            if (sFileLogQueue != null) {
                addFileLogQueue("Debug", tag, message);
            }
        }
    }

    public static void i(String tag, String message) {
        i(tag, message, null);
    }

    public static void i(String tag, String message, Throwable exception) {
        if (!sEnableADBLog && sFileLogQueue == null) {
            return;
        }

        if (exception != null) {
            String finalMessage = message + generateStackTraceMessage(exception);
            if (sEnableADBLog) {
                Log.i(tag, finalMessage);
                exception.printStackTrace();
            }

            if (sFileLogQueue != null) {
                addFileLogQueue("Info", tag, finalMessage);
            }
        } else {
            if (sEnableADBLog) {
                Log.i(tag, message);
            }

            if (sFileLogQueue != null) {
                addFileLogQueue("Info", tag, message);
            }
        }
    }

    public static void v(String tag, String message) {
        v(tag, message, null);
    }

    public static void v(String tag, String message, Throwable exception) {
        if (!sEnableADBLog && sFileLogQueue == null) {
            return;
        }

        if (exception != null) {
            String finalMessage = message + generateStackTraceMessage(exception);
            if (sEnableADBLog) {
                Log.v(tag, finalMessage);
                exception.printStackTrace();
            }

            if (sFileLogQueue != null) {
                addFileLogQueue("Verbose", tag, finalMessage);
            }
        } else {
            if (sEnableADBLog) {
                Log.v(tag, message);
            }

            if (sFileLogQueue != null) {
                addFileLogQueue("Verbose", tag, message);
            }
        }
    }

    private static String generateStackTraceMessage(Throwable exception) {
        StringBuilder sb = new StringBuilder();
        sb.append("\n").append(exception);
        StackTraceElement[] traces = exception.getStackTrace();
        for (StackTraceElement trace : traces) {
            sb.append("\tat ").append(trace).append("\n");
        }

        return sb.toString();
    }

    private static void addFileLogQueue(String type, String tag, String message) {
        try {
            String finalMessage = type + "/" + tag + ": " + message;
            sFileLogQueue.put(sLogDateFormat.format(new Date()) + " " + finalMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
