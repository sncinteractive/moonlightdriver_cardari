package kr.driver.cardari.cardari.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.driver.cardari.cardari.MyApp;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.Defines;
import kr.driver.cardari.cardari.common.DialogHelper;
import kr.driver.cardari.cardari.common.Enums;
import kr.driver.cardari.cardari.common.Enums.NetResultCode;
import kr.driver.cardari.cardari.common.Util;
import kr.driver.cardari.cardari.data.OrderData;
import kr.driver.cardari.cardari.logger.DLog;
import kr.driver.cardari.cardari.network.NetClient;
import kr.driver.cardari.cardari.network.NetResponseCallback;
import kr.driver.cardari.cardari.network.ack.AcceptOrderAck;
import kr.driver.cardari.cardari.network.ack.BaseAck;
import kr.driver.cardari.cardari.network.ack.BiddingAck;
import kr.driver.cardari.cardari.network.ack.OrderDetailAck;
import kr.driver.cardari.cardari.tmap.TMapHelper;
import kr.driver.cardari.cardari.tmap.TMapRouteData;
import kr.driver.cardari.cardari.view.fragment.MainMenuFragment;
import kr.driver.cardari.cardari.view.fragment.MapControlFragment;

import static kr.driver.cardari.cardari.common.Defines.EXTRA_KEY_ORDER_ID;

public class NormalOrderDetailActivity extends AppCompatActivity implements MapControlFragment.EventListener {
    private static final String TAG = "NormalOrderDetailActivity";

    @BindView(R.id.title_text_view) TextView mTitleTextView;

    @BindView(R.id.order_info_layout) LinearLayout mOrderInfoLayout;
    @BindView(R.id.client_grade_title_text_view) TextView mClientGradeTitleTextView;
    @BindView(R.id.client_grade_text_view) TextView mClientGradeTextView;
    @BindView(R.id.start_point_title_text_view) TextView mStartPointTitleTextView;
    @BindView(R.id.start_point_text_view) TextView mStartPointTextView;
    @BindView(R.id.end_point_title_text_view) TextView mEndPointTitleTextView;
    @BindView(R.id.end_point_text_view) TextView mEndPointTextView;
    @BindView(R.id.distance_title_text_view) TextView mDistanceTitleTextView;
    @BindView(R.id.distance_text_view) TextView mDistanceTextView;
    @BindView(R.id.estimate_time_title_text_view) TextView mEstimateTimeTitleTextView;
    @BindView(R.id.estimate_time_text_view) TextView mEstimateTimeTextView;
    @BindView(R.id.payamount_title_text_view) TextView mPayamountTitleTextView;
    @BindView(R.id.payamount_text_view) TextView mPayamountTextView;

    @BindView(R.id.decrease_button) Button mDecreaseButton;
    @BindView(R.id.bidding_price_text_view) TextView mBiddingPriceTextView;
    @BindView(R.id.increase_button) Button mIncreaseButton;
    @BindView(R.id.bidding_button) Button mBiddingButton;

    @BindView(R.id.function_layout_normal) ConstraintLayout mFunctionLayoutNormal;
    @BindView(R.id.function_layout_auction) ConstraintLayout mFunctionLayoutAuction;

    @BindView(R.id.loading_progressbar_layout) View mLoadingProgressbarLayout;

    private OrderData mOrderData;

    private int mMoney = 0;
    private AlertDialog mBiddingWaitDialog;
    private int mBiddingWaitTime;

    private Timer mTimer;

    private MapControlFragment mMapControlFragment;
    private MainMenuFragment mMainMenuFragment;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_normal_order_detail);
            ButterKnife.bind(this);

            mContext = this;

            Intent intent = getIntent();
            Bundle extras = intent.getExtras();
            if (extras != null) {
                String orderId = extras.getString(EXTRA_KEY_ORDER_ID);
                requestOrderDetail(orderId);
            } else {
                DialogHelper.showAlertDialog(NormalOrderDetailActivity.this, "알림", "잘못된 요청입니다.\n", "확인", null, new DialogHelper.AlertDialogButtonCallback() {
                    @Override
                    public void onClickButton(boolean isPositiveClick) {
                        onClickBack();
                    }
                });
            }
        } catch (Exception e) {
            DLog.e(TAG, "onCreate exception", e);
        }
    }

    private void initializeView() {
        try {
            if (mMainMenuFragment == null) {
                mMainMenuFragment = new MainMenuFragment();
            }

            mMainMenuFragment.updateDriverBalance(MyApp.getInstance().getDriverData().getBalance());

            mTimer = new Timer(true);

            mMoney = mOrderData.getMoney();
            updateBiddingPriceText();

            // title
            mTitleTextView.setText(R.string.normal_order_title);

            // map control fragment
            mMapControlFragment = MapControlFragment.createInstance(
                    mOrderData.getClientLocation().getLocation().getCoordinatesLatLng(),
                    mOrderData.getStartLocation().getLocation().getCoordinatesLatLng(),
                    mOrderData.getEndLocation().getLocation().getCoordinatesLatLng(),
                    mOrderData.getThroughLocationLatLngList());

            getSupportFragmentManager().beginTransaction().replace(R.id.map_control_fragment_frame, mMapControlFragment).commit();

            // info
            mClientGradeTextView.setText(mOrderData.getClientGrade());
            mStartPointTextView.setText(mOrderData.getStartLocation().getAddress());
            mEndPointTextView.setText(mOrderData.getEndLocation().getAddress());
            mDistanceTextView.setText("0km");
            mEstimateTimeTextView.setText("0분");
            mPayamountTextView.setText(Util.convertNumberToCommaString(mOrderData.getMoney(), true));

            changeFunctionLayout();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    RequestParams requestParams = TMapHelper.generateTMapRouteRequestParams(mOrderData.getStartLocation().getLocation().getCoordinatesLatLng(), mOrderData.getEndLocation().getLocation().getCoordinatesLatLng(), mOrderData.getThroughLocationLatLngList());
                    NetClient.postTMapRoute(mContext, requestParams, new NetClient.TMapRouteResponseCallback() {
                        @Override
                        public void onResponse(TMapRouteData result) {
                            if (result != null) {
                                mDistanceTextView.setText(Util.convertDistanceString((int)result.getTotalDistance())); // 거리, 소요시간 계산
                                mEstimateTimeTextView.setText(Util.convertTimeToString2((int)result.getTotalTime(), true));
                            }
                        }
                    });
                }
            }, 2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeFunctionLayout() {
        try {
            boolean isModifiable = Util.isAvailableModify(mOrderData.getCreatedTime());

            if(isModifiable) {
                mFunctionLayoutNormal.setVisibility(View.GONE);
                mFunctionLayoutAuction.setVisibility(View.VISIBLE);
            } else {
                mFunctionLayoutNormal.setVisibility(View.VISIBLE);
                mFunctionLayoutAuction.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestOrderDetail(String orderId) {
        try {
            //  오더 상세 조회 서버 연동
            RequestParams requestParams = new RequestParams();
            requestParams.put("callId", orderId);

            NetClient.get(this, NetClient.URL_CALL_DETAIL, OrderDetailAck.class, requestParams, new NetResponseCallback(new OrderDetailResponseListener()));
        } catch (Exception e) {
            DLog.e(TAG, "requestOrderList exception", e);
        }
    }

    public class OrderDetailResponseListener implements NetResponseCallback.NetResponseListener {
        @Override
        public void onResponse(BaseAck result) {
            try {
                if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
                    OrderDetailAck ack = (OrderDetailAck) result;
                    mOrderData = ack.getCallDetail();

                    initializeView();
                } else {
                    DialogHelper.showAlertDialog(NormalOrderDetailActivity.this, "알림", "오더 정보 조회 요청이 실패했습니다.\n" + "code: " + result.getResult().getCode() + "detail: " + result.getResult().getDetail(), "확인", null, null);
                }
            } catch (Exception e) {
                DLog.e(TAG, "OrderDetailResponseListener::onResponse exception", e);
            }
        }
    }

    @OnClick(R.id.back_button)
    public void onClickBack() {
        super.onBackPressed();
    }

    @OnClick(R.id.refusal_button)
    public void onClickRefusal() {
        // 오더 거부 서버 연동
        RequestParams requestParams = new RequestParams();
        requestParams.put("callId", mOrderData.getCallId());
        requestParams.put("driverId", MyApp.getInstance().getDriverData().getDriverId());

        NetClient.post(this, NetClient.URL_REFUSE_CALL, BaseAck.class, requestParams, new NetResponseCallback(new NetResponseCallback.NetResponseListener() {
            @Override
            public void onResponse(BaseAck result) {
                processRefuseOrderResponse(result);
            }
        }));

        // test code begin
//        processRefuseOrderResponse(new BaseAck(new NetResultData(NetResultCode.SUCCESS, null)));
        // test code end
    }

    private void processRefuseOrderResponse(BaseAck result) {
        try {
            if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
                Intent intent = new Intent(this, OrderListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent); // OrderListActivity의 onNewIntent가 호출된다
            } else {
                DialogHelper.showAlertDialog(this, "알림", "거부 요청이 실패했습니다.", getString(R.string.common_confirm), null, null);
            }
        } catch (Exception e) {
            DLog.e(TAG, "processRefuseOrderResponse exception", e);
        }
    }

    @OnClick({R.id.confirm_button, R.id.no_bidding_confirm_button})
    public void onClickConfirm() {
        // 오더 수락 서버 연동
        RequestParams requestParams = new RequestParams();
        requestParams.put("driverId", MyApp.getInstance().getDriverData().getDriverId());
        requestParams.put("callId", mOrderData.getCallId());

        NetClient.post(this, NetClient.URL_CATCH_CALL, AcceptOrderAck.class, requestParams, new NetResponseCallback(new NetResponseCallback.NetResponseListener() {
            @Override
            public void onResponse(BaseAck result) {
                processAcceptOrderResponse(result);
            }
        }));

        // test code begin
//        processAcceptOrderResponse(new AcceptOrderAck(new NetResultData(NetResultCode.SUCCESS, null), DriverState.ALLOCATION));
        // test code end
    }

    private void processAcceptOrderResponse(BaseAck result) {
        try {
            if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
                AcceptOrderAck ack = (AcceptOrderAck) result;

                //ack.getCallState().equals(Enums.CallState.WAIT)
                MyApp.getInstance().getDriverData().setmDriverCallInfoState(Enums.DriverState.ALLOCATION);

                MyApp.getInstance().setAllocatedOrderData(mOrderData);

                DialogHelper.showAlertDialog(this,
                        getString(R.string.order_accept_result_popup_success_title),
                        getString(R.string.order_accept_result_popup_success_message),
                        getString(R.string.common_confirm),
                        null,
                        new DialogHelper.AlertDialogButtonCallback() {
                            @Override
                            public void onClickButton(boolean isPositiveClick) {
                                Intent intent = new Intent(NormalOrderDetailActivity.this, AllocationActivity.class);
                                intent.putExtra(EXTRA_KEY_ORDER_ID, mOrderData.getCallId());
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        });
            } else {
                DialogHelper.showAlertDialog(this, "알림", "콜 수락 요청이 실패했습니다.\n" + "code: " + result.getResult().getCode() + "detail: " + result.getResult().getDetail(), "확인", null, null);
            }
        } catch (Exception e) {
            DLog.e(TAG, "processAcceptOrderResponse", e);
        }
    }

    @Override
    public void onFullScreenModeChanged(boolean isFullScreen) {
        if (isFullScreen) {
            mOrderInfoLayout.setVisibility(View.GONE);
            changeFunctionLayout();
        } else {
            mOrderInfoLayout.setVisibility(View.VISIBLE);
            changeFunctionLayout();
        }
    }

    @OnClick(R.id.increase_button)
    public void onClickIncrease() {
        mMoney += 1000;
        updateBiddingPriceText();
    }

    @OnClick(R.id.decrease_button)
    public void onClickDecrease() {
        mMoney -= 1000;
        updateBiddingPriceText();
    }

    private void updateBiddingPriceText() {
        mBiddingPriceTextView.setText(Integer.toString(mMoney));
    }

    @OnClick(R.id.bidding_button)
    public void onClickBidding() {
        DialogHelper.showAlertDialog(this,
                getString(R.string.bidding_popup_title),
                String.format(getString(R.string.bidding_popup_message_format), Util.convertNumberToCommaString(mMoney)),
                getString(R.string.bidding_popup_confirm_title),
                getString(R.string.bidding_popup_cancel_title),
                new DialogHelper.AlertDialogButtonCallback() {
                    @Override
                    public void onClickButton(boolean isPositiveClick) {
                        if (isPositiveClick) {
                            requestBidding();
                        }
                    }
                }
        );
    }

    private void requestBidding() {
        try {
            showLoadingProgressbar();

            RequestParams requestParams = new RequestParams();
            requestParams.put("driverId", MyApp.getInstance().getDriverData().getDriverId());
            requestParams.put("callId", mOrderData.getCallId());
            requestParams.put("biddingPrice", mMoney);
            NetClient.post(NormalOrderDetailActivity.this, NetClient.URL_BIDDING, BiddingAck.class, requestParams, new NetResponseCallback(new NetResponseCallback.NetResponseListener() {
                @Override
                public void onResponse(BaseAck result) {
                    processBiddingResponse(result);
                }
            }));

//            processBiddingResponse(new BiddingAck(new NetResultData(NetResultCode.SUCCESS, null), true, 7));
        } catch (Exception e) {
            DLog.e(TAG, "requestBidding exception", e);
        }
    }

    private void processBiddingResponse(BaseAck result) {
        hideLoadingProgressbar();

        try {
            if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
                BiddingAck ack = (BiddingAck) result;
                mBiddingWaitTime = Defines.DEFAULT_BIDDING_WAIT_TIME;
                String message = String.format(getString(R.string.bidding_wait_popup_message_format), mBiddingWaitTime);
                mBiddingWaitDialog = DialogHelper.showAlertDialog(this,
                        getString(R.string.bidding_wait_popup_title),
                        message,
                        null,
                        null,
                        null);

                mBiddingWaitDialog.setCancelable(false);

                mTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        mBiddingWaitTime -= 1;

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (mBiddingWaitTime > 0) {
                                    String message = String.format(getString(R.string.bidding_wait_popup_message_format), mBiddingWaitTime);
                                    mBiddingWaitDialog.setMessage(message);
                                } else {
                                    cancel();

                                    requestBiddingResult();
                                }
                            }
                        });
                    }
                }, 1000, 1000);
            } else {
                DialogHelper.showAlertDialog(this, "알림", "콜비 수정 요청이 실패했습니다.\n" + "code: " + result.getResult().getCode() + "detail: " + result.getResult().getDetail(), "확인", null, null);
            }
        } catch (Exception e) {
            DLog.e(TAG, "processBiddingResponse exception", e);
        }
    }

    private void showLoadingProgressbar() {
        mLoadingProgressbarLayout.setVisibility(View.VISIBLE);
    }

    private void hideLoadingProgressbar() {
        mLoadingProgressbarLayout.setVisibility(View.GONE);
    }

    private void requestBiddingResult() {
        try {
            showLoadingProgressbar();

            // 콜비 취소 요청 서버 연동
            RequestParams requestParams = new RequestParams();
            requestParams.put("driverId", MyApp.getInstance().getDriverData().getDriverId());
            requestParams.put("callId", mOrderData.getCallId());
            NetClient.post(NormalOrderDetailActivity.this, NetClient.URL_BIDDING_RESULT, BaseAck.class, requestParams, new NetResponseCallback(new NetResponseCallback.NetResponseListener() {
                @Override
                public void onResponse(BaseAck result) {
                    processBiddingResultResponse(result);
                }
            }));

            // test code begin
//            processBiddingResultResponse(new BiddingResultAck(new NetResultData(NetResultCode.SUCCESS, null), BiddingResult.SUCCESS, 29000, DriverState.ALLOCATION));
            // test code end
        } catch (Exception e) {
            DLog.e(TAG, "requestBidding exception", e);
        }
    }

    private void processBiddingResultResponse(BaseAck result) {
        try {
            mBiddingWaitDialog.dismiss();

            hideLoadingProgressbar();

            if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
                Intent intent = new Intent(NormalOrderDetailActivity.this, OrderListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);

//                switch (ack.getBiddingResult()) {
//                    // 나에게 낙찰
//                    case BiddingResult.SUCCESS: {
//                        DialogHelper.showAlertDialog(this,
//                                getString(R.string.bidding_result_popup_title),
//                                String.format(getString(R.string.bidding_result_popup_message_format), Util.convertNumberToCommaString(ack.getBiddingPrice())),
//                                getString(R.string.common_confirm),
//                                null,
//                                new DialogHelper.AlertDialogButtonCallback() {
//                                    @Override
//                                    public void onClickButton(boolean isPositiveClick) {
//                                        if (isPositiveClick) {
//                                            MyApp.getInstance().getDriverData().setDriverState(ack.getDriverState());
//                                            MyApp.getInstance().setAllocatedOrderData(mOrderData);
//
//                                            Intent intent = new Intent(NormalOrderDetailActivity.this, AllocationActivity.class);
//                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                            startActivity(intent);
//                                        }
//                                    }
//                                });
//                    }
//                    break;
//                    // 유찰
//                    case BiddingResult.FAIL: {
//                        requestOrderDetail(mOrderData.getCallId());
//                    }
//                    break;
//                    // 다른 사람에게 낙찰됨
//                    case BiddingResult.OTHER: {
//                        Intent intent = new Intent(this, OrderListActivity.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                        startActivity(intent); // OrderListActivity의 onNewIntent가 호출된다
//                    }
//                    break;
//                }
            } else {
                DialogHelper.showAlertDialog(this, "알림", "콜비 수정 취소  요청이 실패했습니다.\n" + "code: " + result.getResult().getCode() + "detail: " + result.getResult().getDetail(), "확인", null, null);
            }
        } catch (Exception e) {
            DLog.e(TAG, "processBiddingResponse exception", e);
        }
    }
}
