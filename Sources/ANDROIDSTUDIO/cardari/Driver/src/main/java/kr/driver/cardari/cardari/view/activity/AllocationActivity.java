package kr.driver.cardari.cardari.view.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.driver.cardari.cardari.MyApp;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.AES256Cipher;
import kr.driver.cardari.cardari.common.CommonDialog;
import kr.driver.cardari.cardari.common.Defines;
import kr.driver.cardari.cardari.common.DialogHelper;
import kr.driver.cardari.cardari.common.Enums;
import kr.driver.cardari.cardari.common.Enums.MapViewType;
import kr.driver.cardari.cardari.common.Util;
import kr.driver.cardari.cardari.data.InsuranceAPIData;
import kr.driver.cardari.cardari.data.OrderData;
import kr.driver.cardari.cardari.logger.DLog;
import kr.driver.cardari.cardari.network.NetClient;
import kr.driver.cardari.cardari.network.NetResponseCallback;
import kr.driver.cardari.cardari.network.ack.BaseAck;
import kr.driver.cardari.cardari.network.ack.CancelOrderAck;
import kr.driver.cardari.cardari.network.ack.EndDrivingAck;
import kr.driver.cardari.cardari.network.ack.OrderDetailAck;
import kr.driver.cardari.cardari.network.ack.StartDrivingAck;
import kr.driver.cardari.cardari.tmap.TMapHelper;
import kr.driver.cardari.cardari.tmap.TMapRouteData;
import kr.driver.cardari.cardari.view.fragment.MainMenuFragment;
import kr.driver.cardari.cardari.view.fragment.MapControlFragment;

import static kr.driver.cardari.cardari.common.Enums.DistanceUnit;
import static kr.driver.cardari.cardari.common.Enums.DriverState;
import static kr.driver.cardari.cardari.common.Enums.NetResultCode;

public class AllocationActivity extends AppCompatActivity implements MainMenuFragment.EventListener, MapControlFragment.EventListener {

    private static final String TAG = "AllocationActivity";

    @BindView(R.id.allocation_notice_text_view) TextView mAllocationNoticeTextView;
    @BindView(R.id.distance_notice_text_view) TextView mDistanceNoticeTextView;

    @BindView(R.id.order_info_layout) LinearLayout mOrderInfoLayout;
    @BindView(R.id.client_grade_title_text_view) TextView mClientGradeTitleTextView;
    @BindView(R.id.client_grade_layout) LinearLayout mClientGradeLayout;
    @BindView(R.id.client_grade_text_view) TextView mClientGradeTextView;
    @BindView(R.id.call_client_button) Button mCallClientButton;
    @BindView(R.id.start_point_title_text_view) TextView mStartPointTitleTextView;
    @BindView(R.id.start_point_text_view) TextView mStartPointTextView;
    @BindView(R.id.end_point_title_text_view) TextView mEndPointTitleTextView;
    @BindView(R.id.end_point_text_view) TextView mEndPointTextView;
    @BindView(R.id.distance_title_text_view) TextView mDistanceTitleTextView;
    @BindView(R.id.distance_text_view) TextView mDistanceTextView;
    @BindView(R.id.estimate_time_title_text_view) TextView mEstimateTimeTitleTextView;
    @BindView(R.id.estimate_time_text_view) TextView mEstimateTimeTextView;
    @BindView(R.id.payamount_title_text_view) TextView mPayamountTitleTextView;
    @BindView(R.id.payamount_text_view) TextView mPayamountTextView;

    @BindView(R.id.function_layout) ConstraintLayout mFunctionLayout;
    @BindView(R.id.start_driving_button) Button mStartDrivingButton;
    @BindView(R.id.end_driving_button) Button mEndDrivingButton;

    @BindView(R.id.loading_progressbar_layout) View mLoadingProgressbarLayout;

    private Timer mTimer;

    private MapControlFragment mMapControlFragment;

    private Context mContext;

    private OrderData mOrderData;

    private int mEstimateTime;

    private MainMenuFragment mMainMenuFragment;

    private String mVirtualNumber;
    private PushAlertReceiver mPushAlertReceiver;
    private boolean mIsSecretCalling;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_allocation);
            ButterKnife.bind(this);

            mContext = this;

            mIsSecretCalling = false;

            Intent intent = getIntent();
            Bundle extras = intent.getExtras();
            if (extras != null) {
                String orderId = extras.getString(Defines.EXTRA_KEY_ORDER_ID);
                requestOrderDetail(orderId);
            } else {
                DialogHelper.showAlertDialog(AllocationActivity.this, "알림", "잘못된 요청입니다.\n", "확인", null, new DialogHelper.AlertDialogButtonCallback() {
                    @Override
                    public void onClickButton(boolean isPositiveClick) {
                        Intent intent = new Intent(AllocationActivity.this, OrderListActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent); // OrderListActivity의 onNewIntent가 호출된다
                    }
                });
            }
        } catch (Exception e) {
            DLog.e(TAG, "onCreate exception", e);
        }
    }

    private void initialize() {
        mTimer = new Timer(true);
    }

    private void initializeView() {
        // fragment main menu
        if (mMainMenuFragment == null) {
            mMainMenuFragment = new MainMenuFragment();
        }

        // fragment main menu
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main_menu_frame, mMainMenuFragment).commit();

        // allocation notice
        updateAllocationNoticeVisibility();

        // distance notice
        mDistanceNoticeTextView.setText(null);

        // map control fragment
        OrderData orderData = MyApp.getInstance().getAllocatedOrderData();
        mMapControlFragment = MapControlFragment.createInstance(
            orderData.getClientLocation().getLocation().getCoordinatesLatLng(),
            orderData.getStartLocation().getLocation().getCoordinatesLatLng(),
            orderData.getEndLocation().getLocation().getCoordinatesLatLng(),
            orderData.getThroughLocationLatLngList());
        getSupportFragmentManager().beginTransaction().replace(R.id.map_control_fragment_frame, mMapControlFragment).commit();

        // info
        mClientGradeTextView.setText(orderData.getClientGrade());
        mStartPointTextView.setText(orderData.getStartLocation().getAddress());
        mEndPointTextView.setText(orderData.getEndLocation().getAddress());
        mDistanceTextView.setText("0km"); // 거리, 소요시간 계산
        mEstimateTimeTextView.setText("0분");
        mPayamountTextView.setText(Util.convertNumberToCommaString(orderData.getMoney()));
        mMainMenuFragment.updateDriverBalance(MyApp.getInstance().getDriverData().getBalance());

        updateCallClientButtonVisibility();

        updateDrivingButtonVisibility();

        RequestParams requestParams = TMapHelper.generateTMapRouteRequestParams(mOrderData.getStartLocation().getLocation().getCoordinatesLatLng(), mOrderData.getEndLocation().getLocation().getCoordinatesLatLng(), mOrderData.getThroughLocationLatLngList());
        NetClient.postTMapRoute(mContext, requestParams, new NetClient.TMapRouteResponseCallback() {
            @Override
            public void onResponse(TMapRouteData result) {
                if (result != null) {
                    mDistanceTextView.setText(Util.convertDistanceString((int) result.getTotalDistance())); // 거리, 소요시간 계산
                    mEstimateTime = (int) result.getTotalTime();
                    mEstimateTimeTextView.setText(Util.convertTimeToString2(mEstimateTime, true));
                }
            }
        });
    }

    private void requestOrderDetail(String orderId) {
        try {
            //  오더 상세 조회 서버 연동
            RequestParams requestParams = new RequestParams();
            requestParams.put("callId", orderId);

            NetClient.get(this, NetClient.URL_CALL_DETAIL, OrderDetailAck.class, requestParams, new NetResponseCallback(new OrderDetailResponseListener()));
        } catch (Exception e) {
            DLog.e(TAG, "requestOrderList exception", e);
        }
    }

    public class OrderDetailResponseListener implements NetResponseCallback.NetResponseListener {
        @Override
        public void onResponse(BaseAck result) {
            try {
                if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
                    OrderDetailAck ack = (OrderDetailAck) result;
                    mOrderData = ack.getCallDetail();

                    MyApp.getInstance().setAllocatedOrderData(mOrderData);

                    initialize();
                    initializeView();

                    startUpdateDistanceTimer();
                } else {
                    DialogHelper.showAlertDialog(AllocationActivity.this, "알림", "오더 정보 조회 요청이 실패했습니다.\n" + "code: " + result.getResult().getCode() + "detail: " + result.getResult().getDetail(), "확인", null, null);
                }
            } catch (Exception e) {
                DLog.e(TAG, "OrderDetailResponseListener::onResponse exception", e);
            }
        }
    }

    // 기사와 고객의 위치의 직선 거리를 주기적으로 갱신. 현재 고객 위치는 오더 수락할때의 위치값을 고정으로 사용한다
    private void startUpdateDistanceTimer() {
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                final double distance = Util.distanceBetweenLocation(MyApp.getInstance().getCurLocation(),
                    MyApp.getInstance().getAllocatedOrderData().getClientLocation().getLocation().getCoordinatesLatLng(),
                    DistanceUnit.METER);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mDistanceNoticeTextView.setText(String.format(getString(R.string.distance_notice_format),
                            Util.convertDistanceString((int) distance, true)));
                    }
                });
            }
        }, 0, 5000);
    }

    private void updateAllocationNoticeVisibility() {
        if (MyApp.getInstance().getDriverData().getmDriverCallInfoState() == DriverState.ALLOCATION) {
            mAllocationNoticeTextView.setVisibility(View.VISIBLE);

            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mAllocationNoticeTextView.setVisibility(View.GONE);
                        }
                    });
                }
            }, 3000);
        } else {
            mAllocationNoticeTextView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onFullScreenModeChanged(boolean isFullScreen) {
        if (isFullScreen) {
            mOrderInfoLayout.setVisibility(View.GONE);
            mFunctionLayout.setVisibility(View.GONE);
        } else {
            mOrderInfoLayout.setVisibility(View.VISIBLE);
            mFunctionLayout.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.cancel_allocation_button)
    public void onClickCancel() {
        try {
            String message = getString(R.string.cancel_order_popup_message);
            boolean isPaidCancel = Util.isAvailableModify(mOrderData.getDriver().getCallUpdateTime());

            if (isPaidCancel) {
                message = getString(R.string.cancel_order_popup_message1);
            }

            DialogHelper.showAlertDialog(this,
                getString(R.string.cancel_order_popup_title),
                message,
                getString(R.string.common_confirm),
                getString(R.string.common_cancel),
                new DialogHelper.AlertDialogButtonCallback() {
                    @Override
                    public void onClickButton(boolean isPositiveClick) {
                        if (isPositiveClick) {
                            requestCancelCall();
                        }
                    }
                });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestCancelCall() {
        // 배차 취소 서버 연동
        RequestParams requestParams = new RequestParams();
        requestParams.put("driverId", MyApp.getInstance().getDriverData().getDriverId());
        requestParams.put("callId", MyApp.getInstance().getAllocatedOrderData().getCallId());

        NetClient.post(this, NetClient.URL_CANCEL_CALL, CancelOrderAck.class, requestParams, new NetResponseCallback(new NetResponseCallback.NetResponseListener() {
            @Override
            public void onResponse(BaseAck result) {
                processCancelCallResponse(result);
            }
        }));

//        processCancelCallResponse(new CancelOrderAck(new NetResultData(NetResultCode.SUCCESS, null), DriverState.IDLE));
    }

    private void processCancelCallResponse(BaseAck result) {
        if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
            CancelOrderAck ack = (CancelOrderAck) result;
            MyApp.getInstance().getDriverData().setmDriverCallInfoState(DriverState.IDLE);

            Intent intent = new Intent(AllocationActivity.this, OrderListActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent); // OrderListActivity의 onNewIntent가 호출된다
        } else {
            DialogHelper.showAlertDialog(this, "알림", "배차 취소 요청이 실패했습니다.\n" + "code: " + result.getResult().getCode() + "detail: " + result.getResult().getDetail(), "확인", null, null);
        }
    }

    @OnClick(R.id.start_driving_button)
    public void onClickStartDriving() {
        if (MyApp.getInstance().getDriverData().getmDriverCallInfoState() != DriverState.ALLOCATION) {
            DialogHelper.showAlertDialog(this,
                getString(R.string.start_driving_popup_title),
                getString(R.string.start_driving_popup_unpaid_message),
                getString(R.string.common_close),
                null,
                null);
            return;
        }

        DialogHelper.showAlertDialog(this,
            getString(R.string.start_driving_popup_title),
            getString(R.string.start_driving_popup_message),
            getString(R.string.start_driving_button_title),
            getString(R.string.common_cancel),
            new DialogHelper.AlertDialogButtonCallback() {
                @Override
                public void onClickButton(boolean isPositiveClick) {
                    if (isPositiveClick) {
                        RequestParams requestParams = TMapHelper.generateTMapRouteRequestParams(mOrderData.getStartLocation().getLocation().getCoordinatesLatLng(), mOrderData.getEndLocation().getLocation().getCoordinatesLatLng(), mOrderData.getThroughLocationLatLngList());
                        NetClient.postTMapRoute(mContext, requestParams, new NetClient.TMapRouteResponseCallback() {
                            @Override
                            public void onResponse(TMapRouteData result) {
                                if (result != null) {
                                    mEstimateTime = (int) result.getTotalTime();
                                }

                                requestStartDriving();
                            }
                        });
                    }
                }
            });
    }

    private void requestStartDriving() {
        //  운행 시작 서버 연동
        RequestParams requestParams = new RequestParams();
        requestParams.put("driverId", MyApp.getInstance().getDriverData().getDriverId());
        requestParams.put("callId", MyApp.getInstance().getAllocatedOrderData().getCallId());
        requestParams.put("departureTime", System.currentTimeMillis());
        requestParams.put("estimateTime", System.currentTimeMillis() + (mEstimateTime * 1000));
        NetClient.post(this, NetClient.URL_START_DRIVING, StartDrivingAck.class, requestParams, new NetResponseCallback(new NetResponseCallback.NetResponseListener() {
            @Override
            public void onResponse(BaseAck result) {
                processStartDrivingResponse(result);
            }
        }));

//        processStartDrivingResponse(new StartDrivingAck(new NetResultData(NetResultCode.SUCCESS, null), DriverState.DRIVING));
    }

    private void processStartDrivingResponse(BaseAck result) {
        if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
            StartDrivingAck ack = (StartDrivingAck) result;
            requestInsuranceStartDriving();
//            MyApp.getInstance().getDriverData().setmDriverCallInfoState(DriverState.DRIVING);
//            updateDrivingButtonVisibility();
//            mMapControlFragment.changeMapViewType(MapViewType.PATH_FROM_START_TO_END);
        } else {
            DialogHelper.showAlertDialog(this, "알림", "운행 시작 요청이 실패했습니다.\n" + "code: " + result.getResult().getCode() + "detail: " + result.getResult().getDetail(), "확인", null, null);
        }
    }

    private void requestInsuranceStartDriving() {
        try {
            JSONArray requestDataArr = new JSONArray();
            JSONObject requestData = new JSONObject();

            requestData.put("apiKey", Defines.INSURANCE_API_KEY);
            requestData.put("driving_mode", "1");
            requestData.put("api_driving_id", MyApp.getInstance().getDriverData().getApiDriverId());
            requestData.put("biz_driving_id", MyApp.getInstance().getAllocatedOrderData().getUniqueId());
            requestData.put("api_driver_id", MyApp.getInstance().getDriverData().getApiDriverId());
            requestData.put("call_type", "1");
            requestData.put("driver_cell", AES256Cipher.AES_Encode(MyApp.getInstance().getDriverData().getPhone(), Defines.INSURANCE_CRYPT_KEY));
            requestData.put("order_maker", "");
            requestData.put("order_maker_call", "");
            requestData.put("order_receiver", "");
            requestData.put("order_receiver_call", "");
            requestData.put("start_address", MyApp.getInstance().getAllocatedOrderData().getStartLocation().getAddress());
            requestData.put("start_gps", "");
            requestData.put("target_address", MyApp.getInstance().getAllocatedOrderData().getEndLocation().getAddress());
            requestData.put("target_gps", "");
            requestData.put("client_cell", MyApp.getInstance().getAllocatedOrderData().getUserPhone());
            requestData.put("client_car_type", "");
            requestData.put("client_car_model", "");
            requestData.put("driving_time", Util.getYYYYMMDDHHmmss());

            requestDataArr.put(requestData);

            RequestParams requestParams = new RequestParams();
            requestParams.add("apiKey", Defines.INSURANCE_API_KEY);
            requestParams.add("request_data", requestDataArr.toString());

            NetClient.postInsuranceDriving(mContext, requestParams, new NetClient.InsuranceAPIResponseCallback() {
                @Override
                public void onResponse(InsuranceAPIData result) {
                    if (result != null) {
                        if(result.getMessage() != null && result.getMessage().equals("success")) {
                            MyApp.getInstance().getDriverData().setmDriverCallInfoState(DriverState.DRIVING);
                            updateDrivingButtonVisibility();
                            mMapControlFragment.changeMapViewType(MapViewType.PATH_FROM_START_TO_END);

                            MyApp.getInstance().getAllocatedOrderData().setApiDrivingId(result.getApi_driving_id());
                            MyApp.getInstance().getAllocatedOrderData().setPremiums(result.getPremiums());

                            requestSaveReqStartDrivingLog(result.getApi_driving_id(), result.getPremiums());
                        } else {
                            CommonDialog.showSimpleDialog(mContext, "보험 운행 정보 전송에 실패했습니다.\n관리자에게 문의해주세요.");/**/
                        }
                    } else {
                        CommonDialog.showSimpleDialog(mContext, "보험 운행 정보 전송에 실패했습니다.\n관리자에게 문의해주세요.");/**/
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestSaveReqStartDrivingLog(String apiDrivingId, String premiums) {
        try {
            RequestParams requestParams = new RequestParams();
            requestParams.add("apiKey", Defines.INSURANCE_API_KEY);
            requestParams.add("driving_mode", "1");
            requestParams.add("api_driving_id", "");
            requestParams.add("biz_driving_id", MyApp.getInstance().getAllocatedOrderData().getUniqueId());
            requestParams.add("api_driver_id", MyApp.getInstance().getDriverData().getApiDriverId());
            requestParams.add("call_type", "1");
            requestParams.add("driver_cell", AES256Cipher.AES_Encode(MyApp.getInstance().getDriverData().getPhone(), Defines.INSURANCE_CRYPT_KEY));
            requestParams.add("order_maker", "");
            requestParams.add("order_maker_call", "");
            requestParams.add("order_receiver", "");
            requestParams.add("order_receiver_call", "");
            requestParams.add("start_address", MyApp.getInstance().getAllocatedOrderData().getStartLocation().getAddress());
            requestParams.add("start_gps", "");
            requestParams.add("target_address", MyApp.getInstance().getAllocatedOrderData().getEndLocation().getAddress());
            requestParams.add("target_gps", "");
            requestParams.add("client_cell", MyApp.getInstance().getAllocatedOrderData().getUserPhone());
            requestParams.add("client_car_type", "");
            requestParams.add("client_car_model", "");
            requestParams.add("driving_time", Util.getYYYYMMDDHHmmss());
            requestParams.add("callId", MyApp.getInstance().getAllocatedOrderData().getCallId());
            requestParams.add("apiDrivingId", apiDrivingId);
            requestParams.add("premiums", premiums);

            NetClient.post(this, NetClient.URL_SAVE_REQ_DRIVING_LOG, BaseAck.class, requestParams, new NetResponseCallback(new SaveReqStartDrivingLogResponseListener()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class SaveReqStartDrivingLogResponseListener implements NetResponseCallback.NetResponseListener {
        @Override
        public void onResponse(BaseAck result) {
            try {
                if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
                } else {
                    DLog.e(TAG, "SaveReqStartDrivingLogResponseListener::onResponse : " + result.getResult().getCode());
                }
            } catch (Exception e) {
                DLog.e(TAG, "SaveReqStartDrivingLogResponseListener::onResponse exception", e);
            }
        }
    }

    private void updateCallClientButtonVisibility() {
        if (MyApp.getInstance().getDriverData().getmDriverCallInfoState() == DriverState.ALLOCATION) {
            mCallClientButton.setVisibility(View.VISIBLE);
        } else {
            mCallClientButton.setVisibility(View.GONE);
        }
    }

    private void updateDrivingButtonVisibility() {
        if (MyApp.getInstance().getDriverData().getmDriverCallInfoState() == DriverState.DRIVING) {
            mStartDrivingButton.setVisibility(View.GONE);
            mEndDrivingButton.setVisibility(View.VISIBLE);
        } else {
            mStartDrivingButton.setVisibility(View.VISIBLE);
            mEndDrivingButton.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.call_client_button)
    public void onClickCallClient() {
        try {
            requestRegisterVirtualNumber(mOrderData.getCallId());
//            Util.makePhoneCall(this, mOrderData.getClientPhoneNumber());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.end_driving_button)
    public void onClickEndDriving() {
        DialogHelper.showAlertDialog(this,
            getString(R.string.end_driving_popup_title),
            getString(R.string.end_driving_popup_message),
            getString(R.string.common_complete),
            getString(R.string.common_cancel),
            new DialogHelper.AlertDialogButtonCallback() {
                @Override
                public void onClickButton(boolean isPositiveClick) {
                    if (isPositiveClick) {
                        requestEndDriving();
                    }
                }
            });
    }

    private void requestRegisterVirtualNumber(String _callId) {
        RequestParams requestParams = new RequestParams();
        requestParams.put("recvId", _callId);
        requestParams.put("sendId", MyApp.getInstance().getDriverData().getDriverId());
        requestParams.add("type", "call");
        NetClient.post(this, NetClient.URL_VIRTUAL_NUMBER_REGISTER, BaseAck.class, requestParams, new NetResponseCallback(new NetResponseCallback.NetResponseListener() {
            @Override
            public void onResponse(BaseAck result) {
                processRegisterVirtualNumberResponse(result);
            }
        }));
    }

    private void processRegisterVirtualNumberResponse(BaseAck result) {
        if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
//            Intent intent = new Intent(this, OrderListActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent); // OrderListActivity의 onNewIntent가 호출된다
        } else {
            DialogHelper.showAlertDialog(this, "알림", "운행 종료 요청이 실패했습니다.\n" + "code: " + result.getResult().getCode() + "detail: " + result.getResult().getDetail(), "확인", null, null);
        }
    }

    private void requestUnregisterVirtualNumber() {
        RequestParams requestParams = new RequestParams();
        requestParams.put("sendId", MyApp.getInstance().getDriverData().getDriverId());
        requestParams.add("virtual_number", mVirtualNumber);
        requestParams.add("type", "call");
        NetClient.post(this, NetClient.URL_VIRTUAL_NUMBER_UNREGISTER, BaseAck.class, requestParams, new NetResponseCallback(new NetResponseCallback.NetResponseListener() {
            @Override
            public void onResponse(BaseAck result) {
                processUnregisterVirtualNumberResponse(result);
            }
        }));
    }

    private void processUnregisterVirtualNumberResponse(BaseAck result) {
        if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
//            Intent intent = new Intent(this, OrderListActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent); // OrderListActivity의 onNewIntent가 호출된다
        } else {
            DialogHelper.showAlertDialog(this, "알림", "운행 종료 요청이 실패했습니다.\n" + "code: " + result.getResult().getCode() + "detail: " + result.getResult().getDetail(), "확인", null, null);
        }
    }
    private void requestEndDriving() {
        RequestParams requestParams = new RequestParams();
        requestParams.put("driverId", MyApp.getInstance().getDriverData().getDriverId());
        requestParams.put("callId", MyApp.getInstance().getAllocatedOrderData().getCallId());
        NetClient.post(this, NetClient.URL_END_DRIVING, EndDrivingAck.class, requestParams, new NetResponseCallback(new NetResponseCallback.NetResponseListener() {
            @Override
            public void onResponse(BaseAck result) {
                processEndDrivingResponse(result);
            }
        }));

//        processEndDrivingResponse(new EndDrivingAck(new NetResultData(NetResultCode.SUCCESS, null), DriverState.IDLE));
    }

    private void processEndDrivingResponse(BaseAck result) {
        if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
            EndDrivingAck ack = (EndDrivingAck) result;

            requestInsuranceEndDriving();
//            MyApp.getInstance().getDriverData().setmDriverCallInfoState(DriverState.IDLE);
//
//            Intent intent = new Intent(this, OrderListActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent); // OrderListActivity의 onNewIntent가 호출된다
        } else {
            DialogHelper.showAlertDialog(this, "알림", "운행 종료 요청이 실패했습니다.\n" + "code: " + result.getResult().getCode() + "detail: " + result.getResult().getDetail(), "확인", null, null);
        }
    }

    private void requestInsuranceEndDriving() {
        try {
            JSONArray requestDataArr = new JSONArray();
            JSONObject requestData = new JSONObject();

            requestData.put("apiKey", Defines.INSURANCE_API_KEY);
            requestData.put("driving_mode", "2");
            requestData.put("api_driving_id", MyApp.getInstance().getAllocatedOrderData().getApiDrivingId());
            requestData.put("biz_driving_id", MyApp.getInstance().getAllocatedOrderData().getUniqueId());
            requestData.put("api_driver_id", MyApp.getInstance().getDriverData().getApiDriverId());
            requestData.put("call_type", "1");
            requestData.put("driver_cell", AES256Cipher.AES_Encode(MyApp.getInstance().getDriverData().getPhone(), Defines.INSURANCE_CRYPT_KEY));
            requestData.put("order_maker", "");
            requestData.put("order_maker_call", "");
            requestData.put("order_receiver", "");
            requestData.put("order_receiver_call", "");
            requestData.put("start_address", MyApp.getInstance().getAllocatedOrderData().getStartLocation().getAddress());
            requestData.put("start_gps", "");
            requestData.put("target_address", MyApp.getInstance().getAllocatedOrderData().getEndLocation().getAddress());
            requestData.put("target_gps", "");
            requestData.put("client_cell", MyApp.getInstance().getAllocatedOrderData().getUserPhone());
            requestData.put("client_car_type", "");
            requestData.put("client_car_model", "");
            requestData.put("driving_time", Util.getYYYYMMDDHHmmss());

            requestDataArr.put(requestData);

            RequestParams requestParams = new RequestParams();
            requestParams.add("apiKey", Defines.INSURANCE_API_KEY);
            requestParams.add("request_data", requestDataArr.toString());

            NetClient.postInsuranceDriving(mContext, requestParams, new NetClient.InsuranceAPIResponseCallback() {
                @Override
                public void onResponse(InsuranceAPIData result) {
                    if (result != null) {
                        if(result.getMessage() != null && result.getMessage().equals("success")) {
                            MyApp.getInstance().getDriverData().setmDriverCallInfoState(DriverState.IDLE);

                            requestSaveReqEndDrivingLog(result.getApi_driving_id(), result.getPremiums());
                        } else {
                            CommonDialog.showSimpleDialog(mContext, "보험 운행 종료 정보 전송에 실패했습니다.\n관리자에게 문의해주세요.");/**/
                        }
                    } else {
                        CommonDialog.showSimpleDialog(mContext, "보험 운행 종료 정보 전송에 실패했습니다.\n관리자에게 문의해주세요.");/**/
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestSaveReqEndDrivingLog(String apiDrivingId, String premiums) {
        try {
            RequestParams requestParams = new RequestParams();
            requestParams.add("apiKey", Defines.INSURANCE_API_KEY);
            requestParams.add("driving_mode", "2");
            requestParams.add("api_driving_id", MyApp.getInstance().getAllocatedOrderData().getApiDrivingId());
            requestParams.add("biz_driving_id", MyApp.getInstance().getAllocatedOrderData().getCallId());
            requestParams.add("api_driver_id", MyApp.getInstance().getDriverData().getApiDriverId());
            requestParams.add("call_type", "1");
            requestParams.add("driver_cell", AES256Cipher.AES_Encode(MyApp.getInstance().getDriverData().getPhone(), Defines.INSURANCE_CRYPT_KEY));
            requestParams.add("order_maker", "");
            requestParams.add("order_maker_call", "");
            requestParams.add("order_receiver", "");
            requestParams.add("order_receiver_call", "");
            requestParams.add("start_address", MyApp.getInstance().getAllocatedOrderData().getStartLocation().getAddress());
            requestParams.add("start_gps", "");
            requestParams.add("target_address", MyApp.getInstance().getAllocatedOrderData().getEndLocation().getAddress());
            requestParams.add("target_gps", "");
            requestParams.add("client_cell", MyApp.getInstance().getAllocatedOrderData().getUserPhone());
            requestParams.add("client_car_type", "");
            requestParams.add("client_car_model", "");
            requestParams.add("driving_time", Util.getYYYYMMDDHHmmss());
            requestParams.add("callId", MyApp.getInstance().getAllocatedOrderData().getCallId());
            requestParams.add("apiDrivingId", apiDrivingId);
            requestParams.add("premiums", premiums);

            NetClient.post(this, NetClient.URL_SAVE_REQ_DRIVING_LOG, BaseAck.class, requestParams, new NetResponseCallback(new SaveReqEndDrivingLogResponseListener()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class SaveReqEndDrivingLogResponseListener implements NetResponseCallback.NetResponseListener {
        @Override
        public void onResponse(BaseAck result) {
            try {
                if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
                    Intent intent = new Intent(AllocationActivity.this, OrderListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent); // OrderListActivity의 onNewIntent가 호출된다
                } else {
                    DLog.e(TAG, "SaveReqEndDrivingLogResponseListener::onResponse : " + result.getResult().getCode());
                }
            } catch (Exception e) {
                DLog.e(TAG, "SaveReqEndDrivingLogResponseListener::onResponse exception", e);
            }
        }
    }

    @Override
    public void onBackPressed() {
        finishAndRemoveTask();
    }

    @Override
    public void onClickMainMenu(@Enums.MainMenuType int mainMenuType) {
        try {
            switch (mainMenuType) {
                case Enums.MainMenuType.ALLOCATION: {
                }
                break;
                case Enums.MainMenuType.QUICK_SETTING: {
                    // TODO: 빠른설정 연동
                }
                break;
                case Enums.MainMenuType.SETTING: {
                    DialogHelper.showAlertDialog(this,
                        "알림",
                        "준비중입니다",
                        getString(R.string.common_confirm),
                        null,
                        null);
                }
                break;
                case Enums.MainMenuType.SETTLEMENT: {
                    Intent intent = new Intent(this, SettlementActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                break;
                default:
                    throw new RuntimeException("OnClickMainMenu - invalid main menu type: " + mainMenuType);
            }
        } catch (Exception e) {
            DLog.e(TAG, "OnClickTab exception", e);
        }
    }

    private class PushAlertReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                String action = intent.getAction();

                switch (action) {
                    case Defines.PUSH_INTENT_ACTION_VIRTUAL_NUMBER_RECEIVED:
                        mVirtualNumber = intent.getStringExtra("virtualNumber");

                        if (!Util.isStringNullOrEmpty(mVirtualNumber)) {
                            mIsSecretCalling = true;
                            Util.makePhoneCall(getApplicationContext(), mVirtualNumber);
                        }

                        break;
                    case Defines.PUSH_INTENT_ACTION_INCOMING_CALL:
                        if (intent != null) {
                            String call_state = intent.getStringExtra("call_state");

                            if (mIsSecretCalling && call_state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                                requestUnregisterVirtualNumber();
                            }
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        try {
            mPushAlertReceiver = new PushAlertReceiver();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Defines.PUSH_INTENT_ACTION_VIRTUAL_NUMBER_RECEIVED);
            intentFilter.addAction(Defines.PUSH_INTENT_ACTION_INCOMING_CALL);

            registerReceiver(mPushAlertReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        try {
            unregisterReceiver(mPushAlertReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
