package kr.driver.cardari.cardari.common;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import java.util.Calendar;
import java.util.Date;

import kr.driver.cardari.cardari.logger.DLog;

/**
 * Created by eklee on 2017. 7. 12..
 */

public class DialogHelper {
    private static final String TAG = "DialogHelper";

    // region - Alert Dialog
    public interface AlertDialogButtonCallback {
        void onClickButton(boolean isPositiveClick);
    }

    public static AlertDialog showAlertDialog(Context context,
                                              String title,
                                              String message,
                                              String positiveButtonTitle,
                                              String negativeButtonTitle,
                                              final AlertDialogButtonCallback callback) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(title).setMessage(message);

            if (!Util.isStringNullOrEmpty(positiveButtonTitle)) {
                builder.setPositiveButton(positiveButtonTitle, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (callback != null) {
                            callback.onClickButton(true);
                        }
                        dialog.dismiss();
                    }
                });
            }

            if (!Util.isStringNullOrEmpty(negativeButtonTitle)) {
                builder.setNegativeButton(negativeButtonTitle, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (callback != null) {
                            callback.onClickButton(false);
                        }
                        dialog.dismiss();
                    }
                });
            }

            return builder.show();
        } catch (Exception e) {
            DLog.e(TAG, "showAlertDialog exception", e);
        }

        return null;
    }
    // endregion - Alert Dialog

    // region - DatePicker Dialog
    public static DatePickerDialog showDatePickerDialog(Context context,
                                                        int year,
                                                        int month,
                                                        int dayOfMonth,
                                                        DatePickerDialog.OnDateSetListener onDateSetListener) {
        try {
            DatePickerDialog dialog = new DatePickerDialog(context, android.R.style.Theme_Material_Dialog, onDateSetListener, year, month, dayOfMonth);
            dialog.show();
            return dialog;
        } catch (Exception e) {
            DLog.e(TAG, "showDatePickerDialog exception", e);
        }

        return null;
    }

    public static DatePickerDialog showDatePickerDialog(Context context,
                                                        Date initialDate,
                                                        DatePickerDialog.OnDateSetListener onDateSetListener) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(initialDate);

            return showDatePickerDialog(context, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), onDateSetListener);
        } catch (Exception e) {
            DLog.e(TAG, "showDatePickerDialog exception", e);
        }

        return null;
    }

    public static DatePickerDialog showDatePickerDialog(Context context,
                                                        Calendar initialCalendar,
                                                        DatePickerDialog.OnDateSetListener onDateSetListener) {
        try {
            return showDatePickerDialog(context, initialCalendar.get(Calendar.YEAR), initialCalendar.get(Calendar.MONTH), initialCalendar.get(Calendar.DAY_OF_MONTH), onDateSetListener);
        } catch (Exception e) {
            DLog.e(TAG, "showDatePickerDialog exception", e);
        }

        return null;
    }

    public static DatePickerDialog showDatePickerDialog(Context context,
                                                        long dateTime,
                                                        DatePickerDialog.OnDateSetListener onDateSetListener) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(dateTime);
            return showDatePickerDialog(context, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), onDateSetListener);
        } catch (Exception e) {
            DLog.e(TAG, "showDatePickerDialog exception", e);
        }

        return null;
    }
    // endregion - DatePicker Dialog
}
