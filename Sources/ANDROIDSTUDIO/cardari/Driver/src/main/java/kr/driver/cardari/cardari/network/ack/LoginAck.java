package kr.driver.cardari.cardari.network.ack;

import kr.driver.cardari.cardari.data.CallInfoData;
import kr.driver.cardari.cardari.data.DriverData;

/**
 * Created by eklee on 2017. 11. 1..
 */

public class LoginAck extends BaseAck {
    private DriverData driver;
    private CallInfoData callInfo;

    public LoginAck() {
    }

    public LoginAck(DriverData driver, CallInfoData callInfo) {
        this.driver = driver;
        this.callInfo = callInfo;
    }

    public LoginAck(NetResultData result, DriverData driver, CallInfoData callInfo) {
        super(result);
        this.driver = driver;
        this.callInfo = callInfo;
    }

    public DriverData getDriver() {
        return driver;
    }

    public void setDriver(DriverData driver) {
        this.driver = driver;
    }

    public CallInfoData getCallInfo() {
        return callInfo;
    }

    public void setCallInfo(CallInfoData callInfo) {
        this.callInfo = callInfo;
    }
}
