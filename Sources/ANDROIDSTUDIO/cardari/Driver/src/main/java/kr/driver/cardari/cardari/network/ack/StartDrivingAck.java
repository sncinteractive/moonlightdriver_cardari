package kr.driver.cardari.cardari.network.ack;

import kr.driver.cardari.cardari.common.Enums;

import static kr.driver.cardari.cardari.common.Enums.*;

/**
 * Created by eklee on 2017. 11. 1..
 */

public class StartDrivingAck extends BaseAck {
    private @DriverState int driverState;

    public StartDrivingAck() {
    }

    public StartDrivingAck(NetResultData result, int driverState) {
        super(result);
        this.driverState = driverState;
    }

    public int getDriverState() {
        return driverState;
    }

    public void setDriverState(int driverState) {
        this.driverState = driverState;
    }
}
