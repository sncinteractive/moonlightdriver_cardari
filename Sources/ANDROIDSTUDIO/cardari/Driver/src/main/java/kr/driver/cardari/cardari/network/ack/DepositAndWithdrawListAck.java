package kr.driver.cardari.cardari.network.ack;

import java.util.List;

import kr.driver.cardari.cardari.data.DepositAndWithdrawData;

/**
 * Created by eklee on 2017. 11. 1..
 */

public class DepositAndWithdrawListAck extends BaseAck {
    private List<DepositAndWithdrawData> depositAndWithdrawDataList;

    public DepositAndWithdrawListAck() {
    }

    public DepositAndWithdrawListAck(NetResultData result, List<DepositAndWithdrawData> depositAndWithdrawDataList) {
        super(result);
        this.depositAndWithdrawDataList = depositAndWithdrawDataList;
    }

    public List<DepositAndWithdrawData> getDepositAndWithdrawDataList() {
        return depositAndWithdrawDataList;
    }

    public void setDepositAndWithdrawDataList(List<DepositAndWithdrawData> depositAndWithdrawDataList) {
        this.depositAndWithdrawDataList = depositAndWithdrawDataList;
    }
}
