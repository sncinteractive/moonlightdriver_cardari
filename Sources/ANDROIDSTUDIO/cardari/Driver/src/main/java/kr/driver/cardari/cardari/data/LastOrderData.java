package kr.driver.cardari.cardari.data;

/**
 * Created by eklee on 2017. 11. 28..
 */

public class LastOrderData {
    private long dateTime; // millisecond
    private int payamount;
    private String startPointName;
    private String endPointName;
    private String summary;
    private String centerName;
    private String centerPhoneNumber;

    public LastOrderData() {
    }

    public LastOrderData(long dateTime, int payamount, String startPointName, String endPointName, String summary, String centerName, String centerPhoneNumber) {
        this.payamount = payamount;
        this.dateTime = dateTime;
        this.startPointName = startPointName;
        this.endPointName = endPointName;
        this.summary = summary;
        this.centerName = centerName;
        this.centerPhoneNumber = centerPhoneNumber;
    }

    public int getPayamount() {
        return payamount;
    }

    public void setPayamount(int payamount) {
        this.payamount = payamount;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    public String getStartPointName() {
        return startPointName;
    }

    public void setStartPointName(String startPointName) {
        this.startPointName = startPointName;
    }

    public String getEndPointName() {
        return endPointName;
    }

    public void setEndPointName(String endPointName) {
        this.endPointName = endPointName;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    public String getCenterPhoneNumber() {
        return centerPhoneNumber;
    }

    public void setCenterPhoneNumber(String centerPhoneNumber) {
        this.centerPhoneNumber = centerPhoneNumber;
    }
}
