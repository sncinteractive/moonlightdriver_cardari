package kr.driver.cardari.cardari.network.ack;

import java.util.List;

import kr.driver.cardari.cardari.data.OrderData;

/**
 * Created by eklee on 2017. 11. 1..
 */

public class OrderListAck extends BaseAck {
    private List<OrderData> orderDataList;

    public OrderListAck() {
    }

    public OrderListAck(NetResultData result, List<OrderData> orderDataList) {
        super(result);
        this.orderDataList = orderDataList;
    }

    public List<OrderData> getOrderDataList() {
        return orderDataList;
    }

    public void setOrderDataList(List<OrderData> orderDataList) {
        this.orderDataList = orderDataList;
    }
}
