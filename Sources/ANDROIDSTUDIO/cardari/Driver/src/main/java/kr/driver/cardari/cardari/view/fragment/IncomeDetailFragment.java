package kr.driver.cardari.cardari.view.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import kr.driver.cardari.cardari.MyApp;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.DialogHelper;
import kr.driver.cardari.cardari.common.Enums.NetResultCode;
import kr.driver.cardari.cardari.common.Util;
import kr.driver.cardari.cardari.logger.DLog;
import kr.driver.cardari.cardari.network.NetClient;
import kr.driver.cardari.cardari.network.NetResponseCallback;
import kr.driver.cardari.cardari.network.ack.BaseAck;
import kr.driver.cardari.cardari.network.ack.IncomeQueryAck;

import static kr.driver.cardari.cardari.common.Defines.EXTRA_KEY_INCOME_DATE_TYPE;
import static kr.driver.cardari.cardari.common.Enums.IncomeDateType;

/**
 * Created by eklee on 2017. 11. 21..
 */

public class IncomeDetailFragment extends Fragment {
    public interface EventListener {
        void needToShowLoadingProgressbar();

        void needToHideLoadingProgressbar();

        void onReceivedIncomeQueryAck(IncomeQueryAck ack);
    }

    private static final String TAG = "IncomeDetailFragment";

    @BindView(R.id.date_button) Button mDateButton;
    @BindView(R.id.balance_text_view) TextView mBalanceTextView;
    @BindView(R.id.completion_total_count_text_view) TextView mTotalCountTextView;
    @BindView(R.id.total_driving_charge_text_view) TextView mTotalDrivingChargeTextView;
    @BindView(R.id.deposit_amount_text_view) TextView mDepositAmountTextView;
    @BindView(R.id.penalty_total_charge_text_view) TextView mTotalChargeTextView;
    @BindView(R.id.total_income_text_view) TextView mTotalIncomeTextView;

    private Unbinder mUnBinder;
    private EventListener mEventListener;

    private @IncomeDateType int mIncomeDateType;
    private Calendar mCalendar;

    public static IncomeDetailFragment createInstance(@IncomeDateType int incomeDateType, EventListener eventListener) {
        IncomeDetailFragment fragment = new IncomeDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_KEY_INCOME_DATE_TYPE, incomeDateType);
        fragment.setArguments(bundle);
        fragment.setEventListener(eventListener);
        return fragment;
    }

    public void setEventListener(EventListener eventListener) {
        mEventListener = eventListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = null;
        try {
            rootView = inflater.inflate(R.layout.fragment_income_detail, container, false);
            mUnBinder = ButterKnife.bind(this, rootView);

            initialize();
            initializeView();
        } catch (Exception e) {
            DLog.e(TAG, "onCreateView exception", e);
        }

        return rootView;
    }

    private void initialize() {
        Bundle bundle = getArguments();
        mIncomeDateType = bundle.getInt(EXTRA_KEY_INCOME_DATE_TYPE, IncomeDateType.DAILY);

        mCalendar = Calendar.getInstance();
        mCalendar.setTime(new Date());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mEventListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnBinder.unbind();
    }

    private void initializeView() {
        mBalanceTextView.setText(null);
        mTotalCountTextView.setText(null);
        mTotalDrivingChargeTextView.setText(null);
        mDepositAmountTextView.setText(null);
        mTotalChargeTextView.setText(null);
        mTotalIncomeTextView.setText(null);

        applyDateDataToUI();
    }

    private void applyDateDataToUI() {
        try {
            switch (mIncomeDateType) {
                case IncomeDateType.MONTHLY:
                    mDateButton.setText(Util.convertYearMonthString(mCalendar));
                    break;
                default:
                    mDateButton.setText(Util.convertYearMonthDayString(mCalendar));
                    break;
            }
        } catch (Exception e) {
            DLog.e(TAG, "applyDateDataToUI exception", e);
        }
    }

    @OnClick(R.id.date_button)
    public void onClickDate() {
        DialogHelper.showDatePickerDialog(getContext(), mCalendar, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mCalendar.set(year, month, dayOfMonth);
                applyDateDataToUI();
            }
        });
    }

    @OnClick(R.id.query_button)
    public void onClickQuery() {
        requestIncomeQuery();
    }

    private void requestIncomeQuery() {
        try {
            mEventListener.needToShowLoadingProgressbar();

            // 수입 내역 조회 서버 연동
            RequestParams requestParams = new RequestParams();
            requestParams.put("driverId", MyApp.getInstance().getDriverData().getDriverId());
            requestParams.put("date", mDateButton.getText().toString());
            requestParams.put("dateType", mIncomeDateType);
            NetClient.post(getContext(), NetClient.URL_INCOME_LIST, IncomeQueryAck.class, requestParams, new NetResponseCallback(new NetResponseCallback.NetResponseListener() {
                @Override
                public void onResponse(BaseAck result) {
                    processIncomeQueryResponse(result);
                }
            }));

            // test code begin
//            IncomeQueryAck ack;
//            if (mIncomeDateType == IncomeDateType.DAILY) {
//                List<DailyIncomeData> completionList = new ArrayList<>();
//                completionList.add(new DailyIncomeData(1, "강남역", "대치동", 1255, 20000));
//                completionList.add(new DailyIncomeData(2, "대치동", "코엑스", 1500, 20000));
//                completionList.add(new DailyIncomeData(3, "코엑스", "목동이대병원", 2650, 25000));
//                completionList.add(new DailyIncomeData(4, "목동이대병원", "구로역", 950, 20000));
//                completionList.add(new DailyIncomeData(5, "구로역", "서울역", 2100, 25000));
//                completionList.add(new DailyIncomeData(6, "서울역", "수원역", 4650, 45000));
//                completionList.add(new DailyIncomeData(7, "수원역", "강남역", 4200, 30000));
//
//                List<DailyIncomeData> penaltyList = new ArrayList<>();
//                penaltyList.add(new DailyIncomeData(1, "강남역", "대치동", 1255, 20000));
//                penaltyList.add(new DailyIncomeData(2, "대치동", "코엑스", 1500, 20000));
//                penaltyList.add(new DailyIncomeData(3, "코엑스", "목동이대병원", 2650, 25000));
//
//                ack = new IncomeQueryAck(new NetResultData(NetResultCode.SUCCESS, null),
//                    7, 123000, 50000, 23000, 6, 126000, 1, 3000, completionList, penaltyList);
//            } else {
//                ack = new IncomeQueryAck(new NetResultData(NetResultCode.SUCCESS, null),
//                    7, 123000, 50000, 23000, 6, 126000, 1, 3000, null, null);
//            }
//            processIncomeQueryResponse(ack);
            // test code end
        } catch (Exception e) {
            DLog.e(TAG, "requestIncomeQuery exception", e);
            mEventListener.needToHideLoadingProgressbar();
        }
    }

    private void processIncomeQueryResponse(BaseAck result) {
        try {
            mEventListener.needToHideLoadingProgressbar();

            if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
                IncomeQueryAck ack = (IncomeQueryAck) result;
                applyIncomeDataToView(ack);
                mEventListener.onReceivedIncomeQueryAck(ack);
            } else {
                DialogHelper.showAlertDialog(getContext(), "알림", "일별 수입 요청이 실패했습니다.\n" + "code: " + result.getResult().getCode() + "detail: " + result.getResult().getDetail(), "확인", null, null);
            }
        } catch (Exception e) {
            DLog.e(TAG, "processIncomeQueryResponse exception", e);
        }
    }

    private void applyIncomeDataToView(IncomeQueryAck ack) {
        try {
            if (ack != null) {
                mBalanceTextView.setText(Util.convertNumberToCommaString(ack.getBalance(), true));
                mTotalCountTextView.setText(Util.convertNumberToCommaString(ack.getTotalCount()));
                mTotalDrivingChargeTextView.setText(Util.convertNumberToCommaString(ack.getCompletionTotalAmount(), true));
                mDepositAmountTextView.setText(Util.convertNumberToCommaString(ack.getDepositAmount(), true));
                mTotalChargeTextView.setText(Util.convertNumberToCommaString(ack.getPenaltyTotalAmount(), true));
                mTotalIncomeTextView.setText(Util.convertNumberToCommaString(ack.getTotalIncome(), true));
            }
        } catch (Exception e) {
            DLog.e(TAG, "applyIncomeDataToView exception", e);
        }
    }
}