package kr.driver.cardari.cardari.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by eklee on 2017. 11. 7..
 */

public class LocationData implements Parcelable{
    private LatLng location;
    private String name;
    private String address;

    public LocationData() {
    }

    public LocationData(LatLng location, String name, String address) {
        this.location = location;
        this.name = name;
        this.address = address;
    }

    protected LocationData(Parcel in) {
        location = in.readParcelable(LatLng.class.getClassLoader());
        name = in.readString();
        address = in.readString();
    }

    public static final Creator<LocationData> CREATOR = new Creator<LocationData>() {
        @Override
        public LocationData createFromParcel(Parcel in) {
            return new LocationData(in);
        }

        @Override
        public LocationData[] newArray(int size) {
            return new LocationData[size];
        }
    };

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(location, flags);
        dest.writeString(name);
        dest.writeString(address);
    }
}
