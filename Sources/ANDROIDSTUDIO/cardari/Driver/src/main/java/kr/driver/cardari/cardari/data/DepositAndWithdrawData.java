package kr.driver.cardari.cardari.data;

/**
 * Created by eklee on 2017. 11. 7..
 */

public class DepositAndWithdrawData {
    private long dateTime; // millisecond
    private int price;
    private int balance;
    private String summary;

    public DepositAndWithdrawData() {
    }

    public DepositAndWithdrawData(long dateTime, int price, int balance, String summary) {
        this.dateTime = dateTime;
        this.price = price;
        this.balance = balance;
        this.summary = summary;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
