package kr.driver.cardari.cardari.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.CommonDialog;
import kr.driver.cardari.cardari.common.Defines;

public class OkNameWebViewActivity extends AppCompatActivity {

    @BindView(R.id.webView)
    WebView mWebView;
//    @BindView(R.id.loadingProgress)
//    ProgressBar mLoadingProgress;

    private Unbinder mUnBinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        mUnBinder = ButterKnife.bind(this);

        initView();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void initView() {
        try {
            mWebView.addJavascriptInterface(new OkNameWebViewBridge(this), "Android");

            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.getSettings().setSavePassword(false); // 패스워드 저장 사용 안함
            mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            mWebView.getSettings().setDomStorageEnabled(true);
            mWebView.getSettings().setTextZoom(100);

            mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);

            mWebView.setLongClickable(false);
            if(Build.VERSION.SDK_INT >= 21) {
                mWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
            }
            mWebView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            mWebView.clearCache(true);
            mWebView.clearHistory();

            mWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                    return false;
                }
            });

            mWebView.setWebChromeClient(new WebChromeClient() {
                public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                    CommonDialog.showSimpleDialog(OkNameWebViewActivity.this, message);
                    return true;
                }

                @Override
                public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
                    CommonDialog.showDialogWithListener(OkNameWebViewActivity.this, "확인", message, "확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            result.confirm();
                        }
                    }, "취소", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            result.cancel();
                        }
                    });

                    return true;
                }
            });

            mWebView.loadUrl("http://115.68.122.108:8080/okname/popup2.php");

        } catch (Exception e) {
            Log.d("catch", e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    private class OkNameWebViewBridge {
        private Context mContext;

        public OkNameWebViewBridge(Context context) {
            this.mContext = context;
        }

        @JavascriptInterface
        public void callbackFromWeb(final String _ret, final String _phoneNumber, final String _ci, final String _di) {
            String message = "본인 인증에 성공했습니다.";
            if(!_ret.equals("OK")) {
                message = "본인 인증에 실패했습니다.[" + _ret + "]";
            }

            CommonDialog.showDialogWithListener(OkNameWebViewActivity.this, message, "확인", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent returnIntent = new Intent();

                    try {
                        if(_ret.equals("OK")) {
                            returnIntent.putExtra(Defines.EXTRA_KEY_PHONE_AUTH_RESULT, _ret);
                            returnIntent.putExtra(Defines.EXTRA_KEY_PHONE_AUTH_PHONE_NUMBER, _phoneNumber);
                            returnIntent.putExtra(Defines.EXTRA_KEY_PHONE_AUTH_CI, _ci);
                            returnIntent.putExtra(Defines.EXTRA_KEY_PHONE_AUTH_DI, _di);
                        } else {
                            returnIntent.putExtra(Defines.EXTRA_KEY_PHONE_AUTH_RESULT, _ret);
                            returnIntent.putExtra(Defines.EXTRA_KEY_PHONE_AUTH_PHONE_NUMBER, "");
                            returnIntent.putExtra(Defines.EXTRA_KEY_PHONE_AUTH_CI, "");
                            returnIntent.putExtra(Defines.EXTRA_KEY_PHONE_AUTH_DI, "");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                        returnIntent.putExtra(Defines.EXTRA_KEY_PHONE_AUTH_RESULT, _ret);
                        returnIntent.putExtra(Defines.EXTRA_KEY_PHONE_AUTH_PHONE_NUMBER, "");
                        returnIntent.putExtra(Defines.EXTRA_KEY_PHONE_AUTH_CI, "");
                        returnIntent.putExtra(Defines.EXTRA_KEY_PHONE_AUTH_DI, "");
                    }

                    setResult(Activity.RESULT_OK, returnIntent);

                    finish();
                }
            });
        }

        @JavascriptInterface
        public void close() {
            try {
                finish();
            } catch(Exception e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void onError(String _errorCode, String _errorMsg) {
            try {
                finish();
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUnBinder.unbind();
        mUnBinder = null;
    }
}
