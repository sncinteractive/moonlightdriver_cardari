package kr.driver.cardari.cardari.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.Defines;
import kr.driver.cardari.cardari.common.PrefsHelper;
import kr.driver.cardari.cardari.common.Util;
import kr.driver.cardari.cardari.data.OrderData;
import kr.driver.cardari.cardari.logger.DLog;

import static kr.driver.cardari.cardari.common.Enums.PaymentOption;
import static kr.driver.cardari.cardari.common.PrefsHelper.PREFS_KEY_DISTANCE_VISIBILITY;
import static kr.driver.cardari.cardari.common.PrefsHelper.PREFS_KEY_PAY_AMOUNT_VISIBILITY;

/**
 * Created by eklee on 2017. 11. 7..
 */

public class OrderListAdapter extends RecyclerView.Adapter {
    private static final String TAG = "OrderListAdapter";

    private List<OrderData> mOrderDataList;

    private int mTextSize;
    private int mRowHeight;
    private int mAuctionOrderColor;
    private int mNormalOrderColor;
    private int mRefusalOrderColor;
    private int mCardOrderColor;
    private int mModifyOrderColor;
    private int mBackgroundColor;

    private int mDistanceVisibility;
    private int mPayamountVisibility;

    public OrderListAdapter() {
        mTextSize = PrefsHelper.getInt(PrefsHelper.PREFS_KEY_TEXT_SIZE, Defines.DEFAULT_ORDER_TEXT_SIZE_MIN);
        mRowHeight = PrefsHelper.getInt(PrefsHelper.PREFS_KEY_ROW_HEIGHT, Defines.DEFAULT_ORDER_ROW_HEIGHT_MIN);
        mAuctionOrderColor = PrefsHelper.getInt(PrefsHelper.PREFS_KEY_AUCTION_ORDER_COLOR, Defines.DEFAULT_AUCTION_ORDER_COLOR);
        mNormalOrderColor = PrefsHelper.getInt(PrefsHelper.PREFS_KEY_NORMAL_ORDER_COLOR, Defines.DEFAULT_NORMAL_ORDER_COLOR);
        mRefusalOrderColor = PrefsHelper.getInt(PrefsHelper.PREFS_KEY_REFUSAL_ORDER_COLOR, Defines.DEFAULT_REFUSAL_ORDER_COLOR);
        mCardOrderColor = PrefsHelper.getInt(PrefsHelper.PREFS_KEY_CARD_ORDER_COLOR, Defines.DEFAULT_CARD_ORDER_COLOR);
        mModifyOrderColor = PrefsHelper.getInt(PrefsHelper.PREFS_KEY_CARD_ORDER_COLOR, Defines.DEFAULT_MODIFY_ORDER_COLOR);
        mBackgroundColor = PrefsHelper.getInt(PrefsHelper.PREFS_KEY_ORDER_BACKGROUND_COLOR, Defines.DEFAULT_ORDER_BACKGROUND_COLOR);

        mDistanceVisibility = PrefsHelper.getInt(PREFS_KEY_DISTANCE_VISIBILITY, View.VISIBLE);
        mPayamountVisibility = PrefsHelper.getInt(PREFS_KEY_PAY_AMOUNT_VISIBILITY, View.VISIBLE);
    }

    public void setDataList(List<OrderData> orderDataList) {
        mOrderDataList = orderDataList;

        notifyDataSetChanged();
    }

    public OrderData getItem(int position) {
        try {
            return mOrderDataList.get(position);
        } catch (Exception e) {
            DLog.e(TAG, "getItem exception", e);
        }

        return null;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_order_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).bind(mOrderDataList.get(position));
    }

    public void changeTextSize(int textSize) {
        mTextSize = textSize;
        notifyDataSetChanged();
    }

    public void changeRowHeight(int rowHeight) {
        mRowHeight = rowHeight;
        notifyDataSetChanged();
    }

    public void changeAuctionOrderColor(int auctionOrderColor) {
        mAuctionOrderColor = auctionOrderColor;
        notifyDataSetChanged();
    }

    public void changeNormalOrderColor(int normalOrderColor) {
        mNormalOrderColor = normalOrderColor;
        notifyDataSetChanged();
    }

    public void changeRefusalOrderColor(int refusalOrderColor) {
        mRefusalOrderColor = refusalOrderColor;
        notifyDataSetChanged();
    }

    public void changeBackgroundColor(int backgroundColor) {
        mBackgroundColor = backgroundColor;
        notifyDataSetChanged();
    }

    public void changeAllViewSettingProperties(int textSize, int rowHeight, int auctionOrderColor, int normalOrderColor, int refusalOrderColor, int backgroundColor) {
        mTextSize = textSize;
        mRowHeight = rowHeight;
        mAuctionOrderColor = auctionOrderColor;
        mNormalOrderColor = normalOrderColor;
        mRefusalOrderColor = refusalOrderColor;
        mBackgroundColor = backgroundColor;

        notifyDataSetChanged();
    }

    public void changeDistanceVisibility(int visibility) {
        mDistanceVisibility = visibility;
        notifyDataSetChanged();
    }

    public void changePayamountVisibility(int visibility) {
        mPayamountVisibility = visibility;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() { return (mOrderDataList != null ? mOrderDataList.size() : 0); }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.row_order_list_item_layout) LinearLayout mRowOrderListItemLayout;
        @BindView(R.id.row_order_list_item_card_icon_image_view) ImageView mCardIconImageView;
        @BindView(R.id.row_order_list_item_discount_icon_image_view) ImageView mDiscountIconImageView;
        @BindView(R.id.row_order_list_item_distance_text_view) TextView mDistanceTextView;
        @BindView(R.id.row_order_list_item_start_point_text_view) TextView mStartPointTextView;
        @BindView(R.id.row_order_list_item_end_point_text_view) TextView mEndPointTextView;

        @BindView(R.id.row_order_list_item_distance_layout) FrameLayout mDistanceLayout;
        @BindView(R.id.row_order_list_item_payamount_layout) LinearLayout mPayamountLayout;
        @BindView(R.id.row_order_list_item_payamount_text_view) TextView mPayamountTextView;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(OrderData orderData) {
            try {
                // 거리, 요금 필드 visibility
                mDistanceLayout.setVisibility(mDistanceVisibility);
                mPayamountLayout.setVisibility(mPayamountVisibility);

                // 행 높이 설정
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mRowOrderListItemLayout.getLayoutParams();
                layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, mRowHeight, itemView.getResources().getDisplayMetrics());
                mRowOrderListItemLayout.setLayoutParams(layoutParams);
                mRowOrderListItemLayout.setBackgroundColor(mBackgroundColor);

                // 텍스트 크기 설정
                mDistanceTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, mTextSize);
                mStartPointTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, mTextSize);
                mEndPointTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, mTextSize);
                mPayamountTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, mTextSize);

                // 텍스트 색상 설정
                int textColor = mNormalOrderColor;
                boolean isModifiable = Util.isAvailableModify(orderData.getCreatedTime());
                if(isModifiable) {
                    textColor = mModifyOrderColor;
                } else if(PaymentOption.CARD.equals(orderData.getPaymentOption()) || PaymentOption.DISC.equals(orderData.getPaymentOption())) {
                    textColor = mCardOrderColor;
                }

                mDistanceTextView.setTextColor(textColor);
                mStartPointTextView.setTextColor(textColor);
                mEndPointTextView.setTextColor(textColor);
                mPayamountTextView.setTextColor(textColor);

                // bind data
                mCardIconImageView.setVisibility(View.GONE);
                mDiscountIconImageView.setVisibility(View.GONE);
                if (PaymentOption.CARD.equals(orderData.getPaymentOption())) {
                    mCardIconImageView.setVisibility(View.VISIBLE);
                } else if (PaymentOption.DISC.equals(orderData.getPaymentOption())){
                    mDiscountIconImageView.setVisibility(View.VISIBLE);
                }

                String text;
                if(isModifiable) {
                    text = "[수정 " + Util.convertPriceToThousandString(orderData.getMoney()) + "] | ";
                } else {
                    if(orderData.getPaymentOption().equals(PaymentOption.CASH)) {
                        text = "";
                    } else {
                        text = Util.convertPriceToThousandString(orderData.getMoney()) + " | ";
                    }
                }

                text += orderData.getStartLocation().getAddress();
                mStartPointTextView.setText(text);

                mEndPointTextView.setText(orderData.getEndLocation().getAddress());

                mDistanceTextView.setText(Util.convertDistanceString((int)orderData.getDistance()));
                mPayamountTextView.setText(String.format("%sk", Integer.toString(orderData.getMoney() / 1000)));
            } catch (Exception e) {
                DLog.e(TAG, "ViewHolder::Bind exception", e);
            }
        }
    }
}
