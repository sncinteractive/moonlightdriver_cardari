package kr.driver.cardari.cardari.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

/**
 * Created by youngmin on 2016-11-24.
 */

public class IncomingCallReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context _context, Intent _intent){
        try{
            String state = _intent.getStringExtra(TelephonyManager.EXTRA_STATE);

            Intent incomingCallIntent = new Intent();
            incomingCallIntent.setAction(Defines.PUSH_INTENT_ACTION_INCOMING_CALL);
            incomingCallIntent.putExtra("call_state", state);

            _context.sendBroadcast(incomingCallIntent);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
