package kr.driver.cardari.cardari.common;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import kr.driver.cardari.cardari.logger.DLog;

/**
 * Created by youngmin on 2016-10-31.
 */

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {
    private static final String TAG = "GcmBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            ComponentName comp = new ComponentName(context.getPackageName(), GcmIntentService.class.getName());
            startWakefulService(context, (intent.setComponent(comp)));

            DLog.e(TAG, "receive : " + intent.getExtras().toString()); // 서버에서 어떻게 넘어올지 모르니 일단.. pass
            String pushMsgType = intent.getExtras().getString("type", null);
            if (Util.isStringNullOrEmpty(pushMsgType)) {
                return;
            }

            updateCallDriverInfoToSharedPreferences(context, intent);

            sendBroadcast(context, intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateCallDriverInfoToSharedPreferences(Context context, Intent intent) {
        try {
            String pushMsgType = intent.getStringExtra("type");
            String callStatus = intent.getStringExtra("callStatus");

            switch (pushMsgType) {
                case Defines.PUSH_MSG_TYPE_CATCH_CALL: {

                    PrefsHelper.putString(PrefsHelper.PREFS_KEY_LATEST_RECEIVED_PUSH_MSG_TYPE, pushMsgType);
                    PrefsHelper.putString(PrefsHelper.PREFS_KEY_CALL_ID, intent.getStringExtra("callId"));
                    PrefsHelper.putString(PrefsHelper.PREFS_KEY_BIDDING_PRICE, intent.getStringExtra("biddingPrice"));
                    PrefsHelper.putString(PrefsHelper.PREFS_KEY_CALL_STATUS, callStatus);
                }
                break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendBroadcast(Context context, Intent intent) {
        String pushMsgType = intent.getExtras().getString("type", "");
        switch (pushMsgType) {
            case Defines.PUSH_MSG_TYPE_CATCH_CALL: {
                Intent pushMsgIntent = new Intent();
                pushMsgIntent.setAction(Defines.PUSH_INTENT_ACTION_CATCH_CALL);
                context.sendBroadcast(pushMsgIntent);
            }
            break;
            case Defines.PUSH_MSG_TYPE_REGISTER_VIRTUAL_NUMBER: {
                String virtualNumber = intent.getExtras().getString("virtual_number", "");

                Intent pushMsgIntent = new Intent();
                pushMsgIntent.setAction(Defines.PUSH_INTENT_ACTION_VIRTUAL_NUMBER_RECEIVED);
                pushMsgIntent.putExtra("virtualNumber", virtualNumber);
                context.sendBroadcast(pushMsgIntent);
            }
            break;
            default:
                break;
        }
    }
}
