package kr.driver.cardari.cardari;

import android.app.Application;
import android.content.pm.ApplicationInfo;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.skt.Tmap.TMapTapi;

import java.util.LinkedList;
import java.util.List;

import kr.driver.cardari.cardari.common.Defines;
import kr.driver.cardari.cardari.common.PrefsHelper;
import kr.driver.cardari.cardari.data.CallInfoData;
import kr.driver.cardari.cardari.data.DriverData;
import kr.driver.cardari.cardari.data.OrderData;
import kr.driver.cardari.cardari.logger.DLog;

public class MyApp extends Application {
    public interface OnCurLocationChangedListener {
        void OnCurLocationChanged(LatLng curLocation);
    }

    private static final String TAG = "MyApplication";

    private static MyApp sInstance;

    private LatLng mCurLocation;
    private DriverData mDriverData;
    private CallInfoData mCallInfoData;
    private OrderData mAllocatedOrderData;
    private String mRegistrationId;
    private String mUserPhoneNumber;

    private List<OnCurLocationChangedListener> mOnCurLocationChangedListeners = new LinkedList<>();

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");

        super.onCreate();

        sInstance = this;

        PrefsHelper.initialize(this);

        authenticateTMap();

        ApplicationInfo applicationInfo = getApplicationInfo();
        boolean appDebuggable = (applicationInfo.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
        DLog.enableADBLog(appDebuggable);

        // 내 위치 디폴트값으로 초기화
        setCurLocation(Defines.DEFAULT_MY_LOCATION);
    }

    public static MyApp getInstance() {
        return sInstance;
    }

    public void authenticateTMap() {
        TMapTapi tmaptapi = new TMapTapi(this);
        tmaptapi.setSKTMapAuthentication(Defines.TMAP_API_KEY);
    }

    public LatLng getCurLocation() {
        return mCurLocation;
    }

    public void setCurLocation(LatLng curLocation) {
        mCurLocation = curLocation;

        for (OnCurLocationChangedListener listener : mOnCurLocationChangedListeners) {
            listener.OnCurLocationChanged(mCurLocation);
        }
    }

    public DriverData getDriverData() {
        return mDriverData;
    }

    public void setDriverData(DriverData driverData) {
        mDriverData = driverData;
    }

    public OrderData getAllocatedOrderData() {
        return mAllocatedOrderData;
    }

    public void setAllocatedOrderData(OrderData allocatedOrderData) {
        mAllocatedOrderData = allocatedOrderData;
    }

    public void addOnCurLocationChangedListener(OnCurLocationChangedListener listener) {
        try {
            mOnCurLocationChangedListeners.add(listener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeOnCurLocationChangedListener(OnCurLocationChangedListener listener) {
        try {
            mOnCurLocationChangedListeners.remove(listener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getRegistrationId() {
        return mRegistrationId;
    }

    public void setRegistrationId(String registrationId) {
        mRegistrationId = registrationId;
    }

    public String getUserPhoneNumber() {
        return mUserPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        mUserPhoneNumber = userPhoneNumber;
    }

    public CallInfoData getmCallInfoData() {
        return mCallInfoData;
    }

    public void setmCallInfoData(CallInfoData mCallInfoData) {
        this.mCallInfoData = mCallInfoData;
    }
}
