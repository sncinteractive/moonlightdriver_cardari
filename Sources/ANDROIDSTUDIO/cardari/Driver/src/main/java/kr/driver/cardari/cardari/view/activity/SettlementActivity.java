package kr.driver.cardari.cardari.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.driver.cardari.cardari.MyApp;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.Enums.DriverState;
import kr.driver.cardari.cardari.logger.DLog;
import kr.driver.cardari.cardari.view.fragment.DailyIncomeFragment;
import kr.driver.cardari.cardari.view.fragment.DepositAndWithdrawFragment;
import kr.driver.cardari.cardari.view.fragment.LastOrderListFragment;
import kr.driver.cardari.cardari.view.fragment.MonthlyIncomeFragment;

import static kr.driver.cardari.cardari.common.Enums.SettlementTab;

public class SettlementActivity extends AppCompatActivity
    implements DailyIncomeFragment.EventListener,
    MonthlyIncomeFragment.EventListener,
    DepositAndWithdrawFragment.EventListener,
    LastOrderListFragment.EventListener {
    private static final String TAG = "SettlementActivity";

    @BindView(R.id.loading_progressbar_layout) View mLoadingProgressbarLayout;

    private @SettlementTab int mCurSettlementTab;
    private Fragment mCurFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settlement);
        ButterKnife.bind(this);

        initialize();
        initializeView();

        replaceTabFragment(SettlementTab.DAILY_INCOME);
    }

    private void initialize() {

    }

    private void initializeView() {

    }

    private void showLoadingProgressbar() {
        try {
            mLoadingProgressbarLayout.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            DLog.e(TAG, "showLoadingProgressbar", e);
        }
    }

    private void hideLoadingProgressbar() {
        try {
            mLoadingProgressbarLayout.setVisibility(View.GONE);
        } catch (Exception e) {
            DLog.e(TAG, "hideLoadingProgressbar", e);
        }
    }

    @OnClick(R.id.close_button)
    public void onClickClose() {
        Intent intent;
        if (MyApp.getInstance().getDriverData().getmDriverCallInfoState() == DriverState.IDLE) {
            intent = new Intent(this, OrderListActivity.class);
        } else {
            intent = new Intent(this, AllocationActivity.class);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @OnClick({R.id.daily_income_button, R.id.monthly_income_button, R.id.deposit_and_withdraw_button, R.id.last_order_list_button})
    public void onClickTab(View view) {
        @SettlementTab int tab = SettlementTab.DAILY_INCOME;
        switch (view.getId()) {
            case R.id.monthly_income_button:
                tab = SettlementTab.MONTHLY_INCOME;
                break;
            case R.id.deposit_and_withdraw_button:
                tab = SettlementTab.DEPOSIT_AND_WITHDRAW_LIST;
                break;
            case R.id.last_order_list_button:
                tab = SettlementTab.LAST_ORDER_LIST;
                break;
            default:
                break;
        }

        replaceTabFragment(tab);
    }

    private void replaceTabFragment(@SettlementTab int tab) {
        mCurSettlementTab = tab;

        Fragment fragment = null;
        switch (tab) {
            case SettlementTab.DAILY_INCOME:
                fragment = DailyIncomeFragment.createInstance();
                break;
            case SettlementTab.MONTHLY_INCOME:
                fragment = MonthlyIncomeFragment.createInstance();
                break;
            case SettlementTab.DEPOSIT_AND_WITHDRAW_LIST:
                fragment = DepositAndWithdrawFragment.createInstance();
                break;
            case SettlementTab.LAST_ORDER_LIST:
                fragment = LastOrderListFragment.createInstance();
                break;
            default:
                break;
        }

        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_frame, fragment).commit();
        }
    }

    @Override
    public void needToShowLoadingProgressbar() {
        try {
            showLoadingProgressbar();
        } catch (Exception e) {
            DLog.e(TAG, "needToShowLoadingProgressbar exception", e);
        }
    }

    @Override
    public void needToHideLoadingProgressbar() {
        try {
            hideLoadingProgressbar();
        } catch (Exception e) {
            DLog.e(TAG, "needToHideLoadingProgressbar exception", e);
        }
    }
}
