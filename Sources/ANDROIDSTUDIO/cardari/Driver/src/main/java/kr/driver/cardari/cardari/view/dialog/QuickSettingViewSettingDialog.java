package kr.driver.cardari.cardari.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.Defines;
import kr.driver.cardari.cardari.common.PrefsHelper;
import kr.driver.cardari.cardari.logger.DLog;
import yuku.ambilwarna.AmbilWarnaDialog;

/**
 * Created by eklee on 2017. 11. 13..
 */

public class QuickSettingViewSettingDialog extends Dialog {
    public interface EventListener {
        void onTextSizeChanged(int value);

        void onRowHeightChanged(int value);

        void onAuctionOrderColorChanged(int color);

        void onNormalOrderColorChanged(int color);

        void onRefusalOrderColorChanged(int color);

        void onBackgroundColorChanged(int color);

        void onAllPropertiesChanged(int textSize, int rowHeight, int auctionOrderColor, int normalOrderColor, int refusalOrderColor, int backgroundColor);
    }

    private static final String TAG = "QuickSettingViewSettingDialog";

    @BindView(R.id.view_setting_text_size_title_text_view) TextView mTextSizeTitleTextView;
    @BindView(R.id.view_setting_text_size_seek_bar) SeekBar mTextSizeSeekBar;
    @BindView(R.id.view_setting_row_height_title_text_view) TextView mRowHeightTitleTextView;
    @BindView(R.id.view_setting_row_height_seek_bar) SeekBar mRowHeightSeekBar;
    @BindView(R.id.view_setting_auction_order_color_button) Button mAuctionOrderColorButton;
    @BindView(R.id.view_setting_normal_order_color_button) Button mNormalOrderColorButton;
    @BindView(R.id.view_setting_refusal_order_color_button) Button mRefusalOrderColorButton;
    @BindView(R.id.view_setting_background_color_button) Button mBackgroundColorButton;
    @BindView(R.id.view_setting_apply_button) Button mApplyButton;
    @BindView(R.id.view_setting_close_button) Button mCloseButton;

    private EventListener mEventListener;

    public QuickSettingViewSettingDialog(@NonNull Context context, EventListener eventListener) {
        super(context, R.style.Theme_AppCompat_Translucent);
        mEventListener = eventListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_quick_setting_view_setting);
        ButterKnife.bind(this);

        initializeView();
    }

    private void initializeView() {
        // font size
        int textSize = PrefsHelper.getInt(PrefsHelper.PREFS_KEY_TEXT_SIZE, Defines.DEFAULT_ORDER_TEXT_SIZE_MIN);
        mTextSizeTitleTextView.setText(String.format(Locale.ENGLISH, getContext().getString(R.string.text_size_setting_title), textSize));

        mTextSizeSeekBar.setProgress(textSize - Defines.DEFAULT_ORDER_TEXT_SIZE_MIN);
        mTextSizeSeekBar.setMax(Defines.DEFAULT_ORDER_TEXT_SIZE_MAX - Defines.DEFAULT_ORDER_TEXT_SIZE_MIN);
        mTextSizeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int textSize = progress + Defines.DEFAULT_ORDER_TEXT_SIZE_MIN;
                mTextSizeTitleTextView.setText(String.format(Locale.ENGLISH, getContext().getString(R.string.text_size_setting_title), textSize));

                if (mEventListener != null) {
                    mEventListener.onTextSizeChanged(textSize);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // row height
        int rowHeight = PrefsHelper.getInt(PrefsHelper.PREFS_KEY_ROW_HEIGHT, Defines.DEFAULT_ORDER_ROW_HEIGHT_MIN);
        mRowHeightTitleTextView.setText(String.format(Locale.ENGLISH, getContext().getString(R.string.row_height_setting_title), rowHeight));
        mRowHeightSeekBar.setProgress(rowHeight - Defines.DEFAULT_ORDER_ROW_HEIGHT_MIN);
        mRowHeightSeekBar.setMax(Defines.DEFAULT_ORDER_ROW_HEIGHT_MAX - Defines.DEFAULT_ORDER_ROW_HEIGHT_MIN);
        mRowHeightSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int rowHeight = progress + Defines.DEFAULT_ORDER_ROW_HEIGHT_MIN;
                mRowHeightTitleTextView.setText(String.format(Locale.ENGLISH, getContext().getString(R.string.row_height_setting_title), rowHeight));

                if (mEventListener != null) {
                    mEventListener.onRowHeightChanged(rowHeight);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // colors
        mAuctionOrderColorButton.setBackgroundColor(PrefsHelper.getInt(PrefsHelper.PREFS_KEY_AUCTION_ORDER_COLOR, Defines.DEFAULT_AUCTION_ORDER_COLOR));
        mNormalOrderColorButton.setBackgroundColor(PrefsHelper.getInt(PrefsHelper.PREFS_KEY_NORMAL_ORDER_COLOR, Defines.DEFAULT_NORMAL_ORDER_COLOR));
        mRefusalOrderColorButton.setBackgroundColor(PrefsHelper.getInt(PrefsHelper.PREFS_KEY_REFUSAL_ORDER_COLOR, Defines.DEFAULT_REFUSAL_ORDER_COLOR));
        mBackgroundColorButton.setBackgroundColor(PrefsHelper.getInt(PrefsHelper.PREFS_KEY_ORDER_BACKGROUND_COLOR, Defines.DEFAULT_ORDER_BACKGROUND_COLOR));
    }

    @OnClick({R.id.view_setting_auction_order_color_button,
        R.id.view_setting_normal_order_color_button,
        R.id.view_setting_refusal_order_color_button,
        R.id.view_setting_background_color_button})
    public void onClickOrderColor(final View view) {
        try {
            int initialColor = ((ColorDrawable) view.getBackground()).getColor();
            new AmbilWarnaDialog(getContext(), initialColor, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                @Override
                public void onCancel(AmbilWarnaDialog dialog) {
                }

                @Override
                public void onOk(AmbilWarnaDialog dialog, int color) {
                    view.setBackgroundColor(color);

                    if (mEventListener != null) {
                        if (view.getId() == R.id.view_setting_auction_order_color_button) {
                            mEventListener.onAuctionOrderColorChanged(color);
                        } else if (view.getId() == R.id.view_setting_normal_order_color_button) {
                            mEventListener.onNormalOrderColorChanged(color);
                        } else if (view.getId() == R.id.view_setting_refusal_order_color_button) {
                            mEventListener.onRefusalOrderColorChanged(color);
                        } else if (view.getId() == R.id.view_setting_background_color_button) {
                            mEventListener.onBackgroundColorChanged(color);
                        }
                    }
                }
            }).show();
        } catch (Exception e) {
            DLog.e(TAG, "onClickOrderColor exception", e);
        }
    }

    @OnClick(R.id.view_setting_close_button)
    public void onClickClose() {
        // rollback
        if (mEventListener != null) {
            int textSize = PrefsHelper.getInt(PrefsHelper.PREFS_KEY_TEXT_SIZE, Defines.DEFAULT_ORDER_TEXT_SIZE_MIN);
            int rowHeight = PrefsHelper.getInt(PrefsHelper.PREFS_KEY_ROW_HEIGHT, Defines.DEFAULT_ORDER_ROW_HEIGHT_MIN);
            int auctionOrderColor = PrefsHelper.getInt(PrefsHelper.PREFS_KEY_AUCTION_ORDER_COLOR, Defines.DEFAULT_AUCTION_ORDER_COLOR);
            int normalOrderColor = PrefsHelper.getInt(PrefsHelper.PREFS_KEY_NORMAL_ORDER_COLOR, Defines.DEFAULT_NORMAL_ORDER_COLOR);
            int refusalOrderColor = PrefsHelper.getInt(PrefsHelper.PREFS_KEY_REFUSAL_ORDER_COLOR, Defines.DEFAULT_REFUSAL_ORDER_COLOR);
            int backgroundColor = PrefsHelper.getInt(PrefsHelper.PREFS_KEY_ORDER_BACKGROUND_COLOR, Defines.DEFAULT_ORDER_BACKGROUND_COLOR);
            mEventListener.onAllPropertiesChanged(textSize, rowHeight, auctionOrderColor, normalOrderColor, refusalOrderColor, backgroundColor);
        }
        dismiss();
    }

    @OnClick(R.id.view_setting_apply_button)
    public void onClickApply() {
        PrefsHelper.putInt(PrefsHelper.PREFS_KEY_TEXT_SIZE, mTextSizeSeekBar.getProgress() + Defines.DEFAULT_ORDER_TEXT_SIZE_MIN);
        PrefsHelper.putInt(PrefsHelper.PREFS_KEY_ROW_HEIGHT, mRowHeightSeekBar.getProgress() + Defines.DEFAULT_ORDER_ROW_HEIGHT_MIN);
        PrefsHelper.putInt(PrefsHelper.PREFS_KEY_AUCTION_ORDER_COLOR, ((ColorDrawable) mAuctionOrderColorButton.getBackground()).getColor());
        PrefsHelper.putInt(PrefsHelper.PREFS_KEY_NORMAL_ORDER_COLOR, ((ColorDrawable) mNormalOrderColorButton.getBackground()).getColor());
        PrefsHelper.putInt(PrefsHelper.PREFS_KEY_REFUSAL_ORDER_COLOR, ((ColorDrawable) mRefusalOrderColorButton.getBackground()).getColor());
        PrefsHelper.putInt(PrefsHelper.PREFS_KEY_ORDER_BACKGROUND_COLOR, ((ColorDrawable) mBackgroundColorButton.getBackground()).getColor());
        dismiss();
    }
}
