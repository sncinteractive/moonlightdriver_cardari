package kr.driver.cardari.cardari.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import java.util.Set;

/**
 * Created by eklee on 2017. 11. 9..
 */

public class PrefsHelper {
    public static final String SHARED_PREFS_NAME = "cardari_driver";

    public static final String PREFS_KEY_MENU_VISIBILITY = "MENU_VISIBILITY";
    public static final String PREFS_KEY_ORDER_QUERY_RADIUS = "ORDER_QUERY_RADIUS";
    public static final String PREFS_KEY_ORDER_QUERY_TYPE = "ORDER_QUERY_TYPE";
    public static final String PREFS_KEY_ORDER_QUERY_INCLUDE_REFUSAL = "ORDER_QUERY_INCLUDE_REFUSAL";
    public static final String PREFS_KEY_TEXT_SIZE = "TEXT_SIZE";
    public static final String PREFS_KEY_ROW_HEIGHT = "ROW_HEIGHT";
    public static final String PREFS_KEY_AUCTION_ORDER_COLOR = "AUCTION_ORDER_COLOR";
    public static final String PREFS_KEY_NORMAL_ORDER_COLOR = "NORMAL_ORDER_COLOR";
    public static final String PREFS_KEY_REFUSAL_ORDER_COLOR = "REFUSAL_ORDER_COLOR";
    public static final String PREFS_KEY_ORDER_BACKGROUND_COLOR = "ORDER_BACKGROUND_COLOR";
    public static final String PREFS_KEY_CARD_ORDER_COLOR = "CARD_ORDER_COLOR";
    public static final String PREFS_KEY_DISTANCE_VISIBILITY = "DISTANCE_VISIBILITY";
    public static final String PREFS_KEY_PAY_AMOUNT_VISIBILITY = "PAY_AMOUNT_VISIBILITY";
    public static final String PREFS_KEY_FIRST_LAUNCH = "FIRST_LAUNCH";
    public static final String PREFS_KEY_DRIVER_ID= "DRIVER_ID";
    public static final String PREFS_KEY_CALL_STATUS = "CALL_STATUS";
    public static final String PREFS_KEY_BIDDING_PRICE = "BIDDING_PRICE";
    public static final String PREFS_KEY_CALL_ID = "CALL_ID";
    public static final String PREFS_KEY_LATEST_RECEIVED_PUSH_MSG_TYPE = "LATEST_RECEIVED_PUSH_MSG_TYPE";

    private static SharedPreferences sSharedPref;
    private static SharedPreferences.Editor sEditor;

    public static void initialize(Context context) {
        if (sSharedPref == null) {
            sSharedPref = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
            sEditor = sSharedPref.edit();
        }
    }

    public static void putBoolean(String key, boolean value) {
        sEditor.putBoolean(key, value);
        sEditor.apply();
    }

    public static void putInt(String key, int value) {
        sEditor.putInt(key, value);
        sEditor.apply();
    }

    public static void putLong(String key, long value) {
        sEditor.putLong(key, value);
        sEditor.apply();
    }

    public static void putFloat(String key, float value) {
        sEditor.putFloat(key, value);
        sEditor.apply();
    }

    public static void putString(String key, String value) {
        sEditor.putString(key, value);
        sEditor.apply();
    }

    public static void putStringSet(String key, Set<String> values) {
        sEditor.putStringSet(key, values);
        sEditor.apply();
    }

    public static boolean getBoolean(String key, boolean defaultValue) {
        return sSharedPref.getBoolean(key, defaultValue);
    }

    public static int getInt(String key, int defaultValue) {
        return sSharedPref.getInt(key, defaultValue);
    }

    public static long getLong(String key, long defaultValue) {
        return sSharedPref.getLong(key, defaultValue);
    }

    public static float getFloat(String key, float defaultValue) {
        return sSharedPref.getFloat(key, defaultValue);
    }

    public static String getString(String key) {
        return getString(key, null);
    }

    public static String getString(String key, @Nullable String defaultValue) {
        return sSharedPref.getString(key, defaultValue);
    }

    public static Set<String> getStringSet(String key) {
        return getStringSet(key, null);
    }

    public static Set<String> getStringSet(String key, @Nullable Set<String> defaultValues) {
        return sSharedPref.getStringSet(key, defaultValues);
    }

    public static void remove(String... keys) {
        for (String key : keys) {
            sEditor.remove(key);
        }
        sEditor.apply();
    }

    public static void clear() {
        sEditor.clear();
        sEditor.apply();
    }

    public static void commit() {
        sEditor.commit();
    }
}
