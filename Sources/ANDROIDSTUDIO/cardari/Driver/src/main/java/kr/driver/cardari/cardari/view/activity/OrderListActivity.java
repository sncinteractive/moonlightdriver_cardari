package kr.driver.cardari.cardari.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.RequestParams;
import com.skt.Tmap.TMapAddressInfo;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.driver.cardari.cardari.MyApp;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.Defines;
import kr.driver.cardari.cardari.common.DialogHelper;
import kr.driver.cardari.cardari.common.Enums;
import kr.driver.cardari.cardari.common.Enums.MainMenuType;
import kr.driver.cardari.cardari.common.PrefsHelper;
import kr.driver.cardari.cardari.common.Util;
import kr.driver.cardari.cardari.data.OrderData;
import kr.driver.cardari.cardari.logger.DLog;
import kr.driver.cardari.cardari.network.NetClient;
import kr.driver.cardari.cardari.network.NetResponseCallback;
import kr.driver.cardari.cardari.network.ack.BaseAck;
import kr.driver.cardari.cardari.network.ack.OrderListAck;
import kr.driver.cardari.cardari.tmap.TMapHelper;
import kr.driver.cardari.cardari.view.adapter.OrderListAdapter;
import kr.driver.cardari.cardari.view.dialog.QuickSettingLocationSettingDialog;
import kr.driver.cardari.cardari.view.dialog.QuickSettingQuerySettingDialog;
import kr.driver.cardari.cardari.view.dialog.QuickSettingViewSettingDialog;
import kr.driver.cardari.cardari.view.fragment.MainMenuFragment;

import static kr.driver.cardari.cardari.common.Defines.EXTRA_KEY_ORDER_ID;
import static kr.driver.cardari.cardari.common.Enums.MenuVisibility;
import static kr.driver.cardari.cardari.common.Enums.NetResultCode;
import static kr.driver.cardari.cardari.common.Enums.OrderQueryType;
import static kr.driver.cardari.cardari.common.PrefsHelper.PREFS_KEY_DISTANCE_VISIBILITY;
import static kr.driver.cardari.cardari.common.PrefsHelper.PREFS_KEY_MENU_VISIBILITY;
import static kr.driver.cardari.cardari.common.PrefsHelper.PREFS_KEY_ORDER_QUERY_INCLUDE_REFUSAL;
import static kr.driver.cardari.cardari.common.PrefsHelper.PREFS_KEY_ORDER_QUERY_RADIUS;
import static kr.driver.cardari.cardari.common.PrefsHelper.PREFS_KEY_ORDER_QUERY_TYPE;
import static kr.driver.cardari.cardari.common.PrefsHelper.PREFS_KEY_PAY_AMOUNT_VISIBILITY;

public class OrderListActivity extends AppCompatActivity
    implements MainMenuFragment.EventListener, MyApp.OnCurLocationChangedListener {
    private static final String TAG = "OrderListActivity";

    @BindView(R.id.fragment_main_menu_frame) FrameLayout mFragmentMainMenuFrame;
    @BindView(R.id.sub_menu_layout) LinearLayout mSubMenuLayout;
    @BindView(R.id.location_button) Button mLocationButton;
    @BindView(R.id.order_query_radius_button) Button mOrderSearchRadiusButton;
    @BindView(R.id.hide_menu_button) Button mHideMenuButton;
    @BindView(R.id.refusal_button) Button mRefusalButton;

    @BindView(R.id.order_list_title_layout) View mOrderListTitleLayout;
    @BindView(R.id.order_list_title_distance_layout) LinearLayout mOrderListTitleDistanceLayout;
    @BindView(R.id.order_list_title_payamount_layout) LinearLayout mOrderListTitlePayamountTitleLayout;
    @BindView(R.id.order_list_title_distance_title_text_view) TextView mOrderListTitleDistanceTitleTextView;
    @BindView(R.id.order_list_title_start_point_title_text_view) TextView mOrderListTitleStartPointTitleTextView;
    @BindView(R.id.order_list_title_end_point_title_text_view) TextView mOrderListTitleEndPointTitleTextView;
    @BindView(R.id.order_list_title_show_all_menu_button) ImageButton mOrderListTitleShowAllMenuButton;
    @BindView(R.id.order_list_recycler_view) RecyclerView mOrderListRecyclerView;

    @BindView(R.id.quick_setting_layout) View mQuickSettingLayout;

    @BindView(R.id.loading_progressbar_layout) View mLoadingProgressbarLayout;

    @BindView(R.id.distance_setting_layout) LinearLayout mDistanceSettingLayout;
    @BindView(R.id.apply_button) Button mApplyButton;

    private OrderListAdapter mOrderListAdapter;
    private int mOrderListScrollPosition;
    private int mOrderQueryRadius;

    private MainMenuFragment mMainMenuFragment;

    private Handler mHandler;
    private Runnable mOrderListRunnable;

    private LatLng mLastLocation = new LatLng(0, 0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        ButterKnife.bind(this);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        initialize(savedInstanceState);
        initializeView();
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            startGetOrderList();

            MyApp.getInstance().addOnCurLocationChangedListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            stopGetOrderList();

            MyApp.getInstance().removeOnCurLocationChangedListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startGetOrderList() {
        try {
            requestOrderList();

            mHandler = new Handler();
            mOrderListRunnable = new Runnable() {
                @Override
                public void run() {
                    requestOrderList();
                    mHandler.postDelayed(this, Defines.REQUEST_ORDER_LIST_INTERVAL);
                }
            };

            mHandler.postDelayed(mOrderListRunnable, Defines.REQUEST_ORDER_LIST_INTERVAL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopGetOrderList() {
        try {
            mHandler.removeCallbacks(mOrderListRunnable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void testClear() {
        PrefsHelper.clear();
    }

    public void initialize(Bundle savedInstanceState) {
        initializeOrderList(savedInstanceState);
    }

    private void initializeOrderList(Bundle savedInstanceState) {
        mOrderListScrollPosition = savedInstanceState != null ? savedInstanceState.getInt("scrollPosition", 0) : 0;
        DLog.d(TAG, "initializeOrderList mOrderListScrollPosition = " + mOrderListScrollPosition);

        mOrderListAdapter = new OrderListAdapter();
        mOrderListRecyclerView.setAdapter(mOrderListAdapter);

        final GestureDetectorCompat gestureDetectorCompat = new GestureDetectorCompat(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
        mOrderListRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                try {
                    View child = rv.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && gestureDetectorCompat.onTouchEvent(e)) {
                        int position = rv.getChildAdapterPosition(child);
                        OrderData orderData = mOrderListAdapter.getItem(position);

                        Intent intent = new Intent(OrderListActivity.this, NormalOrderDetailActivity.class);
                        intent.putExtra(EXTRA_KEY_ORDER_ID, orderData.getCallId());
                        OrderListActivity.this.startActivity(intent);
                    }
                } catch (Exception exception) {
                    DLog.e(TAG, "OrderListRecyclerView::onInterceptTouchEvent exception", exception);
                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mOrderListRecyclerView.setLayoutManager(layoutManager);
    }

    public void initializeView() {
        // fragment main menu
        if (mMainMenuFragment == null) {
            mMainMenuFragment = new MainMenuFragment();
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main_menu_frame, mMainMenuFragment).commit();

        applySettingDataToUI();

        hideQuickSettingLayout();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mOrderListRecyclerView.getLayoutManager();
        outState.putInt("scrollPosition", linearLayoutManager.findFirstVisibleItemPosition());
        DLog.d(TAG, "onSaveInstanceState scrollPosition = " + linearLayoutManager.findFirstVisibleItemPosition());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        int scrollPosition = savedInstanceState.getInt("scrollPosition", 0);
        mOrderListRecyclerView.scrollToPosition(scrollPosition);
        DLog.d(TAG, "onRestoreInstanceState scrollPosition = " + scrollPosition);
    }

    @OnClick(R.id.hide_menu_button)
    public void onClickHideMenu() {
        // menu visibility
        @MenuVisibility int menuVisibility = PrefsHelper.getInt(PREFS_KEY_MENU_VISIBILITY, MenuVisibility.ALL);
        if (menuVisibility == MenuVisibility.ALL) {
            menuVisibility = MenuVisibility.SUB_MENU;
        } else {
            menuVisibility = MenuVisibility.HIDE_ALL;
        }

        PrefsHelper.putInt(PREFS_KEY_MENU_VISIBILITY, menuVisibility);

        applySettingDataToUI();
    }

    @OnClick(R.id.order_list_title_show_all_menu_button)
    public void onClickShowAllMenu() {
        PrefsHelper.putInt(PREFS_KEY_MENU_VISIBILITY, MenuVisibility.ALL);
        applySettingDataToUI();
    }

    @OnClick(R.id.refusal_button)
    public void onClickRefusal() {
        boolean orderQueryIncludeRefusal = PrefsHelper.getBoolean(PREFS_KEY_ORDER_QUERY_INCLUDE_REFUSAL, false);
        PrefsHelper.putBoolean(PREFS_KEY_ORDER_QUERY_INCLUDE_REFUSAL, !orderQueryIncludeRefusal);

        applySettingDataToUI();

        requestOrderList();
    }

    @OnClick({R.id.order_query_radius_button,
        R.id.quick_setting_radius_setting_button})
    public void onClickOrderQueryRadiusSetting() {
        hideQuickSettingLayout();

        mOrderQueryRadius = PrefsHelper.getInt(PREFS_KEY_ORDER_QUERY_RADIUS, 1000);
        applyRadius(mOrderQueryRadius);

        mDistanceSettingLayout.setVisibility(View.VISIBLE);
    }

    @OnClick({R.id.decrease_big_button, R.id.decrease_button, R.id.increase_button, R.id.increase_big_button})
    public void onClickRadiusControlButton(View view) {
        int newRadius = mOrderQueryRadius;
        switch (view.getId()) {
            case R.id.decrease_big_button: {
                newRadius -= 1000;
            }
            break;
            case R.id.decrease_button: {
                newRadius -= 100;
            }
            break;
            case R.id.increase_button: {
                newRadius += 100;
            }
            break;
            case R.id.increase_big_button: {
                newRadius += 1000;
            }
            break;
            default:
                return;
        }

        if (newRadius != mOrderQueryRadius) {
            applyRadius(newRadius);
        }
    }

    private void applyRadius(int radius) {
        final int MIN_VALUE = 1;
        final int MAX_VALUE = 1000 * 100; // meter
        mOrderQueryRadius = Math.min(Math.max(radius, MIN_VALUE), MAX_VALUE);
        mApplyButton.setText(Util.convertDistanceString(mOrderQueryRadius));
    }

    @OnClick(R.id.apply_button)
    public void onClickApply() {
        final int orderQueryRadius = PrefsHelper.getInt(PREFS_KEY_ORDER_QUERY_RADIUS, 1000);

        if (orderQueryRadius != mOrderQueryRadius) {
            PrefsHelper.putInt(PREFS_KEY_ORDER_QUERY_RADIUS, mOrderQueryRadius);
            applySettingDataToUI();
            requestOrderList();
        }

        mDistanceSettingLayout.setVisibility(View.GONE);
    }

    public void applySettingDataToUI() {
        try {
            // menu visibility
            @MenuVisibility int menuVisibility = PrefsHelper.getInt(PREFS_KEY_MENU_VISIBILITY, MenuVisibility.ALL);
            switch (menuVisibility) {
                case MenuVisibility.ALL: {
                    mFragmentMainMenuFrame.setVisibility(View.VISIBLE);
                    mSubMenuLayout.setVisibility(View.VISIBLE);
                    mOrderListTitleShowAllMenuButton.setVisibility(View.GONE);
                }
                break;
                case MenuVisibility.SUB_MENU: {
                    mFragmentMainMenuFrame.setVisibility(View.GONE);
                    mSubMenuLayout.setVisibility(View.VISIBLE);
                    mOrderListTitleShowAllMenuButton.setVisibility(View.GONE);
                }
                break;
                case MenuVisibility.HIDE_ALL: {
                    mFragmentMainMenuFrame.setVisibility(View.GONE);
                    mSubMenuLayout.setVisibility(View.GONE);
                    mOrderListTitleShowAllMenuButton.setVisibility(View.VISIBLE);
                }
                break;
                default:
                    throw new RuntimeException("applySettingDataToUI - not implemented menu visibility type");
            }

            // refusal button
            boolean orderQueryIncludeRefusal = PrefsHelper.getBoolean(PREFS_KEY_ORDER_QUERY_INCLUDE_REFUSAL, false);
            mRefusalButton.setText(orderQueryIncludeRefusal ? R.string.refusal_button_off_title : R.string.refusal_button_on_title);

            // order search radius
            int orderQueryRadius = PrefsHelper.getInt(PREFS_KEY_ORDER_QUERY_RADIUS, 1000);
            mOrderSearchRadiusButton.setText(Util.convertDistanceString(orderQueryRadius));

            // order list title
            int distanceVisibility = PrefsHelper.getInt(PREFS_KEY_DISTANCE_VISIBILITY, View.VISIBLE);
            mOrderListTitleDistanceLayout.setVisibility(distanceVisibility);
            int payamountVisibility = PrefsHelper.getInt(PREFS_KEY_PAY_AMOUNT_VISIBILITY, View.VISIBLE);
            mOrderListTitlePayamountTitleLayout.setVisibility(payamountVisibility);
        } catch (Exception e) {
            DLog.e(TAG, "applySettingDataToUI exception", e);
        }
    }

    public void showLoadingProgressBar() {
        mLoadingProgressbarLayout.setVisibility(View.VISIBLE);
    }

    public void hideLoadingProgressBar() {
        mLoadingProgressbarLayout.setVisibility(View.GONE);
    }

    public void requestOrderList() {
        try {
            @OrderQueryType String queryType = PrefsHelper.getString(PREFS_KEY_ORDER_QUERY_TYPE, OrderQueryType.ALL);
            final boolean includeRefusal = PrefsHelper.getBoolean(PREFS_KEY_ORDER_QUERY_INCLUDE_REFUSAL, false);
            int dist = PrefsHelper.getInt(PREFS_KEY_ORDER_QUERY_RADIUS, 1000);

            // 오더 리스트 조회 서버 연동
            RequestParams requestParams = new RequestParams();
            requestParams.put("queryType", queryType);
            requestParams.put("dist", dist);
            requestParams.put("includeRefusal", includeRefusal);
            requestParams.put("lat", MyApp.getInstance().getCurLocation().latitude);
            requestParams.put("lng", MyApp.getInstance().getCurLocation().longitude);

            NetClient.get(this, NetClient.URL_CALL_LIST, OrderListAck.class, requestParams, new NetResponseCallback(new OrderListResponseListener()));
        } catch (Exception e) {
            DLog.e(TAG, "requestOrderList exception", e);
        }
    }

    public class OrderListResponseListener implements NetResponseCallback.NetResponseListener {
        @Override
        public void onResponse(BaseAck result) {
            try {
                if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
                    OrderListAck ack = (OrderListAck) result;
                    mOrderListAdapter.setDataList(ack.getOrderDataList());
                    mOrderListRecyclerView.scrollToPosition(mOrderListScrollPosition);
                    DLog.d(TAG, "OrderListResponseListener mOrderListScrollPosition = " + mOrderListScrollPosition);
                    mMainMenuFragment.updateDriverBalance(MyApp.getInstance().getDriverData().getBalance());
                } else {
                    DialogHelper.showAlertDialog(OrderListActivity.this, "알림", "오더 정보 조회 요청이 실패했습니다.\n" + "code: " + result.getResult().getCode() + "detail: " + result.getResult().getDetail(), "확인", null, null);
                }
            } catch (Exception e) {
                DLog.e(TAG, "OrderListResponseListener::onResponse exception", e);
            }
        }
    }

    // region - quick setting
    public void showQuickSettingLayout() {
        mQuickSettingLayout.setVisibility(View.VISIBLE);
    }

    public void hideQuickSettingLayout() {
        mQuickSettingLayout.setVisibility(View.GONE);
    }

    @OnClick(R.id.quick_setting_layout)
    public void onClickQuickSettingLayout() {
        hideQuickSettingLayout();
    }

    @OnClick(R.id.quick_setting_query_setting_button)
    public void onClickQuickSettingQuerySetting() {
        try {
            hideQuickSettingLayout();

            new QuickSettingQuerySettingDialog(this, new QuickSettingQuerySettingDialog.EventListener() {
                @Override
                public void onSettingChanged() {
                    applySettingDataToUI();
                    requestOrderList();
                }
            }).show();
        } catch (Exception e) {
            DLog.e(TAG, "onClickQuickSettingQuerySetting exception", e);
        }

    }

    @OnClick(R.id.quick_setting_view_setting_button)
    public void onClickQuickSettingViewSetting() {
        try {
            hideQuickSettingLayout();

            new QuickSettingViewSettingDialog(this, new QuickSettingViewSettingDialog.EventListener() {
                @Override
                public void onTextSizeChanged(int value) {
                    mOrderListAdapter.changeTextSize(value);
                }

                @Override
                public void onRowHeightChanged(int value) {
                    mOrderListAdapter.changeRowHeight(value);
                }

                @Override
                public void onAuctionOrderColorChanged(int color) {
                    mOrderListAdapter.changeAuctionOrderColor(color);
                }

                @Override
                public void onNormalOrderColorChanged(int color) {
                    mOrderListAdapter.changeNormalOrderColor(color);
                }

                @Override
                public void onRefusalOrderColorChanged(int color) {
                    mOrderListAdapter.changeRefusalOrderColor(color);
                }

                @Override
                public void onBackgroundColorChanged(int color) {
                    mOrderListAdapter.changeBackgroundColor(color);
                }

                @Override
                public void onAllPropertiesChanged(int textSize, int rowHeight, int auctionOrderColor, int normalOrderColor, int refusalOrderColor, int backgroundColor) {
                    mOrderListAdapter.changeAllViewSettingProperties(textSize, rowHeight, auctionOrderColor, normalOrderColor, refusalOrderColor, backgroundColor);
                }
            }).show();

        } catch (Exception e) {
            DLog.e(TAG, "onClickQuickSettingViewSetting exception", e);
        }
    }

    @OnClick(R.id.quick_setting_distance_visibility_setting_button)
    public void onClickDistanceVisibilitySetting() {
        hideQuickSettingLayout();

        int visibility = mOrderListTitleDistanceLayout.getVisibility();
        if (visibility == View.VISIBLE) {
            visibility = View.GONE;
        } else {
            visibility = View.VISIBLE;
        }

        mOrderListTitleDistanceLayout.setVisibility(visibility);
        mOrderListAdapter.changeDistanceVisibility(visibility);
        PrefsHelper.putInt(PREFS_KEY_DISTANCE_VISIBILITY, visibility);
    }

    @OnClick(R.id.quick_setting_payamount_visibility_setting_button)
    public void onClickPayamountVisibilitySetting() {
        hideQuickSettingLayout();

        int visibility = mOrderListTitlePayamountTitleLayout.getVisibility();
        if (visibility == View.VISIBLE) {
            visibility = View.GONE;
        } else {
            visibility = View.VISIBLE;
        }

        mOrderListTitlePayamountTitleLayout.setVisibility(visibility);
        mOrderListAdapter.changePayamountVisibility(visibility);
        PrefsHelper.putInt(PREFS_KEY_PAY_AMOUNT_VISIBILITY, visibility);
    }

    @OnClick(R.id.quick_setting_location_setting_button)
    public void onClickLocation() {
        hideQuickSettingLayout();

        new QuickSettingLocationSettingDialog(this).show();
    }
    // endregion - quick setting


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        requestOrderList();
    }

    @Override
    public void onClickMainMenu(@MainMenuType int mainMenuType) {
        try {
            switch (mainMenuType) {
                case MainMenuType.ORDER: {
                    hideQuickSettingLayout();
                }
                break;
                case MainMenuType.QUICK_SETTING: {
                    if (mQuickSettingLayout.getVisibility() == View.VISIBLE) {
                        hideQuickSettingLayout();
                    } else {
                        showQuickSettingLayout();
                    }
                }
                break;
                case MainMenuType.SETTING: {
                    hideQuickSettingLayout();

                    DialogHelper.showAlertDialog(this,
                        "알림",
                        "준비중입니다",
                        getString(R.string.common_confirm),
                        null,
                        null);
                }
                break;
                case MainMenuType.SETTLEMENT: {
                    hideQuickSettingLayout();

                    Intent intent = new Intent(this, SettlementActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                break;
                default:
                    throw new RuntimeException("OnClickMainMenu - invalid main menu type: " + mainMenuType);
            }
        } catch (Exception e) {
            DLog.e(TAG, "OnClickTab exception", e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            switch (requestCode) {
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void OnCurLocationChanged(LatLng curLocation) {
        try {
            // 이전 위치와 100미터 이상 차이나면 갱신되는지 테스트
            double diff = Util.distanceBetweenLocation(mLastLocation, curLocation, Enums.DistanceUnit.METER) - Defines.LOCATION_UPDATE_DISTANCE_MIN;
            if (diff >= 0) {
                mLastLocation = curLocation;

                TMapHelper.convertGpsToAddress(new TMapHelper.ReverseGeocodingTask.ReverseGeocodingListener() {
                    @Override
                    public void onReverseGeocoding(TMapAddressInfo result) {
                        if (result != null) {
                            if (!Util.isStringNullOrEmpty(result.strBuildingName)) {
                                mLocationButton.setText(result.strBuildingName);
                            } else if (!Util.isStringNullOrEmpty(result.strRoadName)) {
                                mLocationButton.setText(result.strRoadName);
                            } else if (!Util.isStringNullOrEmpty(result.strLegalDong)) {
                                mLocationButton.setText(result.strLegalDong);
                            }
                        }
                    }
                }, curLocation.latitude, curLocation.longitude);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
