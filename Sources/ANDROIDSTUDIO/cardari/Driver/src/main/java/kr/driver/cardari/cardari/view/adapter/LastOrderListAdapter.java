package kr.driver.cardari.cardari.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.Util;
import kr.driver.cardari.cardari.data.LastOrderData;
import kr.driver.cardari.cardari.logger.DLog;

/**
 * Created by eklee on 2017. 11. 7..
 */

public class LastOrderListAdapter extends RecyclerView.Adapter {
    private static final String TAG = "LastOrderListAdapter";

    private List<LastOrderData> mLastOrderDataList;

    public LastOrderListAdapter() {
    }

    public void setDataList(List<LastOrderData> lastOrderDataList) {
        mLastOrderDataList = lastOrderDataList;

        notifyDataSetChanged();
    }

    public LastOrderData getItem(int position) {
        try {
            return mLastOrderDataList.get(position);
        } catch (Exception e) {
            DLog.e(TAG, "getItem exception", e);
        }

        return null;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_last_order_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).bind(mLastOrderDataList.get(position));
    }

    @Override
    public int getItemCount() { return (mLastOrderDataList != null ? mLastOrderDataList.size() : 0); }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.date_text_view) TextView mDateTextView;
        @BindView(R.id.start_point_text_view) TextView mStartPointTextView;
        @BindView(R.id.end_point_text_view) TextView mEndPointTextView;
        @BindView(R.id.payamount_text_view) TextView mPayamountTextView;
        @BindView(R.id.summary_text_view) TextView mSummaryTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(LastOrderData data) {
            try {
                mDateTextView.setText(Util.convertYearMonthDayString(data.getDateTime()));
                mStartPointTextView.setText(data.getStartPointName());
                mEndPointTextView.setText(data.getEndPointName());
                mPayamountTextView.setText(Util.convertPriceToThousandString(data.getPayamount()));
                mSummaryTextView.setText(data.getSummary());

            } catch (Exception e) {
                DLog.e(TAG, "ViewHolder::Bind exception", e);
            }
        }
    }
}
