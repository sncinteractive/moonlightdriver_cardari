package kr.driver.cardari.cardari.common;

import com.google.android.gms.maps.model.LatLng;

import kr.driver.cardari.cardari.data.LocationData;

/**
 * Created by eklee on 2017. 6. 14..
 */

public class Defines {
//    public static final String TMAP_API_KEY = "013703a7-d144-36c8-a049-9dafe4e4417b";
    public static final String TMAP_API_KEY = "3c0058e4-dd9b-41d7-9ba0-e84b68265164";

    public static final String INSURANCE_API_KEY = "C99F059C-2672-494C-8A33-BE8A32108F51";
	public static final String INSURANCE_COMPANY_NUMBER = "11111111";
	public static final String INSURANCE_CRYPT_KEY = "B48AC0F1AE1E4D86ACCC948E491057D6";

    public static final LatLng DEFAULT_MY_LOCATION = new LatLng(37.567929, 126.978046); // 베스트웨스턴 뉴서울호텔 (서울시청 위)

    // dummy for test
    public static final LocationData DUMMY_LOCATION_SEOUL_CITY_HALL = new LocationData(new LatLng(37.566221, 126.977921), "서울시청", "서울특별시 중구 명동 세종대로 110");
    public static final LocationData DUMMY_LOCATION_CITY_HALL_STATION = new LocationData(new LatLng(37.565772, 126.976867), "시청역", "서울특별시 중구 소공동 지하 101");
    public static final LocationData DUMMY_LOCATION_GANG_NAM_STATION = new LocationData(new LatLng(37.498163, 127.027600), "강남역", "서울특별시 역삼1동");

    // setting values
    public static final int DEFAULT_ORDER_TEXT_SIZE_MIN = 15; // sp
    public static final int DEFAULT_ORDER_TEXT_SIZE_MAX = 30; // sp
    public static final int DEFAULT_ORDER_ROW_HEIGHT_MIN = 60; // dp
    public static final int DEFAULT_ORDER_ROW_HEIGHT_MAX = 90; // dp

    public static final int DEFAULT_AUCTION_ORDER_COLOR = 0xFF1072BD;
    public static final int DEFAULT_NORMAL_ORDER_COLOR = 0xFF000000;
    public static final int DEFAULT_REFUSAL_ORDER_COLOR = 0xFFFC0D1B;
	public static final int DEFAULT_CARD_ORDER_COLOR = 0xFFFF0000;
	public static final int DEFAULT_MODIFY_ORDER_COLOR = 0xFF0000FF;
    public static final int DEFAULT_ORDER_BACKGROUND_COLOR = 0xFFF2F2F2;

    // google map
    public static final float GOOGLE_MAP_DEFAULT_ZOOM = 16f;

    // extra key
    public static final String EXTRA_KEY_ORDER_DATA = "orderData";
	public static final String EXTRA_KEY_ORDER_ID = "orderId";
    public static final String EXTRA_KEY_ORDER_DETAIL_RESULT = "result";

    public static final String EXTRA_KEY_CLIENT_POINT = "clientPoint";
    public static final String EXTRA_KEY_START_POINT = "startPoint";
    public static final String EXTRA_KEY_END_POINT = "endPoint";
    public static final String EXTRA_KEY_THROUGH_POINT_LIST = "throughPointList";

    public static final String EXTRA_KEY_INCOME_DATE_TYPE = "incomeDateType";

	public static final String EXTRA_KEY_PHONE_AUTH_RESULT = "PHONE_AUTH_RESULT";
	public static final String EXTRA_KEY_PHONE_AUTH_PHONE_NUMBER = "PHONE_AUTH_PHONE_NUMBER";
	public static final String EXTRA_KEY_PHONE_AUTH_CI = "PHONE_AUTH_CI";
	public static final String EXTRA_KEY_PHONE_AUTH_DI = "PHONE_AUTH_DI";

	public static final int ACCESS_LOCATION_REQ_CODE = 1;
	public static final int PHONE_NUMBER_REQ_CODE = 2;
	public static final int CALL_PHONE_REQ_CODE = 3;
	public static final int GPS_SETTING_REQ_CODE = 4;
	public static final int WIFI_SETTING_REQ_CODE = 5;
	public static final int PHONE_AUTH_REQ_CODE = 6;

	public static final int REQUEST_GOOGLE_PLAY_SERVICES = 9000;

	public static final long DOUBLE_PRESS_INTERVAL = 2000;
	public static final int REQUEST_ORDER_LIST_INTERVAL = 5000;

	// 콜비 수정 요청 후 기다리는 시간 7초
	public static final int DEFAULT_BIDDING_WAIT_TIME = 7;

    public static final String PROPERTY_LATEST_RECEIVED_CALL_DRIVER_PUSH_MSG_TYPE = "LATEST_RECEIVED_CALL_DRIVER_PUSH_MSG_TYPE";

	public static final String SENDER_ID = "563199847799";

	public static final String APP_DOWNLOAD_URL = "https://play.google.com/store/apps/details?id=kr.moonlightdriver.user";

	public static final String PUSH_MSG_TYPE_CATCH_CALL = "catch_call";


	// push intent action type
	public static final String PUSH_INTENT_ACTION_CATCH_CALL = "PUSH_INTENT_ACTION_CATCH_CALL";
	public static final String PUSH_INTENT_ACTION_BIDDING = "PUSH_INTENT_ACTION_BIDDING";
	public static final String PUSH_INTENT_ACTION_VIRTUAL_NUMBER_RECEIVED = "PUSH_INTENT_ACTION_VIRTUAL_NUMBER_RECEIVED";
	public static final String PUSH_INTENT_ACTION_INCOMING_CALL = "PUSH_INTENT_ACTION_INCOMING_CALL";
	public static final String PUSH_MSG_TYPE_REGISTER_VIRTUAL_NUMBER = "register_virtual_number";

	// push notification id
	public static final int NOTIFICATION_ID_CATCH_CALL = 1;
	public static final int NOTIFICATION_ID_BIDDING = 2;

	public static final int LOCATION_UPDATE_DISTANCE_MIN = 100; // meter
}
