package kr.driver.cardari.cardari.data;

import android.os.Parcel;
import android.os.Parcelable;

public class CallInfoUserData implements Parcelable {
	private String userId;
	private String phone;
	private String address;
	private String city_do;
	private String gu_gun;
	private CallPositionData location;

	public CallInfoUserData() {}

	public CallInfoUserData(String userId, String phone, String address, String city_do, String gu_gun, CallPositionData location) {
		this.userId = userId;
		this.phone = phone;
		this.address = address;
		this.city_do = city_do;
		this.gu_gun = gu_gun;
		this.location = location;
	}

	public CallInfoUserData(Parcel in) {
		userId = in.readString();
		phone = in.readString();
		address = in.readString();
		city_do = in.readString();
		gu_gun = in.readString();
		location = in.readParcelable(CallPositionData.class.getClassLoader());
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeString(userId);
		parcel.writeString(phone);
		parcel.writeString(address);
		parcel.writeString(city_do);
		parcel.writeString(gu_gun);
		parcel.writeParcelable(location, flags);
	}

	public static final Creator<CallInfoData> CREATOR = new Creator<CallInfoData>() {
		@Override
		public CallInfoData createFromParcel(Parcel in) {
			return new CallInfoData(in);
		}

		@Override
		public CallInfoData[] newArray(int size) {
			return new CallInfoData[size];
		}
	};
}
