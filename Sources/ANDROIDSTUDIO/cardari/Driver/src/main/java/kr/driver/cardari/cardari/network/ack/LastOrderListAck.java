package kr.driver.cardari.cardari.network.ack;

import java.util.List;

import kr.driver.cardari.cardari.data.LastOrderData;
import kr.driver.cardari.cardari.data.OrderData;

/**
 * Created by eklee on 2017. 11. 1..
 */

public class LastOrderListAck extends BaseAck {
    private List<LastOrderData> lastOrderDataList;

    public LastOrderListAck() {
    }

    public LastOrderListAck(NetResultData result, List<LastOrderData> lastOrderDataList) {
        super(result);
        this.lastOrderDataList = lastOrderDataList;
    }

    public List<LastOrderData> getLastOrderDataList() {
        return lastOrderDataList;
    }

    public void setLastOrderDataList(List<LastOrderData> lastOrderDataList) {
        this.lastOrderDataList = lastOrderDataList;
    }
}
