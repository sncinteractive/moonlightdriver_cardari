package kr.driver.cardari.cardari.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.Enums.OrderQueryType;
import kr.driver.cardari.cardari.common.PrefsHelper;

import static kr.driver.cardari.cardari.common.PrefsHelper.PREFS_KEY_ORDER_QUERY_INCLUDE_REFUSAL;
import static kr.driver.cardari.cardari.common.PrefsHelper.PREFS_KEY_ORDER_QUERY_TYPE;

/**
 * Created by eklee on 2017. 11. 13..
 */

public class QuickSettingQuerySettingDialog extends Dialog {
    public interface EventListener {
        void onSettingChanged();
    }

    private static final String TAG = "QuickSettingQuerySettingDialog";

    @BindView(R.id.order_query_setting_radio_group) RadioGroup mQueryTypeRadioGroup;
    @BindView(R.id.order_query_setting_include_refusal_order_check_box) CheckBox mIncludeRefusalOrderCheckBox;
    @BindView(R.id.order_query_setting_confirm_button) Button mConfirmButton;

    private @OrderQueryType String mOrderQueryTypePrev;
    private boolean mOrderQueryIncludeRefusalPrev;
    private EventListener mEventListener;

    public QuickSettingQuerySettingDialog(@NonNull Context context, EventListener listener) {
        super(context, R.style.Theme_AppCompat_Translucent);

        mOrderQueryTypePrev = PrefsHelper.getString(PREFS_KEY_ORDER_QUERY_TYPE, OrderQueryType.ALL);
        mOrderQueryIncludeRefusalPrev = PrefsHelper.getBoolean(PREFS_KEY_ORDER_QUERY_INCLUDE_REFUSAL, false);

        mEventListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_quick_setting_query_setting);
        ButterKnife.bind(this);

        initializeView();
    }

    private void initializeView() {
        switch (mOrderQueryTypePrev) {
            case OrderQueryType.ALL: {
                mQueryTypeRadioGroup.check(R.id.order_query_setting_all_radio_button);
            }
            break;
//            case OrderQueryType.AUCTION: {
//                mQueryTypeRadioGroup.check(R.id.order_query_setting_auction_radio_button);
//            }
//            break;
            case OrderQueryType.NORMAL: {
                mQueryTypeRadioGroup.check(R.id.order_query_setting_normal_radio_button);
            }
            break;
        }

        mIncludeRefusalOrderCheckBox.setChecked(mOrderQueryIncludeRefusalPrev);
    }

    @OnClick({R.id.order_query_setting_confirm_button})
    public void onClickConfirmButton() {
        if (mEventListener != null) {
            @OrderQueryType String orderQueryTypeNew;
            switch (mQueryTypeRadioGroup.getCheckedRadioButtonId()) {
                case R.id.order_query_setting_all_radio_button: {
                    orderQueryTypeNew = OrderQueryType.ALL;
                }
                break;
//                case R.id.order_query_setting_auction_radio_button: {
//                    orderQueryTypeNew = OrderQueryType.AUCTION;
//                }
//                break;
                case R.id.order_query_setting_normal_radio_button: {
                    orderQueryTypeNew = OrderQueryType.NORMAL;
                }
                break;
                default:
                    throw new RuntimeException("onClickConfirmButton - not implemented order query type");
            }
            boolean orderQueryIncludeRefusalNew = mIncludeRefusalOrderCheckBox.isChecked();

            if (!mOrderQueryTypePrev.equals(orderQueryTypeNew) || mOrderQueryIncludeRefusalPrev != orderQueryIncludeRefusalNew) {

                PrefsHelper.putString(PREFS_KEY_ORDER_QUERY_TYPE, orderQueryTypeNew);
                PrefsHelper.putBoolean(PREFS_KEY_ORDER_QUERY_INCLUDE_REFUSAL, orderQueryIncludeRefusalNew);

                mEventListener.onSettingChanged();
            }
        }

        dismiss();
    }
}
