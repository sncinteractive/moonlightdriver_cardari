package kr.driver.cardari.cardari.data;

import android.os.Parcel;
import android.os.Parcelable;

import kr.driver.cardari.cardari.common.Enums;

/**
 * Created by eklee on 2017. 11. 23..
 */

public class DriverData implements Parcelable {
    private String driverId;
    private String phone;
    private String phone1;
    private String uniqueId;
    private String name;
    private String licenseType;
    private String licenseNumber;
    private String driverStatus;        // 기사 심사 통과 여부
    private String insuranceStatus;
    private String apiDriverId;
    private String insureNumber;
    private String email;
    private int balance;
    private String gcmId;
    private String version;
    private String image;
    private String ynLogin;
    private int point;
    private String macAddr;
    private int star;
    private int starnum;
    private @Enums.DriverState int mDriverCallInfoState;    // 기사가 잡은 콜 상태
    private long callUpdateTime;

    public DriverData() {
    }

    public DriverData(String driverId, String phone, String phone1, String uniqueId, String name, String licenseType, String licenseNumber, String driverStatus, String insuranceStatus, String apiDriverId, String insureNumber, String email, int balance, String gcmId, String version, String image, String ynLogin, int point, String macAddr, int star, int starnum, int mDriverCallInfoState, long callUpdateTime) {
        this.driverId = driverId;
        this.phone = phone;
        this.phone1 = phone1;
        this.uniqueId = uniqueId;
        this.name = name;
        this.licenseType = licenseType;
        this.licenseNumber = licenseNumber;
        this.driverStatus = driverStatus;
        this.insuranceStatus = insuranceStatus;
        this.apiDriverId = apiDriverId;
        this.insureNumber = insureNumber;
        this.email = email;
        this.balance = balance;
        this.gcmId = gcmId;
        this.version = version;
        this.image = image;
        this.ynLogin = ynLogin;
        this.point = point;
        this.macAddr = macAddr;
        this.star = star;
        this.starnum = starnum;
        this.mDriverCallInfoState = mDriverCallInfoState;
        this.callUpdateTime = callUpdateTime;
    }

    protected DriverData(Parcel in) {
        this.driverId = in.readString();
        this.phone = in.readString();
        this.phone1 = in.readString();
        this.uniqueId = in.readString();
        this.name = in.readString();
        this.licenseType = in.readString();
        this.licenseNumber = in.readString();
        this.driverStatus = in.readString();
        this.insuranceStatus = in.readString();
        this.apiDriverId = in.readString();
        this.insureNumber = in.readString();
        this.email = in.readString();
        this.balance = in.readInt();
        this.gcmId = in.readString();
        this.version = in.readString();
        this.image = in.readString();
        this.ynLogin = in.readString();
        this.point = in.readInt();
        this.macAddr = in.readString();
        this.star = in.readInt();
        this.starnum = in.readInt();
        this.mDriverCallInfoState = in.readInt();
        this.callUpdateTime = in.readLong();
    }

    public static final Creator<DriverData> CREATOR = new Creator<DriverData>() {
        @Override
        public DriverData createFromParcel(Parcel in) {
            return new DriverData(in);
        }

        @Override
        public DriverData[] newArray(int size) {
            return new DriverData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(driverId);
        dest.writeString(phone);
        dest.writeString(phone1);
        dest.writeString(uniqueId);
        dest.writeString(name);
        dest.writeString(licenseType);
        dest.writeString(licenseNumber);
        dest.writeString(driverStatus);
        dest.writeString(insuranceStatus);
        dest.writeString(apiDriverId);
        dest.writeString(insureNumber);
        dest.writeString(email);
        dest.writeInt(balance);
        dest.writeString(gcmId);
        dest.writeString(version);
        dest.writeString(image);
        dest.writeString(ynLogin);
        dest.writeInt(point);
        dest.writeString(macAddr);
        dest.writeInt(star);
        dest.writeInt(starnum);
        dest.writeInt(mDriverCallInfoState);
        dest.writeLong(callUpdateTime);
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getDriverStatus() {
        return driverStatus;
    }

    public void setDriverStatus(String driverStatus) {
        this.driverStatus = driverStatus;
    }

    public String getInsuranceStatus() {
        return insuranceStatus;
    }

    public void setInsuranceStatus(String insuranceStatus) {
        this.insuranceStatus = insuranceStatus;
    }

    public String getApiDriverId() {
        return apiDriverId;
    }

    public void setApiDriverId(String apiDriverId) {
        this.apiDriverId = apiDriverId;
    }

    public String getInsureNumber() {
        return insureNumber;
    }

    public void setInsureNumber(String insureNumber) {
        this.insureNumber = insureNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getGcmId() {
        return gcmId;
    }

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getYnLogin() {
        return ynLogin;
    }

    public void setYnLogin(String ynLogin) {
        this.ynLogin = ynLogin;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getMacAddr() {
        return macAddr;
    }

    public void setMacAddr(String macAddr) {
        this.macAddr = macAddr;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public int getStarnum() {
        return starnum;
    }

    public void setStarnum(int starnum) {
        this.starnum = starnum;
    }

    public int getmDriverCallInfoState() {
        return mDriverCallInfoState;
    }

    public void setmDriverCallInfoState(int mDriverCallInfoState) {
        this.mDriverCallInfoState = mDriverCallInfoState;
    }

    public long getCallUpdateTime() {
        return callUpdateTime;
    }

    public void setCallUpdateTime(long callUpdateTime) {
        this.callUpdateTime = callUpdateTime;
    }
}
