package kr.driver.cardari.cardari.network.ack;

import kr.driver.cardari.cardari.common.Enums;
import kr.driver.cardari.cardari.common.Enums.NetResultCode;

import static kr.driver.cardari.cardari.common.Enums.NetResultCode.*;

/**
 * Created by eklee on 2017. 11. 1..
 */

public class NetResultData {
    private @NetResultCode int code;
    private String detail;

    public NetResultData() {
        code = NetResultCode.UNKNOWN_ERROR;
        detail = "unknown error";
    }

    public NetResultData(@NetResultCode int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public @NetResultCode int getCode() {
        return code;
    }

    public void setCode(@NetResultCode int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
