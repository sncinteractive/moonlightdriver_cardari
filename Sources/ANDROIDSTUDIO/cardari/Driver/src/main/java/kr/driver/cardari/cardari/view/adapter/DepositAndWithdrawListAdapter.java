package kr.driver.cardari.cardari.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.Util;
import kr.driver.cardari.cardari.data.DailyIncomeData;
import kr.driver.cardari.cardari.data.DepositAndWithdrawData;
import kr.driver.cardari.cardari.logger.DLog;

/**
 * Created by eklee on 2017. 11. 7..
 */

public class DepositAndWithdrawListAdapter extends RecyclerView.Adapter {
    private static final String TAG = "DailyIncomeListAdapter";

    private List<DepositAndWithdrawData> mDepositAndWithdrawDataList;

    public DepositAndWithdrawListAdapter() {
    }

    public void setDataList(List<DepositAndWithdrawData> depositAndWithdrawDataList) {
        mDepositAndWithdrawDataList = depositAndWithdrawDataList;

        notifyDataSetChanged();
    }

    public DepositAndWithdrawData getItem(int position) {
        try {
            return mDepositAndWithdrawDataList.get(position);
        } catch (Exception e) {
            DLog.e(TAG, "getItem exception", e);
        }

        return null;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_deposit_and_withdraw_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).bind(mDepositAndWithdrawDataList.get(position));
    }

    @Override
    public int getItemCount() { return (mDepositAndWithdrawDataList != null ? mDepositAndWithdrawDataList.size() : 0); }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.date_text_view) TextView mDateTextView;
        @BindView(R.id.price_text_view) TextView mPriceTextView;
        @BindView(R.id.balance_text_view) TextView mBalanceTextView;
        @BindView(R.id.summary_text_view) TextView mSummaryTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(DepositAndWithdrawData data) {
            try {
                mDateTextView.setText(Util.convertYearMonthDayString(data.getDateTime()));
                mPriceTextView.setText(Util.convertNumberToCommaString(data.getPrice()));
                mBalanceTextView.setText(Util.convertNumberToCommaString(data.getBalance()));
                mSummaryTextView.setText(data.getSummary());
            } catch (Exception e) {
                DLog.e(TAG, "ViewHolder::Bind exception", e);
            }
        }
    }
}
