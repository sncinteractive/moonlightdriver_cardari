package kr.driver.cardari.cardari.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by eklee on 2017. 11. 7..
 */

public class DailyIncomeData implements Parcelable {
    private int sequence;
    private String startPointName;
    private String endPointName;
    private int elapsedTime; // second
    private int charge;

    public DailyIncomeData() {
    }

    public DailyIncomeData(int sequence,
                           String startPointName,
                           String endPointName,
                           int elapsedTime,
                           int charge) {
        this.sequence = sequence;
        this.startPointName = startPointName;
        this.endPointName = endPointName;
        this.elapsedTime = elapsedTime;
        this.charge = charge;
    }

    protected DailyIncomeData(Parcel in) {
        sequence = in.readInt();
        startPointName = in.readString();
        endPointName = in.readString();
        elapsedTime = in.readInt();
        charge = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(sequence);
        dest.writeString(startPointName);
        dest.writeString(endPointName);
        dest.writeInt(elapsedTime);
        dest.writeInt(charge);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DailyIncomeData> CREATOR = new Creator<DailyIncomeData>() {
        @Override
        public DailyIncomeData createFromParcel(Parcel in) {
            return new DailyIncomeData(in);
        }

        @Override
        public DailyIncomeData[] newArray(int size) {
            return new DailyIncomeData[size];
        }
    };

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public String getStartPointName() {
        return startPointName;
    }

    public void setStartPointName(String startPointName) {
        this.startPointName = startPointName;
    }

    public String getEndPointName() {
        return endPointName;
    }

    public void setEndPointName(String endPointName) {
        this.endPointName = endPointName;
    }

    public int getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(int elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public int getCharge() {
        return charge;
    }

    public void setCharge(int charge) {
        this.charge = charge;
    }
}
