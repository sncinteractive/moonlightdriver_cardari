package kr.driver.cardari.cardari.network;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.ParseException;
import kr.driver.cardari.cardari.common.Defines;
import kr.driver.cardari.cardari.common.DialogHelper;
import kr.driver.cardari.cardari.data.InsuranceAPIData;
import kr.driver.cardari.cardari.logger.DLog;
import kr.driver.cardari.cardari.network.ack.BaseAck;
import kr.driver.cardari.cardari.tmap.TMapHelper;
import kr.driver.cardari.cardari.tmap.TMapRouteData;

public class NetClient {
    private static final String TAG = "NetClient";

    // REAL SVR
    public static final String HOST_URL = "http://115.68.104.30:8888";
    public static final String IMAGE_SERVER_HOST = "http://115.68.122.108:8080";

    // SERVER URL
//    public static final String HOST_URL = "http://dev.qwerts.co.kr:8888";
//    public static final String IMAGE_SERVER_HOST = "http://dev.qwerts.co.kr:8889";

    public static final String URL_CHECK_VERSION = "/cardari/version";
    public static final String URL_LOGIN = "/cardari_driver/login"; //"/cardari_driver/register/auto";
	public static final String URL_REGISTER = "/cardari_driver/register";
    public static final String URL_SAVE_REQ_UNDER_WRITING_LOG = "/cardari_driver/req/underwritinglog";
    public static final String URL_SAVE_REQ_DRIVING_LOG = "/cardari_driver/req/drivinglog";
    public static final String URL_CHECK_REGISTER = "/cardari_driver/check/register";
    public static final String URL_CALL_LIST = "/call/list";
    public static final String URL_CALL_DETAIL = "/call/detail";
    public static final String URL_CATCH_CALL = "/call/catch";
    public static final String URL_REFUSE_CALL = "/call/refuse";
    public static final String URL_START_DRIVING = "/call/start";
    public static final String URL_CANCEL_CALL = "/call/cancel";
    public static final String URL_END_DRIVING = "/call/end";
    public static final String URL_BIDDING = "/call/bidding";
    public static final String URL_BIDDING_RESULT = "/call/bidding/cancel";
    public static final String URL_INCOME_LIST= "/cardari_driver/income_list";
    public static final String URL_DEPOSIT_AND_WITHDRAW_LIST = "/cardari_driver/deposit_and_withdraw_list";
    public static final String URL_LAST_ORDER_LIST = "/cardari_driver/last_order_list";
    public static final String URL_NOTICE_LIST = "/notice/user/list";
    public static final String URL_VIRTUAL_NUMBER_REGISTER = "/virtual_number/register";
    public static final String URL_VIRTUAL_NUMBER_UNREGISTER = "/virtual_number/unregister";

    // 보험 회사 기사 심사요청 URL
    public static final String INSURANCE_HOST = "http://125.209.197.202:3000";
    public static final String URL_UNDER_WRITING = INSURANCE_HOST + "/api/driver_underwriting";
    public static final String URL_DRIVING = INSURANCE_HOST + "/api/driving";

    // TMAP URL
    private static final String TMAP_ROUTE_URL = "https://api2.sktelecom.com/tmap/routes";

    private static AsyncHttpClient sAsyncHttpClient;

    private static Header[] sTMapHeaders;

    private static AsyncHttpClient getAsyncHttpClient() {
        try {
            if (sAsyncHttpClient == null) {
                sAsyncHttpClient = new AsyncHttpClient();
            }

            sAsyncHttpClient.setLoggingEnabled(false);
            sAsyncHttpClient.setTimeout(20 * 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sAsyncHttpClient;
    }

    public static void get(Context context, final String url, final Class cls, final RequestParams requestParams, final NetResponseCallback callback) {
        AsyncHttpClient asyncHttpClient = getAsyncHttpClient();

        asyncHttpClient.get(context, getRelativeURL(url), requestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                BaseAck result = null;
                try {
                    DLog.e(TAG, generateLogMsg("ge::onSuccess", getRelativeURL(url), cls, requestParams, "response", new String(responseBody)));
                    Gson gson = new Gson();
                    result = (BaseAck)gson.fromJson(new String(responseBody), cls);
                } catch (Exception e) {
                    DLog.e(TAG, generateLogMsg("get::onSuccess", getRelativeURL(url), cls, requestParams), e);
                } finally {
                    callback.onResponse(result);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                DLog.e(TAG, generateLogMsg("get::onFailure", getRelativeURL(url), cls, requestParams), error);
                callback.onResponse(null);
            }
        });
    }

    public static void post(Context context, final String url, final Class cls, final RequestParams requestParams, final NetResponseCallback callback) {
        AsyncHttpClient asyncHttpClient = getAsyncHttpClient();
        asyncHttpClient.post(context, getRelativeURL(url), requestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                BaseAck result = null;
                try {
                    DLog.e(TAG, generateLogMsg("post::onSuccess", getRelativeURL(url), cls, requestParams, "response", new String(responseBody)));
                    Gson gson = new Gson();
                    result = (BaseAck)gson.fromJson(new String(responseBody), cls);
                } catch (Exception e) {
                    DLog.e(TAG, generateLogMsg("post::onSuccess", getRelativeURL(url), cls, requestParams), e);
                } finally {
                    callback.onResponse(result);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                DLog.e(TAG, generateLogMsg("post::onFailure", getRelativeURL(url), cls, requestParams), error);
                callback.onResponse(null);
            }
        });
    }

    private static String generateLogMsg(String msg, String url, Class<BaseAck> cls, RequestParams requestParams) {
        return generateLogMsg(msg, url, cls, requestParams, null, null);
    }

    private static String generateLogMsg(String msg, String url, Class<BaseAck> cls, RequestParams requestParams, String extraTitle, String extraValue) {
        StringBuilder sb = new StringBuilder();
        sb.append(msg).append(".");
        if (extraTitle != null) {
            sb.append(" ").append(extraTitle).append(" =>");
        }
        if (extraValue != null) {
            sb.append(" ").append(extraValue);
        }
        sb.append(" url => ").append(url);

        sb.append(" ack class => ").append(cls != null ? cls.getClass().getName() : "null");
        sb.append(" reqParams => ").append(requestParams != null ? requestParams.toString() : "null");

        return sb.toString();
    }

    private static String getRelativeURL(String url) {
        return HOST_URL + url;
    }

    // region - TMap REST API
    public interface TMapRouteResponseCallback {
        void onResponse(TMapRouteData result);
    }

    public static void postTMapRoute(final Context context, final RequestParams requestParams, final TMapRouteResponseCallback callback) {
        if (sTMapHeaders == null) {
            sTMapHeaders = createTMapHeaders();
        }

        getAsyncHttpClient().post(context, TMAP_ROUTE_URL, sTMapHeaders, requestParams, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                TMapRouteData result = null;
                try {
                    DLog.d(TAG, generateLogMsg("postTMapRoute::onSuccess", TMAP_ROUTE_URL, null, requestParams, "response", new String(responseBody)));
                    Gson gson = new Gson();
                    JsonObject jsonObject = gson.fromJson(new String(responseBody), JsonObject.class);
                    result = TMapHelper.parseJsonToTMapRouteData(jsonObject);
                } catch (Exception e) {
                    DLog.e(TAG, generateLogMsg("postTMapRoute::onSuccess", TMAP_ROUTE_URL, null, requestParams), e);
                } finally {
                    callback.onResponse(result);
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                DLog.e(TAG, generateLogMsg("postTMapRoute::onFailure", TMAP_ROUTE_URL, null, requestParams), error);

                if (error instanceof java.net.SocketTimeoutException) {
                    DialogHelper.showAlertDialog(context,
                        "알림",
                        "네트워크 연결이 원활하지 않습니다.\n다시 시도해주십시오.",
                        "확인",
                        null,
                        new DialogHelper.AlertDialogButtonCallback() {
                            @Override
                            public void onClickButton(boolean isPositiveClick) {
                                if (isPositiveClick) {
                                    callback.onResponse(null);
                                    //android.os.Process.killProcess(android.os.Process.myPid());
                                }
                            }
                        }
                    );
                } else {
//                    DialogHelper.showAlertDialog(context,
//                        "알림",
//                        "알수없는 오류가 발생했습니다.\n다시 시도해주십시오.",
//                        "확인",
//                        null,
//                        new DialogHelper.AlertDialogButtonCallback() {
//                            @Override
//                            public void onClickButton(boolean isPositiveClick) {
//                                if (isPositiveClick) {
//                                    callback.onResponse(null);
//                                }
//                            }
//                        }
//                    );
                }
            }
        });
    }

    private static Header[] createTMapHeaders() {
        List<Header> headerList = new ArrayList<>();

        headerList.add(new Header() {
            @Override
            public String getName() {
                return "appKey";
            }

            @Override
            public String getValue() {
                return Defines.TMAP_API_KEY;
            }

            @Override
            public HeaderElement[] getElements() throws ParseException {
                return new HeaderElement[0];
            }
        });

        headerList.add(new Header() {
            @Override
            public String getName() {
                return "Accept-Language";
            }

            @Override
            public String getValue() {
                return "ko";
            }

            @Override
            public HeaderElement[] getElements() throws ParseException {
                return new HeaderElement[0];
            }
        });

        headerList.add(new Header() {
            @Override
            public String getName() {
                return "Accept";
            }

            @Override
            public String getValue() {
                return "application/json";
            }

            @Override
            public HeaderElement[] getElements() throws ParseException {
                return new HeaderElement[0];
            }
        });

        return headerList.toArray(new Header[(headerList.size())]);
    }

    // endregion - TMap REST API


    // region - 보험회사 REST API
    public interface InsuranceAPIResponseCallback {
        void onResponse(InsuranceAPIData result);
    }

    public static void postInsuranceUnderWriting(final Context context, final RequestParams requestParams, final InsuranceAPIResponseCallback callback) {
        getAsyncHttpClient().post(context, URL_UNDER_WRITING, requestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                InsuranceAPIData result = null;
                try {
                    DLog.d(TAG, generateLogMsg("postInsuranceUnderWriting::onSuccess", URL_UNDER_WRITING, null, requestParams, "response", new String(responseBody)));
                    Gson gson = new Gson();
                    result = gson.fromJson(new String(responseBody), InsuranceAPIData.class);
                } catch (Exception e) {
                    DLog.e(TAG, generateLogMsg("postInsuranceUnderWriting::onSuccess", URL_UNDER_WRITING, null, requestParams), e);
                } finally {
                    callback.onResponse(result);
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                DLog.e(TAG, generateLogMsg("postInsuranceUnderWriting::onFailure", URL_UNDER_WRITING, null, requestParams), error);

                if (error instanceof java.net.SocketTimeoutException) {
                    DialogHelper.showAlertDialog(context,
                            "알림",
                            "네트워크 연결이 원활하지 않습니다.\n다시 시도해주십시오.",
                            "확인",
                            null,
                            new DialogHelper.AlertDialogButtonCallback() {
                                @Override
                                public void onClickButton(boolean isPositiveClick) {
                                    if (isPositiveClick) {
                                        callback.onResponse(null);
                                        //android.os.Process.killProcess(android.os.Process.myPid());
                                    }
                                }
                            }
                    );
                } else {
//                    DialogHelper.showAlertDialog(context,
//                            "알림",
//                            "알수없는 오류가 발생했습니다.\n다시 시도해주십시오.",
//                            "확인",
//                            null,
//                            new DialogHelper.AlertDialogButtonCallback() {
//                                @Override
//                                public void onClickButton(boolean isPositiveClick) {
//                                    if (isPositiveClick) {
//                                        callback.onResponse(null);
//                                    }
//                                }
//                            }
//                    );
                }
            }
        });
    }

    public static void postInsuranceDriving(final Context context, final RequestParams requestParams, final InsuranceAPIResponseCallback callback) {
        getAsyncHttpClient().post(context, URL_DRIVING, requestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                InsuranceAPIData result = null;
                try {
                    DLog.d(TAG, generateLogMsg("postInsuranceDriving::onSuccess", URL_DRIVING, null, requestParams, "response", new String(responseBody)));
                    Gson gson = new Gson();
                    result = gson.fromJson(new String(responseBody), InsuranceAPIData.class);
                } catch (Exception e) {
                    DLog.e(TAG, generateLogMsg("postInsuranceDriving::onSuccess", URL_DRIVING, null, requestParams), e);
                } finally {
                    callback.onResponse(result);
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                DLog.e(TAG, generateLogMsg("postInsuranceDriving::onFailure", URL_DRIVING, null, requestParams), error);

                if (error instanceof java.net.SocketTimeoutException) {
                    DialogHelper.showAlertDialog(context,
                            "알림",
                            "네트워크 연결이 원활하지 않습니다.\n다시 시도해주십시오.",
                            "확인",
                            null,
                            new DialogHelper.AlertDialogButtonCallback() {
                                @Override
                                public void onClickButton(boolean isPositiveClick) {
                                    if (isPositiveClick) {
                                        callback.onResponse(null);
                                        //android.os.Process.killProcess(android.os.Process.myPid());
                                    }
                                }
                            }
                    );
                } else {
//                    DialogHelper.showAlertDialog(context,
//                            "알림",
//                            "알수없는 오류가 발생했습니다.\n다시 시도해주십시오.",
//                            "확인",
//                            null,
//                            new DialogHelper.AlertDialogButtonCallback() {
//                                @Override
//                                public void onClickButton(boolean isPositiveClick) {
//                                    if (isPositiveClick) {
//                                        callback.onResponse(null);
//                                    }
//                                }
//                            }
//                    );
                }
            }
        });
    }

    // endregion - 보험회사 REST API
}
