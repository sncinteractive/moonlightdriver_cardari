package kr.driver.cardari.cardari.common;

import android.support.annotation.IntDef;
import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by eklee on 2017. 7. 12..
 * Enum은 integer constant 대비 최종 dex file의 크기를 13배 증가시킴.
 * Enum 값 하나 마다 object로 생성하기 때문인데 결국 heap memory도 많이 먹을수 밖에 없음
 *
 * @IntDef, @StringDef annotation을 사용하면 enum처럼 사용가능한데다 파일 크기, 메모리에서 이득을 볼 수 있다
 * 프로그램의 크기가 작을 경우 별로 상관없겠으나 습관을 위해 위와 같이 사용하자
 * 또 하나 장점은 Parcelable 구현시 int or string 이기 때문에 번거롭지 않게 쓸 수 있다.
 */

public class Enums {
    @IntDef({MenuVisibility.ALL, MenuVisibility.SUB_MENU, MenuVisibility.HIDE_ALL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface MenuVisibility {
        int ALL = 0;
        int SUB_MENU = 1;
        int HIDE_ALL = 2;
    }

    @IntDef({MainMenuType.ORDER, MainMenuType.ALLOCATION, MainMenuType.QUICK_SETTING, MainMenuType.SETTING, MainMenuType.SETTLEMENT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface MainMenuType {
        int ORDER = 0;
        int ALLOCATION = 1;
        int QUICK_SETTING = 2;
        int SETTING = 3;
        int SETTLEMENT = 4;
    }

    @IntDef({NetResultCode.UNKNOWN_ERROR, NetResultCode.SUCCESS, NetResultCode.VERSION_UPDATE, NetResultCode.NOT_REGISTERED_DRIVER, NetResultCode.FAIL_TO_LOGIN})
    @Retention(RetentionPolicy.SOURCE)
    public @interface NetResultCode {
        int UNKNOWN_ERROR = 0;
        int SUCCESS = 100;
        int VERSION_UPDATE = 236;
        int FAIL_TO_LOGIN = 262;
        int NOT_REGISTERED_DRIVER = 263;
    }

    @StringDef({PaymentOption.CASH, PaymentOption.CARD, PaymentOption.DISC})
    @Retention(RetentionPolicy.SOURCE)
    public @interface PaymentOption {
        String CASH = "cash";
        String CARD = "card";
        String DISC = "discount";
    }

    @IntDef({OrderType.AUCTION, OrderType.NORMAL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface OrderType {
        int AUCTION = 0;
        int NORMAL = 1;
    }

    @StringDef({OrderQueryType.ALL, OrderQueryType.AUCTION, OrderQueryType.NORMAL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface OrderQueryType {
        String ALL = "all";
        String AUCTION = "auction";
        String NORMAL = "normal";
    }

    @IntDef({RequestCode.ORDER_DETAIL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface RequestCode {
        int ORDER_DETAIL = 1;
    }

    @IntDef({BiddingResult.SUCCESS, BiddingResult.FAIL, BiddingResult.OTHER})
    @Retention(RetentionPolicy.SOURCE)
    public @interface BiddingResult {
        int SUCCESS = 0; // 낙찰
        int FAIL = 1; // 유찰
        int OTHER = 2; // 다른사람에게 낙찰
    }

    @IntDef({DriverState.IDLE, DriverState.WAIT_ALLOCATION, DriverState.ALLOCATION, DriverState.DRIVING })
    @Retention(RetentionPolicy.SOURCE)
    public @interface DriverState {
        int IDLE = 0;
        int WAIT_ALLOCATION = 1; // 배차 대기 상태 (결제는 아직 안된 상태)
        int ALLOCATION = 2; // 배차 상태
        int DRIVING = 3; // 운행중인 상태
    }

    @StringDef({CallState.WAIT, CallState.CATCH, CallState.START, CallState.END, CallState.CANCEL })
    @Retention(RetentionPolicy.SOURCE)
    public @interface CallState {
        String WAIT = "wait";   // 배차 대기 상태 (결제는 아직 안된 상태)
        String CATCH = "catch"; // 배차 상태
        String START = "start"; // 운행중인 상태
        String END = "end";     // 운행 종료 상태
        String CANCEL = "cancel";     // 배차 취소 상태
    }

    @IntDef({DistanceUnit.MILE, DistanceUnit.METER, DistanceUnit.KILOMETER})
    @Retention(RetentionPolicy.SOURCE)
    public @interface DistanceUnit {
        int MILE = 0;
        int METER = 1;
        int KILOMETER = 2;
    }

    @IntDef({MapViewType.PATH_FROM_DRIVER_TO_CLIENT, MapViewType.START_POINT, MapViewType.END_POINT, MapViewType.PATH_FROM_START_TO_END})
    @Retention(RetentionPolicy.SOURCE)
    public @interface MapViewType {
        int PATH_FROM_DRIVER_TO_CLIENT = 0;
        int START_POINT = 1;
        int END_POINT = 2;
        int PATH_FROM_START_TO_END = 3;
    }

    @IntDef({SettlementTab.DAILY_INCOME, SettlementTab.MONTHLY_INCOME, SettlementTab.DEPOSIT_AND_WITHDRAW_LIST, SettlementTab.LAST_ORDER_LIST})
    @Retention(RetentionPolicy.SOURCE)
    public @interface SettlementTab {
        int DAILY_INCOME = 0;
        int MONTHLY_INCOME = 1;
        int DEPOSIT_AND_WITHDRAW_LIST = 2;
        int LAST_ORDER_LIST = 3;
    }

    @IntDef({IncomeDateType.DAILY, IncomeDateType.MONTHLY})
    @Retention(RetentionPolicy.SOURCE)
    public @interface IncomeDateType {
        int DAILY = 0;
        int MONTHLY = 1;
    }
}