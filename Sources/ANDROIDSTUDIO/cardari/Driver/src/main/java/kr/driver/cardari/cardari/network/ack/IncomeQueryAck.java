package kr.driver.cardari.cardari.network.ack;

import java.util.List;

import kr.driver.cardari.cardari.data.DailyIncomeData;

/**
 * Created by eklee on 2017. 11. 1..
 */

public class IncomeQueryAck extends BaseAck {
    private int totalCount;
    private int totalIncome;
    private int depositAmount;
    private int balance;
    private int completionTotalCount;
    private int completionTotalAmount;
    private int penaltyTotalCount;
    private int penaltyTotalAmount;

    private List<DailyIncomeData> completionList;
    private List<DailyIncomeData> penaltyList;

    public IncomeQueryAck() {
    }

    public IncomeQueryAck(NetResultData result,
                          int totalCount,
                          int totalIncome,
                          int depositAmount,
                          int balance,
                          int completionTotalCount,
                          int completionTotalAmount,
                          int penaltyTotalCount,
                          int penaltyTotalAmount,
                          List<DailyIncomeData> completionList,
                          List<DailyIncomeData> penaltyList) {
        super(result);

        this.totalCount = totalCount;
        this.totalIncome = totalIncome;
        this.depositAmount = depositAmount;
        this.balance = balance;
        this.completionTotalCount = completionTotalCount;
        this.completionTotalAmount = completionTotalAmount;
        this.penaltyTotalCount = penaltyTotalCount;
        this.penaltyTotalAmount = penaltyTotalAmount;
        this.completionList = completionList;
        this.penaltyList = penaltyList;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getCompletionTotalCount() {
        return completionTotalCount;
    }

    public void setCompletionTotalCount(int completionTotalCount) {
        this.completionTotalCount = completionTotalCount;
    }

    public int getCompletionTotalAmount() {
        return completionTotalAmount;
    }

    public void setCompletionTotalAmount(int completionTotalAmount) {
        this.completionTotalAmount = completionTotalAmount;
    }

    public int getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(int depositAmount) {
        this.depositAmount = depositAmount;
    }

    public int getPenaltyTotalCount() {
        return penaltyTotalCount;
    }

    public void setPenaltyTotalCount(int penaltyTotalCount) {
        this.penaltyTotalCount = penaltyTotalCount;
    }

    public int getPenaltyTotalAmount() {
        return penaltyTotalAmount;
    }

    public void setPenaltyTotalAmount(int penaltyTotalAmount) {
        this.penaltyTotalAmount = penaltyTotalAmount;
    }

    public int getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(int totalIncome) {
        this.totalIncome = totalIncome;
    }

    public List<DailyIncomeData> getCompletionList() {
        return completionList;
    }

    public void setCompletionList(List<DailyIncomeData> completionList) {
        this.completionList = completionList;
    }

    public List<DailyIncomeData> getPenaltyList() {
        return penaltyList;
    }

    public void setPenaltyList(List<DailyIncomeData> penaltyList) {
        this.penaltyList = penaltyList;
    }
}
