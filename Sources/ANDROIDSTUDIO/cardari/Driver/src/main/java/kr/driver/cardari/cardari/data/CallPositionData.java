package kr.driver.cardari.cardari.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by eklee on 2017. 11. 7..
 */

public class CallPositionData implements Parcelable{
    private String address;
    private CallLocationData location;

    public CallPositionData() {
    }

    public CallPositionData(CallLocationData location, String address) {
        this.location = location;
        this.address = address;
    }

    protected CallPositionData(Parcel in) {
        address = in.readString();
        location = in.readParcelable(CallLocationData.class.getClassLoader());
    }

    public static final Creator<CallPositionData> CREATOR = new Creator<CallPositionData>() {
        @Override
        public CallPositionData createFromParcel(Parcel in) {
            return new CallPositionData(in);
        }

        @Override
        public CallPositionData[] newArray(int size) {
            return new CallPositionData[size];
        }
    };

    public CallLocationData getLocation() {
        return location;
    }

    public void setLocation(CallLocationData location) {
        this.location = location;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(address);
        dest.writeParcelable(location, flags);
    }
}
