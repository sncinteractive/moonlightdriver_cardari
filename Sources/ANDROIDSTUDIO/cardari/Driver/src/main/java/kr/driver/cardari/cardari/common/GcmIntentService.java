package kr.driver.cardari.cardari.common;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.logger.DLog;
import kr.driver.cardari.cardari.network.NetClient;
import kr.driver.cardari.cardari.view.activity.MainActivity;

/**
 * Created by youngmin on 2016-10-31.
 */

public class GcmIntentService extends IntentService {
    private static final String TAG = "GcmIntentService";

    public GcmIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            Bundle extras = intent.getExtras();

            if (extras != null) {
                GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

                String messageType = gcm.getMessageType(intent);

                if (!extras.isEmpty()) {
                    DLog.e(TAG, "Received: " + extras.toString());

                    if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                        sendNotification(extras);
                    }
                }

                GcmBroadcastReceiver.completeWakefulIntent(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendNotification(Bundle _args) {
        try {
            String pushMsgType = _args.getString("type", "");

            String title = getNotificationTitle(pushMsgType);
            if (title.isEmpty()) {
                return;
            }

            title = _args.getString("title", title);

            String imgUrl = _args.getString("imgUrl", "");
            Bitmap notifyImg = null;
            if (!imgUrl.isEmpty()) {
                notifyImg = Util.getImageDownload(NetClient.IMAGE_SERVER_HOST + imgUrl);
            }

            String content = _args.getString("message", "");
            Intent intent = createIntent(_args);

            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
            builder.setSmallIcon(R.mipmap.ic_launcher);
            builder.setWhen(System.currentTimeMillis());
            builder.setTicker(title);

            builder.setContentTitle(title);
            builder.setContentText(content);
            builder.setContentIntent(contentIntent);
            builder.setAutoCancel(true);
            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            builder.setLargeIcon(bm);
            builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

            if (Build.VERSION.SDK_INT > 15) {
                builder.setPriority(Notification.PRIORITY_MAX);
            }

            if (notifyImg != null) {
                NotificationCompat.BigPictureStyle style = new NotificationCompat.BigPictureStyle();
                style.setBigContentTitle(title);
                style.setSummaryText(content);
                style.bigPicture(notifyImg);
                builder.setStyle(style);
            } else {
                NotificationCompat.BigTextStyle textStyle = new NotificationCompat.BigTextStyle();
                textStyle.setBigContentTitle(title);
                textStyle.bigText(content);
                textStyle.setSummaryText(content);
                builder.setStyle(textStyle);
            }

            Notification notification;
            if (Build.VERSION.SDK_INT > 15) {
                notification = builder.build();
            } else {
                notification = builder.getNotification();
            }

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(Util.getNotificationId(pushMsgType), notification);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getNotificationTitle(String pushMsgType) {
        try {
            switch (pushMsgType) {
                case Defines.PUSH_MSG_TYPE_CATCH_CALL:
                    return "배차 성공";
                default:
                    return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private Intent createIntent(Bundle bundle) {
        Intent intent;
        String pushMsgType = bundle.getString("type", "");
        switch (pushMsgType) {
            case Defines.PUSH_MSG_TYPE_CATCH_CALL: {
                intent = new Intent(this, MainActivity.class);
            }
            break;
            default:
                return null;
        }

        intent.putExtra("pushMsgType", pushMsgType);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        return intent;
    }
}
