package kr.driver.cardari.cardari.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by eklee on 2017. 11. 7..
 */

public class CallLocationData implements Parcelable{
    private String type;
    public ArrayList<Double> coordinates;

    public CallLocationData() {
    }

    public CallLocationData(String type, ArrayList<Double> coordinates) {
        this.type = type;
        this.coordinates = coordinates;
    }

    protected CallLocationData(Parcel in) {
        type = in.readString();
        coordinates = in.readParcelable(ArrayList.class.getClassLoader());
    }

    public static final Creator<CallLocationData> CREATOR = new Creator<CallLocationData>() {
        @Override
        public CallLocationData createFromParcel(Parcel in) {
            return new CallLocationData(in);
        }

        @Override
        public CallLocationData[] newArray(int size) {
            return new CallLocationData[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<Double> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(ArrayList<Double> coordinates) {
        this.coordinates = coordinates;
    }

    public LatLng getCoordinatesLatLng() {
        return new LatLng(coordinates.get(1), coordinates.get(0));
    }

    public static Creator<CallLocationData> getCREATOR() {
        return CREATOR;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(coordinates);
        dest.writeString(type);
    }
}
