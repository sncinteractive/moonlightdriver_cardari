package kr.driver.cardari.cardari.view.activity;

import android.Manifest;
import android.animation.Animator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.driver.cardari.cardari.BuildConfig;
import kr.driver.cardari.cardari.MyApp;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.CommonDialog;
import kr.driver.cardari.cardari.common.Defines;
import kr.driver.cardari.cardari.common.DialogHelper;
import kr.driver.cardari.cardari.common.Enums;
import kr.driver.cardari.cardari.common.PrefsHelper;
import kr.driver.cardari.cardari.common.SendLocationService;
import kr.driver.cardari.cardari.common.Util;
import kr.driver.cardari.cardari.data.CallInfoData;
import kr.driver.cardari.cardari.data.NoticeData;
import kr.driver.cardari.cardari.logger.DLog;
import kr.driver.cardari.cardari.network.NetClient;
import kr.driver.cardari.cardari.network.NetResponseCallback;
import kr.driver.cardari.cardari.network.ack.BaseAck;
import kr.driver.cardari.cardari.network.ack.LoginAck;
import kr.driver.cardari.cardari.network.ack.NoticeListAck;

import static kr.driver.cardari.cardari.common.Defines.EXTRA_KEY_ORDER_ID;
import static kr.driver.cardari.cardari.common.Enums.NetResultCode;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private Context mContext;

    private ImageView mBgSplash;

    private long mLastPressTime;

    private GoogleCloudMessaging mGcm;

    private CallDriverReceiver mCallDriverReceiver;

    @BindView(R.id.policy_service_layout) RelativeLayout mPolicyServiceLayout;
    @BindView(R.id.btn_policy_ok) ImageButton mBtnPolicyOk;
    @BindView(R.id.btn_check_policy_all) ImageButton mBtnCheckPolicyAll;
    @BindView(R.id.btn_check_policy_service) ImageButton mBtnCheckPolicyService;
    @BindView(R.id.btn_check_policy_privacy) ImageButton mBtnCheckPolicyPrivacy;
    @BindView(R.id.btn_check_policy_location) ImageButton mBtnCheckPolicyLocation;

    @BindView(R.id.notice_layout) LinearLayout mNoticeLayout;
    @BindView(R.id.notice_title) TextView mNoticeTitle;
    @BindView(R.id.notice_content) TextView mNoticeContent;
    @BindView(R.id.notice_image) ImageView mNoticeImage;
    @BindView(R.id.notice_link) TextView mNoticeLink;
    @BindView(R.id.btn_not_allow_open) ImageButton mBtnNotAllowOpen;
    @BindView(R.id.btn_notice_close) ImageButton mBtnNoticeClose;

    private ArrayList<NoticeData> mNoticeDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            ButterKnife.bind(this);

            mPolicyServiceLayout.setVisibility(View.GONE);
            mNoticeImage.setVisibility(View.GONE);
            mNoticeLink.setVisibility(View.GONE);

            mContext = this;

            mLastPressTime = 0;

            mBgSplash = findViewById(R.id.bgSplash);
            mBgSplash.setVisibility(View.GONE);

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            startSplash();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        try {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            startSplash();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void startSplash() {
        try {
            mBgSplash.setVisibility(View.VISIBLE);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mBgSplash.setVisibility(View.GONE);
//                    checkNetworkStatus();

                    checkPlayServices();
                }
            }, 3000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getUserPhoneNumber() {
        try {
            if (checkPhoneNumberPermission()) {
                TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                if(tMgr.getLine1Number() != null && !tMgr.getLine1Number().isEmpty()) {
                    MyApp.getInstance().setUserPhoneNumber(Util.convertToLocalPhoneNumber(tMgr.getLine1Number()));
                }

                // TODO : TEST Phone Number
                if (BuildConfig.DEBUG) {
                    MyApp.getInstance().setUserPhoneNumber("01010000000");
                }

                if (MyApp.getInstance().getUserPhoneNumber() == null) {
                    CommonDialog.showDialogWithListener(mContext, "전화번호 조회에 실패했습니다.\n전화번호가 없으면 사용이 불가능합니다.", "확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Util.finishActivity((Activity) mContext);
                        }
                    });
                } else {
//                    checkPlayServices();
                    checkRegister();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void checkRegister() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;

            RequestParams requestParams = new RequestParams();
            requestParams.add("phone", MyApp.getInstance().getUserPhoneNumber());
            requestParams.add("macAddr", "");
            requestParams.add("gcmId", MyApp.getInstance().getRegistrationId());
            requestParams.add("version", version);
            requestParams.add("lat", String.valueOf(MyApp.getInstance().getCurLocation().latitude));
            requestParams.add("lng", String.valueOf(MyApp.getInstance().getCurLocation().longitude));

            NetClient.post(this, NetClient.URL_CHECK_REGISTER, BaseAck.class, requestParams, new NetResponseCallback(new CheckRegisterResponseListener()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class CheckRegisterResponseListener implements NetResponseCallback.NetResponseListener {
        @Override
        public void onResponse(BaseAck result) {
            try {
                if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
                    checkNetworkStatus();
                } else if (result != null && result.getResult().getCode() == NetResultCode.NOT_REGISTERED_DRIVER) {
                    CommonDialog.showDialogWithListener(mContext, "회원가입", "회원 가입 후 사용 가능합니다.\n회원 가입 하시겠습니까?", "회원가입", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            showPolicyPopup();
                        }
                    }, "종료", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Util.finishActivity((Activity) mContext);
                        }
                    });
                } else if (result != null && result.getResult().getCode() == NetResultCode.FAIL_TO_LOGIN) {
                    CommonDialog.showDialogWithListener(mContext, "심사가 완료 된 후 사용 가능합니다.", "확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Util.finishActivity((Activity) mContext);
                        }
                    });
                } else {
                    CommonDialog.showDialogWithListener(mContext, "사용자 인증에 실패했습니다.\n잠시후 다시 시도해주세요.", "확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Util.finishActivity((Activity) mContext);
                        }
                    });
                }
            } catch (Exception e) {
                DLog.e(TAG, "LoginResponseListener::onResponse exception", e);
            }
        }
    }

    private void startOrderListPage() {
        try {
            Intent intent;
            if (MyApp.getInstance().getDriverData().getmDriverCallInfoState() == Enums.DriverState.IDLE) {
                intent = new Intent(this, OrderListActivity.class);
            } else {
                intent = new Intent(this, AllocationActivity.class);
                String callId = PrefsHelper.getString(PrefsHelper.PREFS_KEY_CALL_ID, "");
                intent.putExtra(EXTRA_KEY_ORDER_ID, callId);
            }

            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkNetworkStatus() {
        try {
            int networkStatus = Util.GetLocationServiceStatus(mContext);

            if (networkStatus == 0) {
                try {
//					mContext.sendBroadcast(new Intent("com.skt.intent.action.GPS_TURN_ON"));
                    PopUpLocationSettingDialog(2);
                } catch (Exception e) {
                    PopUpLocationSettingDialog(0);
                }
            } else if (networkStatus == 2) {
                PopUpLocationSettingDialog(2);
            } else if (networkStatus == 1) {
                if (!Util.GetWifiOnStatus(mContext)) {
                    PopUpWifiSettingDialog();
                }
            }

            startLocationService();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            switch (requestCode) {
                case Defines.GPS_SETTING_REQ_CODE:
                case Defines.WIFI_SETTING_REQ_CODE:
                    startLocationService();
                    break;
                case Defines.REQUEST_GOOGLE_PLAY_SERVICES:
                    if (resultCode == Activity.RESULT_OK) {
//                        showPolicyPopup();
                        getUserPhoneNumber();
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        try {
            if (grantResults.length > 0) {
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        switch (requestCode) {
                            case Defines.PHONE_NUMBER_REQ_CODE:
                                getUserPhoneNumber();
                                break;
                            case Defines.ACCESS_LOCATION_REQ_CODE:
                                startLocationService();
                                break;
                        }
                    } else {
                        Toast.makeText(mContext, "권한 승인이 거부 되었습니다.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        try {
            long pressTime = System.currentTimeMillis();

            if (pressTime - mLastPressTime <= Defines.DOUBLE_PRESS_INTERVAL) {
                new AlertDialog.Builder(mContext)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setCancelable(false)
                    .setTitle("앱 종료")
                    .setMessage("앱을 종료하시겠습니까?")
                    .setPositiveButton("종료", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Util.finishActivity(MainActivity.this);
                        }
                    })
                    .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mLastPressTime = System.currentTimeMillis();
                        }
                    })
                    .show();
            } else {
                super.onBackPressed();

                mLastPressTime = pressTime;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PopUpLocationSettingDialog(int status) {
        try {
            String msg = "";

            if (status == 0) {
                msg = "카대리에서 내 위치 정보를 사용하려면, 단말기의 설정에서 '위치 서비스' 사용을 허용해주세요.";
            } else if (status == 2) {
                msg = "위치서비스에서 무선 네트워크 사용을 허용해주세요. 위치를 빠르게 찾을 수 있습니다.";
            }

            CommonDialog.showDialogWithListener(mContext, "위치 서비스", msg, "설정하기", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent viewIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    ((Activity) mContext).startActivityForResult(viewIntent, Defines.GPS_SETTING_REQ_CODE);
                }
            }, "취소", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PopUpWifiSettingDialog() {
        try {
            CommonDialog.showDialogWithListener(mContext, "Wi-Fi", "Wi-Fi를 켜면 위치 정확도를 높일 수 있습니다.", "설정하기", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent viewIntent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                    ((Activity) mContext).startActivityForResult(viewIntent, Defines.WIFI_SETTING_REQ_CODE);
                }
            }, "취소", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startLocationService() {
        try {
            DLog.e(TAG, "startLocationService()");

            if (checkLocationPermission()) {
                Intent serviceIntent = new Intent();
                serviceIntent.putExtra("start_from", "main");
                serviceIntent.setClass(this, SendLocationService.class);

                stopService(serviceIntent);

                startService(serviceIntent);

//                showPolicyPopup();
                registerGcmId();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkLocationPermission() {
        boolean isPermissionGranted = true;

        try {
            if (Build.VERSION.SDK_INT >= 23) {
                if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, Defines.ACCESS_LOCATION_REQ_CODE);
                    isPermissionGranted = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            isPermissionGranted = false;
        }

        return isPermissionGranted;
    }

    private boolean checkPhoneNumberPermission() {
        boolean isPermissionGranted = true;

        try {
            if (Build.VERSION.SDK_INT >= 23) {
                if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.READ_PHONE_STATE}, Defines.PHONE_NUMBER_REQ_CODE);
                    isPermissionGranted = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            isPermissionGranted = false;
        }

        return isPermissionGranted;
    }

    private void checkPlayServices() {
        try {
            GoogleApiAvailability api = GoogleApiAvailability.getInstance();
            int resultCode = api.isGooglePlayServicesAvailable(this);
            if (resultCode == ConnectionResult.SUCCESS) {
                onActivityResult(Defines.REQUEST_GOOGLE_PLAY_SERVICES, Activity.RESULT_OK, null);
            } else if (api.isUserResolvableError(resultCode) && api.showErrorDialogFragment(this, resultCode, Defines.REQUEST_GOOGLE_PLAY_SERVICES)) {

            } else {
                CommonDialog.showDialogWithListener(mContext, "지원하지 않는 단말기입니다.", "확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Util.finishActivity((Activity) mContext);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showPolicyPopup() {
        try {
            boolean isFirstLaunch = PrefsHelper.getBoolean(PrefsHelper.PREFS_KEY_FIRST_LAUNCH, true);

//            if (!isFirstLaunch) {
//                registerGcmId();
//                return;
//            }

            mPolicyServiceLayout.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void registerGcmId() {
        try {
            // If this check succeeds, proceed with normal processing.
            // Otherwise, prompt user to get valid Play Services APK.
            registerInBackground();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registerInBackground() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    if (mGcm == null) {
                        mGcm = GoogleCloudMessaging.getInstance(mContext);
                    }

                    MyApp.getInstance().setRegistrationId(mGcm.register(Defines.SENDER_ID));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void _void) {
                super.onPostExecute(_void);

                checkApplicationVersion();
            }
        }.execute();
    }

    private void checkApplicationVersion() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;

            // real code begin
            RequestParams requestParams = new RequestParams();
            requestParams.put("version", version);
            requestParams.put("os", "android");

            NetClient.post(this, NetClient.URL_CHECK_VERSION, BaseAck.class, requestParams, new NetResponseCallback(new CheckVersionResponseListener()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_policy_ok)
    public void onClickPolicyOk() {
        try {
            if (!(mBtnCheckPolicyLocation.isSelected() && mBtnCheckPolicyPrivacy.isSelected() && mBtnCheckPolicyService.isSelected())) {
                CommonDialog.showSimpleDialog(mContext, "달빛기사 이용약관 및 개인정보 취급 방침, 위치기반 서비스 이용 약관 모두 동의해 주세요. ");
                return;
            }

            PrefsHelper.putBoolean(PrefsHelper.PREFS_KEY_FIRST_LAUNCH, false);

            mPolicyServiceLayout.setVisibility(View.GONE);

            Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_check_policy_all)
    public void onClickCheckPolicyAll() {
        try {
            mBtnCheckPolicyAll.setSelected(!mBtnCheckPolicyAll.isSelected());

            if (mBtnCheckPolicyAll.isSelected()) {
                mBtnCheckPolicyService.setSelected(true);
                mBtnCheckPolicyPrivacy.setSelected(true);
                mBtnCheckPolicyLocation.setSelected(true);
            } else {
                mBtnCheckPolicyService.setSelected(false);
                mBtnCheckPolicyPrivacy.setSelected(false);
                mBtnCheckPolicyLocation.setSelected(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_check_policy_service)
    public void onClickCheckPolicyService() {
        try {
            mBtnCheckPolicyService.setSelected(!mBtnCheckPolicyService.isSelected());

            checkPolicyAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_check_policy_privacy)
    public void onClickCheckPolicyPrivacy() {
        try {
            mBtnCheckPolicyPrivacy.setSelected(!mBtnCheckPolicyPrivacy.isSelected());

            checkPolicyAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_check_policy_location)
    public void onClickCheckPolicyLocation() {
        try {
            mBtnCheckPolicyLocation.setSelected(!mBtnCheckPolicyLocation.isSelected());

            checkPolicyAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkPolicyAll() {
        try {
            if (!(mBtnCheckPolicyLocation.isSelected() && mBtnCheckPolicyPrivacy.isSelected() && mBtnCheckPolicyService.isSelected())) {
                mBtnCheckPolicyAll.setSelected(false);
            } else if (mBtnCheckPolicyLocation.isSelected() && mBtnCheckPolicyPrivacy.isSelected() && mBtnCheckPolicyService.isSelected()) {
                mBtnCheckPolicyAll.setSelected(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class CheckVersionResponseListener implements NetResponseCallback.NetResponseListener {
        @Override
        public void onResponse(BaseAck result) {
            try {
//                hideLoadingProgressBar();

                @NetResultCode int resultCode = result != null ? result.getResult().getCode() : NetResultCode.UNKNOWN_ERROR;
                switch (resultCode) {
                    case NetResultCode.SUCCESS:
                        userLogin();
                        break;
                    case NetResultCode.VERSION_UPDATE:
                        new AlertDialog.Builder(mContext)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setCancelable(false)
                            .setTitle("업데이트")
                            .setMessage("앱을 업데이트 하시겠습니까?")
                            .setPositiveButton("업데이트", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
                                    marketLaunch.setData(Uri.parse(Defines.APP_DOWNLOAD_URL));
                                    startActivity(marketLaunch);

                                    Util.finishActivity(MainActivity.this);
                                }
                            })
                            .setNegativeButton("종료", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Util.finishActivity(MainActivity.this);
                                }
                            })
                            .show();
                        break;
                    default:
                        CommonDialog.showDialogWithListener(mContext, "버전 조회에 실패했습니다.\n사용 불가능한 버전입니다.", "확인", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Util.finishActivity((Activity) mContext);
                            }
                        });
                        break;
                }
            } catch (Exception e) {
                DLog.e(TAG, "CheckVersionResponseListener::onResponse exception", e);
            }
        }
    }

    public void userLogin() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;

            RequestParams requestParams = new RequestParams();
            requestParams.add("phone", MyApp.getInstance().getUserPhoneNumber());
            requestParams.add("macAddr", "");
            requestParams.add("gcmId", MyApp.getInstance().getRegistrationId());
            requestParams.add("version", version);
            requestParams.add("lat", String.valueOf(MyApp.getInstance().getCurLocation().latitude));
            requestParams.add("lng", String.valueOf(MyApp.getInstance().getCurLocation().longitude));

            NetClient.post(this, NetClient.URL_LOGIN, LoginAck.class, requestParams, new NetResponseCallback(new LoginResponseListener()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class LoginResponseListener implements NetResponseCallback.NetResponseListener {
        @Override
        public void onResponse(BaseAck result) {
            try {
                if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
                    LoginAck ack = (LoginAck) result;

                    MyApp.getInstance().setDriverData(ack.getDriver());

                    // 콜 정보 받아서 화면 전화 처리 추가
                    CallInfoData callInfoData = ack.getCallInfo();
                    if (callInfoData == null) {
                        MyApp.getInstance().getDriverData().setmDriverCallInfoState(Enums.DriverState.IDLE);
                    } else {
                        PrefsHelper.putString(PrefsHelper.PREFS_KEY_CALL_ID, callInfoData.getCallId());

                        if (callInfoData.getStatus().equals("catch")) {
                            MyApp.getInstance().getDriverData().setmDriverCallInfoState(Enums.DriverState.ALLOCATION);
                        } else if (callInfoData.getStatus().equals("start")) {
                            MyApp.getInstance().getDriverData().setmDriverCallInfoState(Enums.DriverState.DRIVING);
                        } else {
                            MyApp.getInstance().getDriverData().setmDriverCallInfoState(Enums.DriverState.IDLE);
                        }
                    }

                    PrefsHelper.putString(PrefsHelper.PREFS_KEY_DRIVER_ID, ack.getDriver().getDriverId());

                    getNotice();
                } else if (result != null && result.getResult().getCode() == NetResultCode.NOT_REGISTERED_DRIVER) {
                    CommonDialog.showDialogWithListener(mContext, "회원가입", "회원 가입 후 사용 가능합니다.\n회원 가입 하시겠습니까?", "회원가입", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }, "종료", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Util.finishActivity((Activity) mContext);
                        }
                    });
                } else if (result != null && result.getResult().getCode() == NetResultCode.FAIL_TO_LOGIN) {
                    CommonDialog.showDialogWithListener(mContext, "심사가 완료 된 후 사용 가능합니다.", "확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Util.finishActivity((Activity) mContext);
                        }
                    });
                } else {
                    CommonDialog.showDialogWithListener(mContext, "사용자 인증에 실패했습니다.\n잠시후 다시 시도해주세요.", "확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Util.finishActivity((Activity) mContext);
                        }
                    });
                }
            } catch (Exception e) {
                DLog.e(TAG, "LoginResponseListener::onResponse exception", e);
            }
        }
    }

    private void getNotice() {
        try {
            mNoticeDataList = new ArrayList<>();

            NetClient.get(this, NetClient.URL_NOTICE_LIST, NoticeListAck.class, null, new NetResponseCallback(new NoticeListResponseListener()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class NoticeListResponseListener implements NetResponseCallback.NetResponseListener {
        @Override
        public void onResponse(BaseAck result) {
            try {
                if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
                    NoticeListAck ack = (NoticeListAck) result;

                    mNoticeDataList = ack.getNotices();

                    showNoticePopup();
                }
            } catch (Exception e) {
                DLog.e(TAG, "NoticeListResponseListener::onResponse exception", e);
            }
        }
    }

    private void showNoticePopup() {
        try {
            if (mNoticeDataList.size() <= 0) {
                startOrderListPage();
                return;
            }

            final NoticeData noticeData = mNoticeDataList.get(0);

            if (!checkAllowNotice(noticeData.getUserNoticeId())) {
                mNoticeDataList.remove(0);
                showNoticePopup();
                return;
            }

            initNoticePopup();

            mNoticeTitle.setText(noticeData.getTitle());
            mNoticeContent.setText(noticeData.getContents());

            if (!noticeData.getImagePath().isEmpty()) {
                new Util.DownloadImageTask(mNoticeImage).execute(NetClient.IMAGE_SERVER_HOST + noticeData.getImagePath());
            }

            if (!noticeData.getLinkUrl().isEmpty()) {
                mNoticeLink.setVisibility(View.VISIBLE);
                mNoticeLink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(noticeData.getLinkUrl()));
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

            mBtnNotAllowOpen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        mBtnNotAllowOpen.setSelected(!mBtnNotAllowOpen.isSelected());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            mBtnNoticeClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        closeNoticePopup(mBtnNotAllowOpen.isSelected(), noticeData.getUserNoticeId());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            mNoticeLayout.setVisibility(View.VISIBLE);

            mNoticeDataList.remove(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkAllowNotice(String _userNoticeId) {
        try {
            long noShowNoticeTime = PrefsHelper.getLong(_userNoticeId, 0);
            if ((System.currentTimeMillis() - noShowNoticeTime) < (7 * 24 * 60 * 60 * 1000)) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    private void initNoticePopup() {
        try {
            mNoticeTitle.setText("");
            mNoticeContent.setText("");
            mNoticeImage.setImageURI(null);
            mNoticeImage.setVisibility(View.GONE);
            mNoticeLink.setOnClickListener(null);
            mNoticeLink.setVisibility(View.GONE);
            mBtnNotAllowOpen.setSelected(false);
            mBtnNotAllowOpen.setOnClickListener(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void closeNoticePopup(boolean _isNotAllowNotice, String _userNoticeId) {
        try {
            if (_isNotAllowNotice) {
                PrefsHelper.putLong(_userNoticeId, System.currentTimeMillis());
            }

            mNoticeLayout.animate().setDuration(500).alpha(0f).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    try {
                        mNoticeLayout.setVisibility(View.GONE);
                        mNoticeLayout.setAlpha(1f);
                        showNoticePopup();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
//			initGpsInfo();
//			if(mGpsInfo != null && mGpsInfo.isConnected()) {
//				mGpsInfo.startLocationUpdates();
//			}

            mCallDriverReceiver = new CallDriverReceiver();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Defines.PUSH_INTENT_ACTION_CATCH_CALL);
            intentFilter.addAction(Defines.PUSH_INTENT_ACTION_BIDDING);
            registerReceiver(mCallDriverReceiver, intentFilter);

            processCallDriverPush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        overridePendingTransition(0, 0);

        try {
//			initGpsInfo();
//			if(mGpsInfo != null && mGpsInfo.isConnected()) {
//				mGpsInfo.stopLocationUpdates();n
//			}

            unregisterReceiver(mCallDriverReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class CallDriverReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                processCallDriverPush();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void processCallDriverPush() {
        try {
            String pushMsgType = PrefsHelper.getString(PrefsHelper.PREFS_KEY_LATEST_RECEIVED_PUSH_MSG_TYPE, "");

            switch (pushMsgType) {
                case Defines.PUSH_MSG_TYPE_CATCH_CALL: {
                    final String callId = PrefsHelper.getString(PrefsHelper.PREFS_KEY_CALL_ID, "");
                    final String callStatus = PrefsHelper.getString(PrefsHelper.PREFS_KEY_CALL_STATUS, "");
                    int biddingPrice = Integer.parseInt(PrefsHelper.getString(PrefsHelper.PREFS_KEY_BIDDING_PRICE, "0"));

                    DialogHelper.showAlertDialog(this,
                        getString(R.string.bidding_result_popup_title),
                        String.format(getString(R.string.bidding_result_popup_message_format), Util.convertNumberToCommaString(biddingPrice)),
                        getString(R.string.common_confirm),
                        null,
                        new DialogHelper.AlertDialogButtonCallback() {
                            @Override
                            public void onClickButton(boolean isPositiveClick) {
                                if (isPositiveClick) {
                                    if (callStatus.equals("catch")) {
                                        MyApp.getInstance().getDriverData().setmDriverCallInfoState(Enums.DriverState.ALLOCATION);
                                    } else if (callStatus.equals("start")) {
                                        MyApp.getInstance().getDriverData().setmDriverCallInfoState(Enums.DriverState.DRIVING);
                                    } else {
                                        MyApp.getInstance().getDriverData().setmDriverCallInfoState(Enums.DriverState.IDLE);
                                    }

                                    Intent intent = new Intent(MainActivity.this, AllocationActivity.class);
                                    intent.putExtra(EXTRA_KEY_ORDER_ID, callId);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }
                            }
                        });
                }
                break;
                default:
                    break;
            }

            PrefsHelper.putString(PrefsHelper.PREFS_KEY_LATEST_RECEIVED_PUSH_MSG_TYPE, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
