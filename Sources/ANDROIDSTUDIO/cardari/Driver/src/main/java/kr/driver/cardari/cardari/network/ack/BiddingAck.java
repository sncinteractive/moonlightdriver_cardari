package kr.driver.cardari.cardari.network.ack;

/**
 * Created by eklee on 2017. 11. 1..
 */

public class BiddingAck extends BaseAck {
    private boolean biddingSuccess;
    private int waitTime;

    public BiddingAck() {
    }

    public BiddingAck(NetResultData result, boolean biddingSuccess, int waitTime) {
        super(result);
        this.biddingSuccess = biddingSuccess;
        this.waitTime = waitTime;
    }

    public boolean isBiddingSuccess() {
        return biddingSuccess;
    }

    public void setBiddingSuccess(boolean biddingSuccess) {
        this.biddingSuccess= biddingSuccess;
    }

    public int getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(int waitTime) {
        this.waitTime = waitTime;
    }
}
