package kr.driver.cardari.cardari.view.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;

import com.loopj.android.http.RequestParams;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import kr.driver.cardari.cardari.MyApp;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.DialogHelper;
import kr.driver.cardari.cardari.common.Enums;
import kr.driver.cardari.cardari.common.Util;
import kr.driver.cardari.cardari.logger.DLog;
import kr.driver.cardari.cardari.network.NetClient;
import kr.driver.cardari.cardari.network.NetResponseCallback;
import kr.driver.cardari.cardari.network.ack.BaseAck;
import kr.driver.cardari.cardari.network.ack.DepositAndWithdrawListAck;
import kr.driver.cardari.cardari.view.adapter.DepositAndWithdrawListAdapter;

/**
 * Created by eklee on 2017. 11. 21..
 */

public class DepositAndWithdrawFragment extends Fragment {
    public interface EventListener {
        void needToShowLoadingProgressbar();

        void needToHideLoadingProgressbar();
    }

    private static final String TAG = "DepositAndWithdrawFragment";

    @BindView(R.id.date_from_button) Button mDateFromButton;
    @BindView(R.id.date_to_button) Button mDateToButton;
    @BindView(R.id.query_button) Button mQueryButton;
    @BindView(R.id.deposit_and_withdraw_list_recycler_view) RecyclerView mDepositAndWithdrawListRecyclerView;

    private Unbinder mUnBinder;
    private EventListener mEventListener;

    private DepositAndWithdrawListAdapter mDepositAndWithdrawListAdapter;

    private Calendar mCalendarFrom;
    private Calendar mCalendarTo;

    private int mPage;

    public static DepositAndWithdrawFragment createInstance() {
        return new DepositAndWithdrawFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_deposit_and_withdraw, container, false);
        mUnBinder = ButterKnife.bind(this, rootView);

        initialize();
        initializeView();

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            if (Build.VERSION.SDK_INT >= 23) {
                mEventListener = (EventListener) context;
            }
        } catch (Exception e) {
            DLog.e(TAG, "onAttach - " + context.toString() + " not implemented event listener", e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            if (Build.VERSION.SDK_INT < 23) {
                mEventListener = (EventListener) activity;
            }
        } catch (Exception e) {
            DLog.e(TAG, "onAttach - " + activity.toString() + " not implemented event listener", e);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mEventListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnBinder.unbind();
    }

    private void initialize() {
        mPage = 0;

        mCalendarTo = Calendar.getInstance();
        mCalendarTo.setTime(new Date()); // 현재 시간으로 초기화

        mCalendarFrom = Calendar.getInstance();
        mCalendarFrom.setTime(mCalendarTo.getTime());
        mCalendarFrom.add(Calendar.DAY_OF_MONTH, -7); // 1주일 전으로 초기화

        mDepositAndWithdrawListAdapter = new DepositAndWithdrawListAdapter();
        mDepositAndWithdrawListRecyclerView.setAdapter(mDepositAndWithdrawListAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mDepositAndWithdrawListRecyclerView.setLayoutManager(layoutManager);
    }

    private void initializeView() {
        applyDateFromDataToUI();
        applyDateToDataToUI();
    }

    private void applyDateFromDataToUI() {
        mDateFromButton.setText(Util.convertYearMonthDayString(mCalendarFrom));
    }

    private void applyDateToDataToUI() {
        mDateToButton.setText(Util.convertYearMonthDayString(mCalendarTo));
    }

    public void needToShowLoadingProgressbar() {
        try {
            mEventListener.needToShowLoadingProgressbar();
        } catch (Exception e) {
            DLog.e(TAG, "needToShowLoadingProgressbar exception", e);
        }
    }

    public void needToHideLoadingProgressbar() {
        try {
            mEventListener.needToHideLoadingProgressbar();
        } catch (Exception e) {
            DLog.e(TAG, "needToHideLoadingProgressbar exception", e);
        }
    }

    @OnClick(R.id.date_from_button)
    public void onClickDateFrom() {
        DialogHelper.showDatePickerDialog(getContext(), mCalendarFrom, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mCalendarFrom.set(year, month, dayOfMonth);
                applyDateFromDataToUI();
            }
        });
    }

    @OnClick(R.id.date_to_button)
    public void onClickDateTo() {
        DialogHelper.showDatePickerDialog(getContext(), mCalendarTo, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mCalendarTo.set(year, month, dayOfMonth);
                applyDateToDataToUI();
            }
        });
    }

    @OnClick(R.id.query_button)
    public void onClickQuery() {
        requestDepositAndWithdrawList();
    }

    private void requestDepositAndWithdrawList() {
        RequestParams requestParams = new RequestParams();
        requestParams.put("driverId", MyApp.getInstance().getDriverData().getDriverId());
        requestParams.put("from", mDateFromButton.getText().toString());
        requestParams.put("to", mDateToButton.getText().toString());
        requestParams.put("page", mPage);
        NetClient.post(getContext(), NetClient.URL_DEPOSIT_AND_WITHDRAW_LIST, DepositAndWithdrawListAck.class, requestParams, new NetResponseCallback(new NetResponseCallback.NetResponseListener() {
            @Override
            public void onResponse(BaseAck result) {
                processDepositAndWithdrawListResponse(result);
            }
        }));

        // test code begin
//        List<DepositAndWithdrawData> dataList = new ArrayList<>();
//        dataList.add(new DepositAndWithdrawData(mCalendarFrom.getTimeInMillis(), 10000, 10000, "입금"));
//        dataList.add(new DepositAndWithdrawData(mCalendarFrom.getTimeInMillis(), 15000, 25000, "입금"));
//        dataList.add(new DepositAndWithdrawData(mCalendarFrom.getTimeInMillis(), -25000, 0, "출금"));
//        dataList.add(new DepositAndWithdrawData(mCalendarFrom.getTimeInMillis(), 20000, 10000, "입금"));
//        dataList.add(new DepositAndWithdrawData(mCalendarFrom.getTimeInMillis(), 25000, 45000, "입금"));
//        dataList.add(new DepositAndWithdrawData(mCalendarFrom.getTimeInMillis(), 15000, 60000, "입금"));
//        dataList.add(new DepositAndWithdrawData(mCalendarFrom.getTimeInMillis(), 10000, 70000, "입금"));
//        dataList.add(new DepositAndWithdrawData(mCalendarFrom.getTimeInMillis(), 10000, 80000, "입금"));
//        dataList.add(new DepositAndWithdrawData(mCalendarFrom.getTimeInMillis(), 15000, 95000, "입금"));
//        dataList.add(new DepositAndWithdrawData(mCalendarFrom.getTimeInMillis(), 20000, 115000, "입금"));
//        dataList.add(new DepositAndWithdrawData(mCalendarFrom.getTimeInMillis(), 25000, 140000, "입금"));
//        dataList.add(new DepositAndWithdrawData(mCalendarFrom.getTimeInMillis(), -100000, 40000, "출금"));
//        dataList.add(new DepositAndWithdrawData(mCalendarFrom.getTimeInMillis(), 30000, 70000, "입금"));
//        dataList.add(new DepositAndWithdrawData(mCalendarFrom.getTimeInMillis(), 10000, 80000, "입금"));
//        dataList.add(new DepositAndWithdrawData(mCalendarFrom.getTimeInMillis(), 20000, 100000, "입금"));
//        processDepositAndWithdrawListResponse(new DepositAndWithdrawListAck(new NetResultData(Enums.NetResultCode.SUCCESS, null), dataList));
        // test code end
    }

    private void processDepositAndWithdrawListResponse(BaseAck result) {
        if (result != null && result.getResult().getCode() == Enums.NetResultCode.SUCCESS) {
            DepositAndWithdrawListAck ack = (DepositAndWithdrawListAck) result;

            mDepositAndWithdrawListAdapter.setDataList(ack.getDepositAndWithdrawDataList());
            mDepositAndWithdrawListRecyclerView.scrollToPosition(0);
        } else {
            DialogHelper.showAlertDialog(getContext(), "알림", "입출금 내역 조회 요청이 실패했습니다.\n" + "code: " + result.getResult().getCode() + "detail: " + result.getResult().getDetail(), "확인", null, null);
        }
    }
}