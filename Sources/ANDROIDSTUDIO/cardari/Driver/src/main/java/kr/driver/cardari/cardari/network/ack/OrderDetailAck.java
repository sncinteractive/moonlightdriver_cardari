package kr.driver.cardari.cardari.network.ack;

import kr.driver.cardari.cardari.data.OrderData;

/**
 * Created by eklee on 2017. 11. 1..
 */

public class OrderDetailAck extends BaseAck {
    private OrderData callDetail;

    public OrderDetailAck() {
    }

    public OrderDetailAck(OrderData callDetail) {
        this.callDetail = callDetail;
    }

    public OrderDetailAck(NetResultData result, OrderData callDetail) {
        super(result);
        this.callDetail = callDetail;
    }

    public OrderData getCallDetail() {
        return callDetail;
    }

    public void setCallDetail(OrderData callDetail) {
        this.callDetail = callDetail;
    }
}
