package kr.driver.cardari.cardari.tmap;

import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.loopj.android.http.RequestParams;
import com.skt.Tmap.TMapAddressInfo;
import com.skt.Tmap.TMapData;
import com.skt.Tmap.TMapPoint;
import com.skt.Tmap.TMapPolyLine;

import java.util.LinkedList;
import java.util.List;

import kr.driver.cardari.cardari.logger.DLog;

/**
 * Created by eklee on 2017. 11. 1..
 */

public class TMapHelper {
    private static final String TAG = "TMapHelper";

    /**
     * 위도, 경도 값을 주소로 반환 (TMapAddressInfo)
     *
     * @param listener 처리 결과를 받을 콜백 리스너
     * @param lat      위도
     * @param lng      경도
     */
    public static void convertGpsToAddress(ReverseGeocodingTask.ReverseGeocodingListener listener, double lat, double lng) {
        new ReverseGeocodingTask(listener, lat, lng).execute();
    }

    public static class ReverseGeocodingTask extends AsyncTask<Void, Integer, TMapAddressInfo> {
        public interface ReverseGeocodingListener {
            void onReverseGeocoding(TMapAddressInfo result);
        }

        private ReverseGeocodingListener mListener;
        private double mLat;
        private double mLng;

        public ReverseGeocodingTask(ReverseGeocodingListener listener, double lat, double lng) {
            mLat = lat;
            mLng = lng;
            mListener = listener;
        }

        @Override
        protected TMapAddressInfo doInBackground(Void... voids) {
            TMapData tMapData = new TMapData();
            try {
                return tMapData.reverseGeocoding(mLat, mLng, "A04");
            } catch (Exception e) {
                DLog.e(TAG, "ReverseGeocodingTask::doInBackground exception. " + e.toString(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(TMapAddressInfo result) {
            if (mListener != null) {
                mListener.onReverseGeocoding(result);
            }
        }
    }

    /**
     * 주어진 위치에 대한 경로를 탐색해서 반환 (TMapPolyLine)
     *
     * @param listener   처리 결과를 받을 콜백 리스너
     * @param startPoint 출발지
     * @param endPoint   도착지
     * @param passPoints 경유지 (없으면 null)
     *                   경로상의 점들은 TMapPolyLine.getLinePoint() 호출, 거리는 TMapPolyLine.getDistance() 호출
     *                   소요 시간 정보는 포함되어 있지 않음. 소요 시간도 구하려면 를 사용
     */
    public static void findPathData(FindPathDataAsyncTask.FindPathDataListener listener, TMapPoint startPoint, TMapPoint endPoint, TMapPoint... passPoints) {
        new FindPathDataAsyncTask(listener, startPoint, endPoint, passPoints).execute(startPoint, endPoint);
    }

    private static class FindPathDataAsyncTask extends AsyncTask<TMapPoint, Integer, TMapPolyLine> {
        public interface FindPathDataListener {
            void onFindPathData(TMapPolyLine result);
        }

        private FindPathDataListener mListener;
        private TMapPoint mStartPoint;
        private TMapPoint mEndPoint;
        private TMapPoint[] mPassList;

        public FindPathDataAsyncTask(FindPathDataAsyncTask.FindPathDataListener listener, TMapPoint startPoint, TMapPoint endPoint, TMapPoint... passPoints) {
            mListener = listener;
            mStartPoint = startPoint;
            mEndPoint = endPoint;
            mPassList = passPoints;
        }

        @Override
        protected TMapPolyLine doInBackground(TMapPoint... tMapPoints) {
            TMapData tMapData = new TMapData();
            try {
                return tMapData.findPathData(tMapPoints[0], tMapPoints[1]);
            } catch (Exception e) {
                DLog.e(TAG, "FindPathDataAsyncTask::doInBackground exception. " + e.toString(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(TMapPolyLine result) {
            if (mListener != null) {
                mListener.onFindPathData(result);
            }
        }
    }

    public static TMapRouteData parseJsonToTMapRouteData(JsonObject jsonObject) {
        try {
            if (jsonObject == null) {
                return null;
            }

            JsonArray features = jsonObject.getAsJsonArray("features");
            if (features == null || features.size() == 0) {
                return null;
            }

            /**
             * 첫 번째 feature의 properties에서 총 거리 및 소요시간을 구할 수 있다
             */
            int totalDistanceValue = 0;
            int totalTimeValue = 0;
            {
                JsonObject firstFeature = features.get(0).getAsJsonObject();
                JsonObject properties = firstFeature.getAsJsonObject("properties");
                JsonElement totalDistance = properties.get("totalDistance");
                if (totalDistance != null) {
                    totalDistanceValue = totalDistance.getAsInt();
                }
                JsonElement totalTime = properties.get("totalTime");
                if (totalTime != null) {
                    totalTimeValue = totalTime.getAsInt();
                }
            }

            /**
             * 첫번째 feature를 제외한 나머지 feature들의 geometry에서 type이 LineString인 경우에 coordinates를 파싱해서 좌표 리스트에 추가한다.
             * 경유지와 연결된 가상의 라인 표시용 좌표는 포함하지 않는다.
             */
            List<LatLng> resultCoordinateList = new LinkedList<>();
            int featureSize = features.size();
            for (int i = 1; i < featureSize; i++) {
                JsonObject feature = features.get(i).getAsJsonObject();
                JsonObject geometry = feature.getAsJsonObject("geometry");
                if (geometry == null) {
                    continue;
                }

                JsonElement type = geometry.get("type");
                if (type == null || !type.getAsJsonPrimitive().getAsString().equals("LineString")) {
                    continue;
                }

                JsonObject properties = feature.getAsJsonObject("properties");
                if (properties == null) {
                    continue;
                }

                JsonElement desc = properties.get("description");
                if (desc != null && desc.getAsJsonPrimitive().getAsString().equals("경유지와 연결된 가상의 라인입니다")) {
                    continue;
                }

                JsonArray coordinates = geometry.getAsJsonArray("coordinates");
                int coordinateSize = coordinates.size();
                for (int j = 0; j < coordinateSize; j++) {
                    JsonArray item = coordinates.get(j).getAsJsonArray();
                    LatLng newPoint = new LatLng(item.get(1).getAsJsonPrimitive().getAsDouble(), item.get(0).getAsJsonPrimitive().getAsDouble());
                    resultCoordinateList.add(newPoint);
                }
            }

            if (resultCoordinateList.size() == 0) {
                return null;
            }

            return new TMapRouteData(totalDistanceValue, totalTimeValue, resultCoordinateList);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String convertPassListToString(List<LatLng> passPoints) {
        try {
            if (passPoints == null || passPoints.isEmpty()) {
                return null;
            }

            StringBuilder sb = new StringBuilder();
            for (LatLng location : passPoints) {
                if (location == null) {
                    continue;
                }

                if (sb.length() != 0)
                    sb.append('_');

                sb.append(location.longitude);
                sb.append(',');
                sb.append(location.latitude);
            }
            return sb.toString();
        } catch (Exception e) {
            DLog.e(TAG, "convertPassListToString exception", e);
        }

        return null;
    }

    public static RequestParams generateTMapRouteRequestParams(LatLng startPoint, LatLng endPoint, List<LatLng> passPoints) {
        try {
            RequestParams requestParams = new RequestParams();
            requestParams.put("version", 1);
            requestParams.put("startX", String.valueOf(startPoint.longitude));
            requestParams.put("startY", String.valueOf(startPoint.latitude));
            requestParams.put("endX", String.valueOf(endPoint.longitude));
            requestParams.put("endY", String.valueOf(endPoint.latitude));
            requestParams.put("reqCoordType", "WGS84GEO");
            requestParams.put("resCoordType", "WGS84GEO");

            String passList = convertPassListToString(passPoints);
            if (passList != null) {
                requestParams.put("passList", passList);
            }

            return requestParams;
        } catch (Exception e) {
            DLog.e(TAG, "generateTMapRouteRequestParams exception", e);
        }

        return null;
    }
}
