package kr.driver.cardari.cardari.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.driver.cardari.cardari.R;

/**
 * Created by eklee on 2017. 11. 13..
 */

public class QuickSettingLocationSettingDialog extends Dialog {
    private static final String TAG = "QuickSettingLocationSettingDialog";

    @BindView(R.id.location_setting_title_text_view) TextView mLocationSettingTitleTextView;
    @BindView(R.id.location_setting_auto_button) Button mLocationSettingAutoButton;
    @BindView(R.id.location_setting_manual_button) Button mLocationSettingManualButton;
    @BindView(R.id.location_setting_close_button) Button mLocationSettingCloseButton;

    public QuickSettingLocationSettingDialog(@NonNull Context context) {
        super(context, R.style.Theme_AppCompat_Translucent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_quick_setting_location_setting);
        ButterKnife.bind(this);

        initializeView();
    }

    private void initializeView() {

    }

    @OnClick(R.id.location_setting_auto_button)
    public void onClickAuto() {
        dismiss();
    }

    @OnClick(R.id.location_setting_manual_button)
    public void onClickManual() {
        dismiss();
    }

    @OnClick(R.id.location_setting_close_button)
    public void onClickClose() {
        dismiss();
    }
}
