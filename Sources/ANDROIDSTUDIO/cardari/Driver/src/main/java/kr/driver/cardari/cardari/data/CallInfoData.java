package kr.driver.cardari.cardari.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by eklee on 2017. 11. 23..
 */

public class CallInfoData implements Parcelable {
    private String callId;
    private int callSeq;
    private String callType;
    private String status;
    private CallPositionData start;
    private CallPositionData through;
    private CallPositionData through1;
    private CallPositionData through2;
    private CallPositionData end;
    private int money;
    private String paymentOption;
    private CallInfoUserData user;
    private DriverData driver;
    private String etc;
    private String useReliefMessage;
    private String evaluatedYN;
    private String reportedUnfairnessYN;
    private long createdTime;

    public CallInfoData() {
    }

    public CallInfoData(String callId, int callSeq, String callType, String status, CallPositionData start, CallPositionData through, CallPositionData through1, CallPositionData through2, CallPositionData end, int money, String paymentOption, CallInfoUserData user, DriverData driver, String etc, String useReliefMessage, String evaluatedYN, String reportedUnfairnessYN, long createdTime) {
        this.callId = callId;
        this.callSeq = callSeq;
        this.callType = callType;
        this.status = status;
        this.start = start;
        this.through = through;
        this.through1 = through1;
        this.through2 = through2;
        this.end = end;
        this.money = money;
        this.paymentOption = paymentOption;
        this.user = user;
        this.driver = driver;
        this.etc = etc;
        this.useReliefMessage = useReliefMessage;
        this.evaluatedYN = evaluatedYN;
        this.reportedUnfairnessYN = reportedUnfairnessYN;
        this.createdTime = createdTime;
    }

    protected CallInfoData(Parcel in) {
        this.callId = in.readString();
        this.callSeq = in.readInt();
        this.callType = in.readString();
        this.status = in.readString();
        this.start = in.readParcelable(CallPositionData.class.getClassLoader());
        this.through = in.readParcelable(CallPositionData.class.getClassLoader());;
        this.through1 = in.readParcelable(CallPositionData.class.getClassLoader());;
        this.through2 = in.readParcelable(CallPositionData.class.getClassLoader());;
        this.end = in.readParcelable(CallPositionData.class.getClassLoader());;
        this.money = in.readInt();
        this.paymentOption = in.readString();
        this.user = in.readParcelable(CallInfoUserData.class.getClassLoader());
        this.driver = in.readParcelable(DriverData.class.getClassLoader());
        this.etc = in.readString();
        this.useReliefMessage = in.readString();
        this.evaluatedYN = in.readString();
        this.reportedUnfairnessYN = in.readString();
        this.createdTime = in.readLong();
    }

    public static final Creator<CallInfoData> CREATOR = new Creator<CallInfoData>() {
        @Override
        public CallInfoData createFromParcel(Parcel in) {
            return new CallInfoData(in);
        }

        @Override
        public CallInfoData[] newArray(int size) {
            return new CallInfoData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(callId);
        dest.writeInt(callSeq);
        dest.writeString(callType);
        dest.writeString(status);
        dest.writeParcelable(start, flags);
        dest.writeParcelable(through, flags);
        dest.writeParcelable(through1, flags);
        dest.writeParcelable(through2, flags);
        dest.writeParcelable(end, flags);
        dest.writeInt(money);
        dest.writeString(paymentOption);
        dest.writeParcelable(user, flags);
        dest.writeParcelable(driver, flags);
        dest.writeString(etc);
        dest.writeString(useReliefMessage);
        dest.writeString(evaluatedYN);
        dest.writeString(reportedUnfairnessYN);
        dest.writeLong(createdTime);
    }

    public String getCallId() {
        return callId;
    }

    public void setCallId(String callId) {
        this.callId = callId;
    }

    public int getCallSeq() {
        return callSeq;
    }

    public void setCallSeq(int callSeq) {
        this.callSeq = callSeq;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CallPositionData getStart() {
        return start;
    }

    public void setStart(CallPositionData start) {
        this.start = start;
    }

    public CallPositionData getThrough() {
        return through;
    }

    public void setThrough(CallPositionData through) {
        this.through = through;
    }

    public CallPositionData getThrough1() {
        return through1;
    }

    public void setThrough1(CallPositionData through1) {
        this.through1 = through1;
    }

    public CallPositionData getThrough2() {
        return through2;
    }

    public void setThrough2(CallPositionData through2) {
        this.through2 = through2;
    }

    public CallPositionData getEnd() {
        return end;
    }

    public void setEnd(CallPositionData end) {
        this.end = end;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public String getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(String paymentOption) {
        this.paymentOption = paymentOption;
    }

    public CallInfoUserData getUser() {
        return user;
    }

    public void setUser(CallInfoUserData user) {
        this.user = user;
    }

    public DriverData getDriver() {
        return driver;
    }

    public void setDriver(DriverData driver) {
        this.driver = driver;
    }

    public String getEtc() {
        return etc;
    }

    public void setEtc(String etc) {
        this.etc = etc;
    }

    public String getUseReliefMessage() {
        return useReliefMessage;
    }

    public void setUseReliefMessage(String useReliefMessage) {
        this.useReliefMessage = useReliefMessage;
    }

    public String getEvaluatedYN() {
        return evaluatedYN;
    }

    public void setEvaluatedYN(String evaluatedYN) {
        this.evaluatedYN = evaluatedYN;
    }

    public String getReportedUnfairnessYN() {
        return reportedUnfairnessYN;
    }

    public void setReportedUnfairnessYN(String reportedUnfairnessYN) {
        this.reportedUnfairnessYN = reportedUnfairnessYN;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }
}
