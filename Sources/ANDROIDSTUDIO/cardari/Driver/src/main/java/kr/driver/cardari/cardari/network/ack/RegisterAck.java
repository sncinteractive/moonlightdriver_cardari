package kr.driver.cardari.cardari.network.ack;

/**
 * Created by eklee on 2017. 11. 1..
 */

public class RegisterAck extends BaseAck {
    private String driverId;

    public RegisterAck() {
    }

    public RegisterAck(String driverId) {
        this.driverId = driverId;
    }

    public RegisterAck(NetResultData result, String driverId) {
        super(result);
        this.driverId = driverId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriver(String driverId) {
        this.driverId = driverId;
    }
}
