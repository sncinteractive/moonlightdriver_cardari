package kr.driver.cardari.cardari.network;

import kr.driver.cardari.cardari.network.ack.BaseAck;

public class NetResponseCallback {
    public interface NetResponseListener {
        void onResponse(BaseAck result);
    }

    private NetResponseListener mNetResponseListener;

    public NetResponseCallback(NetResponseListener listener) {
        mNetResponseListener = listener;
    }

    public void onResponse(BaseAck result) {
        mNetResponseListener.onResponse(result);
    }
}