package kr.driver.cardari.cardari.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.driver.cardari.cardari.MyApp;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.DialogHelper;
import kr.driver.cardari.cardari.common.Enums.BiddingResult;
import kr.driver.cardari.cardari.common.Enums.NetResultCode;
import kr.driver.cardari.cardari.common.Util;
import kr.driver.cardari.cardari.data.OrderData;
import kr.driver.cardari.cardari.logger.DLog;
import kr.driver.cardari.cardari.network.ack.BaseAck;
import kr.driver.cardari.cardari.network.ack.BiddingAck;
import kr.driver.cardari.cardari.network.ack.BiddingResultAck;
import kr.driver.cardari.cardari.network.ack.NetResultData;
import kr.driver.cardari.cardari.view.fragment.MapControlFragment;

import static kr.driver.cardari.cardari.common.Defines.EXTRA_KEY_ORDER_DATA;
import static kr.driver.cardari.cardari.common.Enums.DriverState;

public class AuctionOrderDetailActivity extends AppCompatActivity implements MapControlFragment.EventListener {
    private static final String TAG = "AuctionOrderDetailActivity";

    @BindView(R.id.title_text_view) TextView mTitleTextView;

    @BindView(R.id.order_info_layout) ConstraintLayout mOrderInfoLayout;
    @BindView(R.id.client_point_title_text_view) TextView mClientPointTitleTextView;
    @BindView(R.id.client_point_text_view) TextView mClientPointTextView;
    @BindView(R.id.start_point_title_text_view) TextView mStartPointTitleTextView;
    @BindView(R.id.start_point_text_view) TextView mStartPointTextView;
    @BindView(R.id.end_point_title_text_view) TextView mEndPointTitleTextView;
    @BindView(R.id.end_point_text_view) TextView mEndPointTextView;
    @BindView(R.id.distance_and_time_title_text_view) TextView mDistanceAndTimeTitleTextView;
    @BindView(R.id.distance_and_time_text_view) TextView mDistanceAndTimeTextView;
    @BindView(R.id.cur_min_bidding_price_title_text_view) TextView mCurMinBiddingPriceTitleTextView;
    @BindView(R.id.cur_min_bidding_price_text_view) TextView mCurMinBiddingPriceTextView;
    @BindView(R.id.biddable_price_title_text_view) TextView mBiddablePriceTitleTextView;
    @BindView(R.id.biddable_price_text_view) TextView mBiddablePriceTextView;

    @BindView(R.id.function_layout) ConstraintLayout mFunctionLayout;
    @BindView(R.id.decrease_button) Button mDecreaseButton;
    @BindView(R.id.bidding_price_text_view) TextView mBiddingPriceTextView;
    @BindView(R.id.increase_button) Button mIncreaseButton;
    @BindView(R.id.bidding_button) Button mBiddingButton;

    @BindView(R.id.loading_progressbar_layout) View mLoadingProgressbarLayout;


    private OrderData mOrderData;

    private Timer mTimer;

    private int mMoney = 0;

    private AlertDialog mBiddingWaitDialog;
    private int mBiddingWaitTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_auction_order_detail);
            ButterKnife.bind(this);

            Intent intent = getIntent();
            Bundle extras = intent.getExtras();
            if (extras != null) {
                mOrderData = extras.getParcelable(EXTRA_KEY_ORDER_DATA);
            }

            mMoney = mOrderData.getMoney();
            initializeView();
        } catch (Exception e) {
            DLog.e(TAG, "onCreate exception", e);
        }
    }

    private void initializeView() {
        try {
            // title
            mTimer = new Timer("auction order timer", true);
            mTimer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String timeText;
                            long remainTime = mOrderData.getBiddingEndTime() - System.currentTimeMillis();
                            if (remainTime > 1000) {
                                timeText = getString(R.string.normal_order_title);
                                long totalSeconds = remainTime / 1000;
                                long minute = totalSeconds / 60;
                                if (minute > 0) {
                                    timeText += "(" + minute + "분 " + String.format("%d초 남음", totalSeconds % 60) + ")";
                                } else {
                                    timeText += "(" + String.format("%d초 남음", totalSeconds % 60) + ")";
                                }
                            } else {
                                timeText = getString(R.string.bidding_time_over);

                                cancel();
                            }
                            mTitleTextView.setText(timeText);
                        }
                    });
                }
            }, 0, 1000);

            // map control fragment
            MapControlFragment mapControlFragment = MapControlFragment.createInstance(
                mOrderData.getClientLocation().getLocation().getCoordinatesLatLng(),
                mOrderData.getStartLocation().getLocation().getCoordinatesLatLng(),
                mOrderData.getEndLocation().getLocation().getCoordinatesLatLng(),
                mOrderData.getThroughLocationLatLngList());
            getSupportFragmentManager().beginTransaction().replace(R.id.map_control_fragment_frame, mapControlFragment).commit();

            // info
            mClientPointTextView.setText(Util.convertDistanceString((int)mOrderData.getDistance()));
            mStartPointTextView.setText(mOrderData.getStartLocation().getAddress());
            mEndPointTextView.setText(mOrderData.getEndLocation().getAddress());
            mDistanceAndTimeTextView.setText("?20km/50분"); // 거리/소요시간 계산
            mCurMinBiddingPriceTextView.setText(Util.convertNumberToCommaString(mOrderData.getCurMinBiddingPrice()));
            mBiddablePriceTextView.setText(String.format(Locale.US, getString(R.string.biddable_price_format), Util.convertNumberToCommaString(mOrderData.getBiddablePrice())));

            updateBiddingPriceText();
        } catch (Exception e) {
            DLog.e(TAG, "initializeView exception", e);
        }
    }

    @OnClick(R.id.back_button)
    public void onClickBack() {
        super.onBackPressed();
    }

    @Override
    public void onFullScreenModeChanged(boolean isFullScreen) {
        if (isFullScreen) {
            mOrderInfoLayout.setVisibility(View.GONE);
            mFunctionLayout.setVisibility(View.GONE);
        } else {
            mOrderInfoLayout.setVisibility(View.VISIBLE);
            mFunctionLayout.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.increase_button)
    public void onClickIncrease() {
        mMoney += 1000;
        updateBiddingPriceText();
    }

    @OnClick(R.id.decrease_button)
    public void onClickDecrease() {
        mMoney -= 1000;
        updateBiddingPriceText();
    }

    private void updateBiddingPriceText() {
        mBiddablePriceTextView.setText(Integer.toString(mMoney));
    }

    @OnClick(R.id.bidding_button)
    public void onClickBidding() {
        DialogHelper.showAlertDialog(this,
            getString(R.string.bidding_popup_title),
            String.format(getString(R.string.bidding_popup_message_format), Util.convertNumberToCommaString(mMoney)),
            getString(R.string.bidding_popup_confirm_title),
            getString(R.string.bidding_popup_cancel_title),
            new DialogHelper.AlertDialogButtonCallback() {
                @Override
                public void onClickButton(boolean isPositiveClick) {
                    if (isPositiveClick) {
                        requestBidding();
                    }
                }
            }
        );
    }

    private void requestBidding() {
        try {
            showLoadingProgressbar();

            // 콜비 수정 요청 서버 연동
//            RequestParams requestParams = new RequestParams();
//            requestParams.put("biddingPrice", mPayAmount);
//            NetClient.post(AuctionOrderDetailActivity.this, NetClient.URL_BIDDING, BiddingAck.class, requestParams, new NetResponseCallback(new NetResponseCallback.NetResponseListener() {
//                @Override
//                public void onResponse(BaseAck result) {
//                    processBiddingResponse(result);
//                }
//            }));

            processBiddingResponse(new BiddingAck(new NetResultData(NetResultCode.SUCCESS, null), true, 3));
        } catch (Exception e) {
            DLog.e(TAG, "requestBidding exception", e);
        }
    }

    private void processBiddingResponse(BaseAck result) {
        hideLoadingProgressbar();

        try {
            if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
                BiddingAck ack = (BiddingAck) result;
                if (ack.isBiddingSuccess()) {
                    mBiddingWaitTime = ack.getWaitTime();
                    String message = String.format(getString(R.string.bidding_wait_popup_message_format), mBiddingWaitTime);
                    mBiddingWaitDialog = DialogHelper.showAlertDialog(this,
                        getString(R.string.bidding_wait_popup_title),
                        message,
                        null,
                        null,
                        null);

                    mBiddingWaitDialog.setCancelable(false);

                    mTimer.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            mBiddingWaitTime -= 1;

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (mBiddingWaitTime > 0) {
                                        String message = String.format(getString(R.string.bidding_wait_popup_message_format), mBiddingWaitTime);
                                        mBiddingWaitDialog.setMessage(message);
                                    } else {
                                        cancel();
                                        requestBiddingResult();
                                    }
                                }
                            });
                        }
                    }, 0, 1000);
                } else {
                    DialogHelper.showAlertDialog(this,
                        "알림",
                        "콜비 수정 요청이 실패했습니다.",
                        null,
                        null,
                        null);
                }
            } else {
                DialogHelper.showAlertDialog(this, "알림", "콜비 수정 요청이 실패했습니다.\n" + "code: " + result.getResult().getCode() + "detail: " + result.getResult().getDetail(), "확인", null, null);
            }
        } catch (Exception e) {
            DLog.e(TAG, "processBiddingResponse exception", e);
        }
    }

    private void showLoadingProgressbar() {
        mLoadingProgressbarLayout.setVisibility(View.VISIBLE);
    }

    private void hideLoadingProgressbar() {
        mLoadingProgressbarLayout.setVisibility(View.GONE);
    }

    private void requestBiddingResult() {
        try {
            showLoadingProgressbar();

            // 콜비 수정 결과 요청 서버 연동
//            RequestParams requestParams = new RequestParams();
//            NetClient.get(AuctionOrderDetailActivity.this, NetClient.URL_BIDDING_RESULT, BiddingResultAck.class, requestParams, new NetResponseCallback(new NetResponseCallback.NetResponseListener() {
//                @Override
//                public void onResponse(BaseAck result) {
//                    processBiddingResultResponse(result);
//                }
//            }));

            // test code begin
            processBiddingResultResponse(new BiddingResultAck(new NetResultData(NetResultCode.SUCCESS, null), BiddingResult.SUCCESS, 29000, DriverState.ALLOCATION));
            // test code end
        } catch (Exception e) {
            DLog.e(TAG, "requestBidding exception", e);
        }
    }

    private void processBiddingResultResponse(BaseAck result) {
        try {
            mBiddingWaitDialog.dismiss();

            hideLoadingProgressbar();

            if (result != null && result.getResult().getCode() == NetResultCode.SUCCESS) {
                final BiddingResultAck ack = (BiddingResultAck) result;
                switch (ack.getBiddingResult()) {
                    // 나에게 낙찰
                    case BiddingResult.SUCCESS: {
                        DialogHelper.showAlertDialog(this,
                            getString(R.string.bidding_result_popup_title),
                            String.format(getString(R.string.bidding_result_popup_message_format), Util.convertNumberToCommaString(ack.getBiddingPrice())),
                            getString(R.string.common_confirm),
                            null,
                            new DialogHelper.AlertDialogButtonCallback() {
                                @Override
                                public void onClickButton(boolean isPositiveClick) {
                                    if (isPositiveClick) {
//                                        MyApp.getInstance().getDriverData().setDriverState(ack.getDriverState());
                                        MyApp.getInstance().setAllocatedOrderData(mOrderData);

                                        Intent intent = new Intent(AuctionOrderDetailActivity.this, AllocationActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }
                                }
                            });
                    }
                    break;
                    // 유찰
                    case BiddingResult.FAIL: {
                        // 콜 상세 정보 갱신

                    }
                    break;
                    // 다른 사람에게 낙찰됨
                    case BiddingResult.OTHER: {
                        Intent intent = new Intent(this, OrderListActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent); // OrderListActivity의 onNewIntent가 호출된다
                    }
                    break;
                }
            } else {
                DialogHelper.showAlertDialog(this, "알림", "콜비 수정 결과 요청이 실패했습니다.\n" + "code: " + result.getResult().getCode() + "detail: " + result.getResult().getDetail(), "확인", null, null);
            }
        } catch (Exception e) {
            DLog.e(TAG, "processBiddingResponse exception", e);
        }
    }
}
