package kr.driver.cardari.cardari.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.Util;

/**
 * Created by eklee on 2017. 11. 13..
 */

public class QuickSettingQueryRadiusSettingDialog extends Dialog {
    public interface EventListener {
        void onResult(int orderSearchRadius);
    }

    private static final String TAG = "QuickSettingQueryRadiusSettingDialog";

    @BindView(R.id.decrease_big_button) Button mDecreaseBigButton;
    @BindView(R.id.decrease_button) Button mDecreaseButton;
    @BindView(R.id.apply_button) Button mApplyButton;
    @BindView(R.id.increase_button) Button mIncreaseButton;
    @BindView(R.id.increase_big_button) Button mIncreaseBigButton;

    private int mOrderSearchRadius;
    private EventListener mEventListener;

    public QuickSettingQueryRadiusSettingDialog(@NonNull Context context, int orderSearchRadius, EventListener listener) {
        super(context, R.style.Theme_AppCompat_Translucent);
        mOrderSearchRadius = orderSearchRadius;
        mEventListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_search_radius_setting);
        ButterKnife.bind(this);

        applyRadius(mOrderSearchRadius);
    }

    @OnClick({R.id.decrease_big_button, R.id.decrease_button, R.id.increase_button, R.id.increase_big_button})
    public void onClickRadiusControlButton(View view) {
        int newRadius = mOrderSearchRadius;
        switch (view.getId()) {
            case R.id.decrease_big_button: {
                newRadius -= 1000;
            }
            break;
            case R.id.decrease_button: {
                newRadius -= 100;
            }
            break;
            case R.id.increase_button: {
                newRadius += 100;
            }
            break;
            case R.id.increase_big_button: {
                newRadius += 1000;
            }
            break;
            default:
                return;
        }

        if (newRadius != mOrderSearchRadius) {
            applyRadius(newRadius);
        }
    }

    @OnClick(R.id.apply_button)
    public void onClickApply() {
        mEventListener.onResult(mOrderSearchRadius);
        dismiss();
    }

    private void applyRadius(int radius) {
        final int MIN_VALUE = 0;
        final int MAX_VALUE = 1000 * 100; // meter
        mOrderSearchRadius = Math.min(Math.max(radius, MIN_VALUE), MAX_VALUE);
        mApplyButton.setText(Util.convertDistanceString(mOrderSearchRadius));
    }
}
