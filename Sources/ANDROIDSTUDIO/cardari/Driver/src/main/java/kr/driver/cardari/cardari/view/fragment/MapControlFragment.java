package kr.driver.cardari.cardari.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import kr.driver.cardari.cardari.MyApp;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.Defines;
import kr.driver.cardari.cardari.common.DialogHelper;
import kr.driver.cardari.cardari.logger.DLog;
import kr.driver.cardari.cardari.network.NetClient;
import kr.driver.cardari.cardari.tmap.TMapHelper;
import kr.driver.cardari.cardari.tmap.TMapRouteData;

import static kr.driver.cardari.cardari.common.Defines.EXTRA_KEY_CLIENT_POINT;
import static kr.driver.cardari.cardari.common.Defines.EXTRA_KEY_END_POINT;
import static kr.driver.cardari.cardari.common.Defines.EXTRA_KEY_START_POINT;
import static kr.driver.cardari.cardari.common.Defines.EXTRA_KEY_THROUGH_POINT_LIST;
import static kr.driver.cardari.cardari.common.Enums.MapViewType;

/**
 * Created by eklee on 2017. 11. 21..
 */

public class MapControlFragment extends Fragment implements OnMapReadyCallback {
    public interface EventListener {
        void onFullScreenModeChanged(boolean isFullScreen);
    }

    private static final String TAG = "MapControlFragment";

    @BindView(R.id.map_control_radio_group) RadioGroup mMapControlRadioGroup;
    @BindView(R.id.map_control_path_from_driver_to_client_point_radio_button) RadioButton mClientPointRadioButton;
    @BindView(R.id.map_control_start_point_radio_button) RadioButton mStartPointRadioButton;
    @BindView(R.id.map_control_end_point_radio_button) RadioButton mEndPointRadioButton;
    @BindView(R.id.map_control_path_from_start_point_to_end_point_radio_button) RadioButton mThroughPointRadioButton;
    @BindView(R.id.map_control_full_screen_button) Button mFullScreenButton;

    @BindView(R.id.loading_progressbar_layout) View mLoadingProgressbarLayout;

    private Unbinder mUnBinder;
    private EventListener mEventListener;

    private GoogleMap mGoogleMap;
    private Polyline mPolyline;
    private List<Marker> mMarkerList;
    private float mCurZoomLevel;

    private LatLng mClientPoint;
    private LatLng mStartPoint;
    private LatLng mEndPoint;
    private ArrayList<LatLng> mThroughPointList;

    private TMapRouteData mPathFromDriverToClient;
    private TMapRouteData mPathFromStartToEnd;

    public static MapControlFragment createInstance(LatLng clientPoint,
                                                    LatLng startPoint,
                                                    LatLng endPoint,
                                                    List<LatLng> throughPointList) {
        MapControlFragment fragment = new MapControlFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(EXTRA_KEY_CLIENT_POINT, clientPoint);
        bundle.putParcelable(EXTRA_KEY_START_POINT, startPoint);
        bundle.putParcelable(EXTRA_KEY_END_POINT, endPoint);
        bundle.putParcelableArrayList(EXTRA_KEY_THROUGH_POINT_LIST, throughPointList != null ? new ArrayList<Parcelable>(throughPointList) : null);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map_control, container, false);
        mUnBinder = ButterKnife.bind(this, rootView);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mClientPoint = bundle.getParcelable(EXTRA_KEY_CLIENT_POINT);
            mStartPoint = bundle.getParcelable(EXTRA_KEY_START_POINT);
            mEndPoint = bundle.getParcelable(EXTRA_KEY_END_POINT);
            mThroughPointList = bundle.getParcelableArrayList(EXTRA_KEY_THROUGH_POINT_LIST);
        }

        initializeView();

        initializeGoogleMap();

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            if (Build.VERSION.SDK_INT >= 23) {
                mEventListener = (EventListener) context;
            }
        } catch (Exception e) {
            DLog.e(TAG, "onAttach - " + context.toString() + " not implemented event listener", e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            if (Build.VERSION.SDK_INT < 23) {
                mEventListener = (EventListener) activity;
            }
        } catch (Exception e) {
            DLog.e(TAG, "onAttach - " + activity.toString() + " not implemented event listener", e);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mEventListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnBinder.unbind();
    }

    private void initializeView() {
        mMapControlRadioGroup.check(-1); // OnMapReady에서 기사와 고객과의 거리 보여주도록 check 호출하므로 여기서 -1로 초기화한다.
        mMapControlRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                try {
                    switch (checkedId) {
                        case R.id.map_control_path_from_driver_to_client_point_radio_button: {
                            changeMapViewType(MapViewType.PATH_FROM_DRIVER_TO_CLIENT);
                        }
                        break;
                        case R.id.map_control_start_point_radio_button: {
                            changeMapViewType(MapViewType.START_POINT);
                        }
                        break;
                        case R.id.map_control_end_point_radio_button: {
                            changeMapViewType(MapViewType.END_POINT);
                        }
                        break;
                        case R.id.map_control_path_from_start_point_to_end_point_radio_button: {
                            changeMapViewType(MapViewType.PATH_FROM_START_TO_END);
                        }
                        break;
                        default:
                            DLog.e(TAG, "onCheckedChanged - not implemented map view control radio button");
                            break;
                    }
                } catch (Exception e) {
                    DLog.e(TAG, "onCheckedChanged exception", e);
                }
            }
        });

        mFullScreenButton.setSelected(false);
    }

    private void initializeGoogleMap() {
        try {
            if (mGoogleMap == null) {
                SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.google_map_fragment);
                mapFragment.getMapAsync(this);
            }
        } catch (Exception e) {
            DLog.e(TAG, "initializeGoogleMap exception", e);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mMarkerList = new LinkedList<>();

        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions.width(15f); // default width 10
        mPolyline = mGoogleMap.addPolyline(polylineOptions);

        mCurZoomLevel = Defines.GOOGLE_MAP_DEFAULT_ZOOM;

        mGoogleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                mCurZoomLevel = mGoogleMap.getCameraPosition().zoom;
            }
        });


        // 제일 처음엔 내 위치로 표시하고
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MyApp.getInstance().getCurLocation(), mCurZoomLevel));

        // 기사와 고객과의 경로를 요청해서 보여준다
        changeMapViewType(MapViewType.PATH_FROM_DRIVER_TO_CLIENT);
    }

    @OnClick(R.id.map_control_full_screen_button)
    public void toggleFullScreen() {
        try {
            mFullScreenButton.setSelected(!mFullScreenButton.isSelected());
            mEventListener.onFullScreenModeChanged(mFullScreenButton.isSelected());
            mFullScreenButton.setText(mFullScreenButton.isSelected() ? R.string.minimize_map : R.string.maximize_map);
        } catch (Exception e) {
            DLog.e(TAG, "toggleFullScreen exception", e);
        }
    }

    public void showLoadingProgressbar() {
        try {
            mLoadingProgressbarLayout.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            DLog.e(TAG, "showLoadingProgressbar", e);
        }
    }

    public void hideLoadingProgressbar() {
        try {
            mLoadingProgressbarLayout.setVisibility(View.GONE);
        } catch (Exception e) {
            DLog.e(TAG, "hideLoadingProgressbar", e);
        }
    }

    private void updateGoogleMap(TMapRouteData tMapRouteData) {
        try {
            removeMarkers();

            List<LatLng> pointList = tMapRouteData.getPointList();

            if (pointList.size() >= 1) {
                mMarkerList.add(mGoogleMap.addMarker(new MarkerOptions().position(pointList.get(0))
//                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_custom_marker))
                ));
            }

            if (pointList.size() >= 2) {
                mMarkerList.add(mGoogleMap.addMarker(new MarkerOptions().position(pointList.get(pointList.size() - 1))
//                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_custom_marker))
                ));
            }

            mPolyline.setPoints(pointList);

            updateCameraBounds();

        } catch (Exception e) {
            DLog.e(TAG, "applyPathToMap exception", e);
        }
    }

    private void updateGoogleMap(LatLng point) {
        try {
            removeMarkers();

            mMarkerList.add(mGoogleMap.addMarker(new MarkerOptions().position(point)
//                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_custom_marker))
            ));

            List<LatLng> pointList = new ArrayList<>(1);
            pointList.add(point);
            mPolyline.setPoints(pointList);
        } catch (Exception e) {
            DLog.e(TAG, "applyPathToMap exception", e);
        }
    }

    private void removeMarkers() {
        try {
            for (Marker marker : mMarkerList) {
                marker.remove();
            }
        } catch (Exception e) {
            DLog.e(TAG, "removeMarkers exception", e);
        }
    }

    private void updateCameraBounds() {
        try {
            List<LatLng> pointList = mPolyline.getPoints();

            LatLngBounds.Builder builder = LatLngBounds.builder();

            // 시작점과 도착점 기준으로 바운드 계산
            // builder.include(pointList.get(0)).include(pointList.get(pointList.size() - 1));

            // 전체 경로상의 점을 기준으로 바운드 계산
            for (LatLng point : pointList) {
                builder.include(point);
            }

            int padding = 200;
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), padding));
        } catch (Exception e) {
            DLog.e(TAG, "updateCameraBounds exception", e);
        }
    }

    public void changeMapViewType(@MapViewType int mapViewType) {
        switch (mapViewType) {
            case MapViewType.PATH_FROM_DRIVER_TO_CLIENT: {
                if (mPathFromDriverToClient == null) {
                    showLoadingProgressbar();

                    RequestParams requestParams = TMapHelper.generateTMapRouteRequestParams(MyApp.getInstance().getCurLocation(), mClientPoint, null);
                    NetClient.postTMapRoute(getContext(), requestParams, new NetClient.TMapRouteResponseCallback() {
                        @Override
                        public void onResponse(TMapRouteData result) {
                            hideLoadingProgressbar();

                            if (result != null) {
                                mPathFromDriverToClient = result;
                                updateGoogleMap(mPathFromDriverToClient);
                            } else {
//                                DialogHelper.showAlertDialog(getContext(),
//                                    "알림",
//                                    "경로 조회에 실패했습니다. 다시 시도해주십시오.",
//                                    "확인",
//                                    null,
//                                    null);
                            }
                        }
                    });
                } else {
                    updateGoogleMap(mPathFromDriverToClient);
                }

                mMapControlRadioGroup.check(R.id.map_control_path_from_driver_to_client_point_radio_button);
            }
            break;
            case MapViewType.START_POINT: {
                updateGoogleMap(mStartPoint);
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mStartPoint, mCurZoomLevel));

                mMapControlRadioGroup.check(R.id.map_control_start_point_radio_button);
            }
            break;
            case MapViewType.END_POINT: {
                updateGoogleMap(mEndPoint);
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mEndPoint, mCurZoomLevel));

                mMapControlRadioGroup.check(R.id.map_control_end_point_radio_button);
            }
            break;
            case MapViewType.PATH_FROM_START_TO_END: {
                if (mPathFromStartToEnd == null) {
                    showLoadingProgressbar();

                    RequestParams requestParams = TMapHelper.generateTMapRouteRequestParams(mStartPoint, mEndPoint, mThroughPointList);
                    NetClient.postTMapRoute(getContext(), requestParams, new NetClient.TMapRouteResponseCallback() {
                        @Override
                        public void onResponse(TMapRouteData result) {
                            hideLoadingProgressbar();

                            if (result != null) {
                                mPathFromStartToEnd = result;
                                updateGoogleMap(mPathFromStartToEnd);
                            } else {
//                                DialogHelper.showAlertDialog(getContext(),
//                                    "알림",
//                                    "경로 조회에 실패했습니다. 다시 시도해주십시오.",
//                                    "확인",
//                                    null,
//                                    null);
                            }
                        }
                    });
                } else {
                    updateGoogleMap(mPathFromStartToEnd);
                }

                mMapControlRadioGroup.check(R.id.map_control_path_from_start_point_to_end_point_radio_button);
            }
            break;
            default:
                DLog.e(TAG, "changeMapViewType - not implemented map view type" + mapViewType);
        }
    }
}