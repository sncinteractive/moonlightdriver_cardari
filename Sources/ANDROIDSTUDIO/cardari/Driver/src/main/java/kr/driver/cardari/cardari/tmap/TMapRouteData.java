package kr.driver.cardari.cardari.tmap;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by eklee on 2017. 11. 2..
 */

public class TMapRouteData {
    private long mTotalDistance;
    private long mTotalTime;
    private List<LatLng> mPointList;

    public TMapRouteData() {
    }

    public TMapRouteData(long totalDistance, long totalTime, List<LatLng> pointList) {
        mTotalDistance = totalDistance;
        mTotalTime = totalTime;
        mPointList = pointList;
    }

    public double getTotalDistance() {
        return mTotalDistance;
    }

    public void setTotalDistance(long totalDistance) {
        mTotalDistance = totalDistance;
    }

    public double getTotalTime() {
        return mTotalTime;
    }

    public void setTotalTime(long totalTime) {
        mTotalTime = totalTime;
    }

    public List<LatLng> getPointList() {
        return mPointList;
    }

    public void setPointList(List<LatLng> pointList) {
        mPointList = pointList;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"total distance\"").append(":").append("\"").append(Long.toString(mTotalDistance)).append("\"");
        sb.append(",\"total time\"").append(":").append("\"").append(Long.toString(mTotalTime)).append("\"");

        if(mPointList != null && !mPointList.isEmpty()) {
            sb.append(",\"point list\"").append(":").append("[");
            for (LatLng point : mPointList) {
                sb.append("{").append("\"latitude\":").append(Double.toString(point.latitude))
                    .append(",\"longitude\":").append(Double.toString((point.longitude))).append("},");
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.append("]");
        }
        sb.append("}");


        return sb.toString();
    }
}
