package kr.driver.cardari.cardari.data;

/**
 * Created by youngmin on 2016-12-29.
 */
public class NoticeData {
    public String userNoticeId;
    public int seq;
    public String title;
    public String contents;
    public String imagePath;
    public String orgImagePath;
    public String linkUrl;
    public long startDate;
    public long endDate;

    public NoticeData() {    }

    public NoticeData(String userNoticeId, int seq, String title, String contents, String imagePath, String orgImagePath, String linkUrl, long startDate, long endDate) {
        this.userNoticeId = userNoticeId;
        this.seq = seq;
        this.title = title;
        this.contents = contents;
        this.imagePath = imagePath;
        this.orgImagePath = orgImagePath;
        this.linkUrl = linkUrl;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getUserNoticeId() {
        return userNoticeId;
    }

    public void setUserNoticeId(String userNoticeId) {
        this.userNoticeId = userNoticeId;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getOrgImagePath() {
        return orgImagePath;
    }

    public void setOrgImagePath(String orgImagePath) {
        this.orgImagePath = orgImagePath;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }
}
