package kr.driver.cardari.cardari.common;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;

import kr.driver.cardari.cardari.MyApp;

/**
 * Created by youngmin on 2016-09-08.
 */
public class SendLocationService extends Service {
    private static final String TAG = "SendLocationService";
    private boolean mIsCheckGpsUpdateTime;
    private Context mContext;
    private GpsInfo mGpsInfo;

    private String mUserId = "";
    private LatLng mLastUpdateLocation;
    private double mLastUpdateLocationTime = 0;

	private LocationCallback mLocationCallBack;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        try {
            mContext = this;

            if (intent == null) {
                mIsCheckGpsUpdateTime = true;
            } else {
                mIsCheckGpsUpdateTime = false;
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                mIsCheckGpsUpdateTime = true;
                            }
                        }, 30000);
            }

	        mLocationCallBack = new LocationCallback() {
		        @Override
		        public void onLocationResult(LocationResult locationResult) {
			        super.onLocationResult(locationResult);

			        if(locationResult != null) {
			        	for(Location location : locationResult.getLocations()) {
                            MyApp.getInstance().setCurLocation(new LatLng(location.getLatitude(), location.getLongitude()));
				        }
			        }
		        }
	        };

            initializeLocationManager();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return START_STICKY;
    }

    private void initializeLocationManager() {
        try {
            stopLocationUpdates();

            startLocationUpdates();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startLocationUpdates() {
        try {
            mGpsInfo = new GpsInfo(mContext, mLocationCallBack);
            mGpsInfo.connectGoogleApi();

            updateLastKnownLocation();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopLocationUpdates() {
        try {
            if (mGpsInfo != null) {
                if (mGpsInfo.isConnected()) {
                    mGpsInfo.stopLocationUpdates();
                    mGpsInfo.disconnectGoogleApi();
                }

                mGpsInfo = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateLastKnownLocation() {
        try {
            if (mGpsInfo != null) {
                mGpsInfo.getLastLocation(new OnSuccessListener<Location>() {
	                @Override
	                public void onSuccess(Location location) {
		                if(location != null) {
                            MyApp.getInstance().setCurLocation(new LatLng(location.getLatitude(), location.getLongitude()));
		                }
	                }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            stopLocationUpdates();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
