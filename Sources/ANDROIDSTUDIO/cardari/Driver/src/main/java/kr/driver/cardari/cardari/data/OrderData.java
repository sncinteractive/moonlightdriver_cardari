package kr.driver.cardari.cardari.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import kr.driver.cardari.cardari.common.Enums.OrderType;
import kr.driver.cardari.cardari.common.Enums.PaymentOption;

/**
 * Created by eklee on 2017. 11. 7..
 */

public class OrderData implements Parcelable {
    private String callId;
    private @OrderType int orderType;
    private boolean refused;
    private String clientGrade;
    private String uniqueId;
    private DriverData driver;
    private String userPhone;
    private CallPositionData clientLocation;
    private CallPositionData startLocation;
    private CallPositionData endLocation;
    private List<CallPositionData> throughLocationList;
    private @PaymentOption String paymentOption;
    private int money;
    private long createdTime;

    private double distance;

    // for auction order
    private int curMinBiddingPrice;
    private int biddablePrice;
    private long biddingEndTime;

    private String apiDrivingId;
    private String premiums;

    public OrderData() {
    }

    public OrderData(String callId,
                     @OrderType int orderType,
                     boolean refused,
                     String clientGrade,
                     String uniqueId,
                     DriverData driver,
                     String userPhone,
                     CallPositionData clientLocation,
                     CallPositionData startLocation,
                     CallPositionData endLocation,
                     List<CallPositionData> throughLocationList,
                     @PaymentOption String paymentOption,
                     int money,
                     long createdTime,
                     double distance,
                     int curMinBiddingPrice,
                     int biddablePrice,
                     long biddingEndTime,
                     String apiDrivingId,
                     String premiums) {
        this.callId = callId;
        this.orderType = orderType;
        this.refused = refused;
        this.clientGrade = clientGrade;
        this.uniqueId = uniqueId;
        this.driver = driver;
        this.userPhone = userPhone;
        this.clientLocation = clientLocation;
        this.startLocation = startLocation;
        this.endLocation = endLocation;
        this.throughLocationList = throughLocationList;
        this.paymentOption = paymentOption;
        this.money = money;
        this.createdTime = createdTime;
        this.distance = distance;
        this.curMinBiddingPrice = curMinBiddingPrice;
        this.biddablePrice = biddablePrice;
        this.biddingEndTime = biddingEndTime;
        this.apiDrivingId = apiDrivingId;
        this.premiums = premiums;
    }

    protected OrderData(Parcel in) {
        callId = in.readString();
        orderType = in.readInt();
        refused = in.readByte() != 0;
        clientGrade = in.readString();
        uniqueId = in.readString();
        driver = in.readParcelable(DriverData.class.getClassLoader());
        userPhone = in.readString();
        clientLocation = in.readParcelable(CallPositionData.class.getClassLoader());
        startLocation = in.readParcelable(CallPositionData.class.getClassLoader());
        endLocation = in.readParcelable(CallPositionData.class.getClassLoader());
        throughLocationList = in.createTypedArrayList(CallPositionData.CREATOR);
        paymentOption = in.readString();
        money = in.readInt();
        createdTime = in.readLong();
        distance = in.readDouble();
        curMinBiddingPrice = in.readInt();
        biddablePrice = in.readInt();
        biddingEndTime = in.readLong();
        apiDrivingId = in.readString();
        premiums = in.readString();
    }

    public static final Creator<OrderData> CREATOR = new Creator<OrderData>() {
        @Override
        public OrderData createFromParcel(Parcel in) {
            return new OrderData(in);
        }

        @Override
        public OrderData[] newArray(int size) {
            return new OrderData[size];
        }
    };

    public String getCallId() {
        return callId;
    }

    public void setCallId(String callId) {
        this.callId = callId;
    }

    public @OrderType int getOrderType() {
        return orderType;
    }

    public void setOrderType(@OrderType int orderType) {
        this.orderType = orderType;
    }

    public boolean isRefused() {
        return refused;
    }

    public void setRefused(boolean refused) {
        this.refused = refused;
    }

    public String getClientGrade() {
        return clientGrade;
    }

    public void setClientGrade(String clientGrade) {
        this.clientGrade = clientGrade;
    }

    public CallPositionData getClientLocation() {
        return clientLocation;
    }

    public void setClientLocation(CallPositionData clientLocation) {
        this.clientLocation = clientLocation;
    }

    public CallPositionData getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(CallPositionData startLocation) {
        this.startLocation = startLocation;
    }

    public CallPositionData getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(CallPositionData endLocation) {
        this.endLocation = endLocation;
    }

    public List<CallPositionData> getThroughLocationList() {
        return throughLocationList;
    }

    public List<LatLng> getThroughLocationLatLngList() {
        if (throughLocationList != null) {
            List<LatLng> result = new ArrayList<>(throughLocationList.size());
            for (CallPositionData callPositionData : throughLocationList) {
                if(callPositionData.getLocation().coordinates.size() > 0) {
                    result.add(callPositionData.getLocation().getCoordinatesLatLng());
                }
            }

            if(result.size() > 0) {
                return result;
            }
        }

        return null;
    }

    public void setThroughLocationList(List<CallPositionData> throughLocationList) {
        this.throughLocationList = throughLocationList;
    }

    public @PaymentOption
    String getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(@PaymentOption String paymentOption) {
        this.paymentOption = paymentOption;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public int getCurMinBiddingPrice() {
        return curMinBiddingPrice;
    }

    public void setCurMinBiddingPrice(int curMinBiddingPrice) {
        this.curMinBiddingPrice = curMinBiddingPrice;
    }

    public int getBiddablePrice() {
        return biddablePrice;
    }

    public void setBiddablePrice(int biddablePrice) {
        this.biddablePrice = biddablePrice;
    }

    public long getBiddingEndTime() {
        return biddingEndTime;
    }

    public void setBiddingEndTime(long biddingEndTime) {
        this.biddingEndTime = biddingEndTime;
    }

    public DriverData getDriver() {
        return driver;
    }

    public void setDriver(DriverData driver) {
        this.driver = driver;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getApiDrivingId() {
        return apiDrivingId;
    }

    public void setApiDrivingId(String apiDrivingId) {
        this.apiDrivingId = apiDrivingId;
    }

    public String getPremiums() {
        return premiums;
    }

    public void setPremiums(String premiums) {
        this.premiums = premiums;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(callId);
        dest.writeInt(orderType);
        dest.writeByte((byte) (refused ? 1 : 0));
        dest.writeString(clientGrade);
        dest.writeString(uniqueId);
        dest.writeParcelable(driver, flags);
        dest.writeString(userPhone);
        dest.writeParcelable(clientLocation, flags);
        dest.writeParcelable(startLocation, flags);
        dest.writeParcelable(endLocation, flags);
        dest.writeList(throughLocationList);
        dest.writeString(paymentOption);
        dest.writeInt(money);
        dest.writeDouble(distance);
        dest.writeInt(curMinBiddingPrice);
        dest.writeInt(biddablePrice);
        dest.writeLong(biddingEndTime);
        dest.writeString(apiDrivingId);
        dest.writeString(premiums);
    }
}
