package kr.driver.cardari.cardari.logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.LinkedBlockingQueue;

import static kr.driver.cardari.cardari.logger.DLog.LogType.Debug;

/**
 * Created by eklee on 2017. 10. 17..
 */

public class WriteLogThread extends Thread {
    private static final String TAG = "WriteLogThread";

    private LinkedBlockingQueue<String> mLogQueue;
    private String mRootDirPath;
    private String mFilePath;
    private boolean mRun;
    private static final int MAX_LOG_FILE_SIZE = 1024 * 1024 * 10; // 1mb

    public WriteLogThread(LinkedBlockingQueue<String> logQueue, String rootDirPath) {
        super(TAG);
        mLogQueue = logQueue;
        mRootDirPath = rootDirPath;
        mFilePath = "";
        mRun = true;
        DLog.adbLog(Debug, TAG, "constructor. root dir path = " + rootDirPath);
    }

    public void terminate() {
        mRun = false;
        DLog.adbLog(Debug, TAG, "terminate");
    }

    @Override
    public void run() {
        DLog.adbLog(Debug, TAG, "run");
        try {
            while (mRun || mLogQueue.size() != 0) {
                try {
                    String log = mLogQueue.take();
                    if (log != null) {
                        boolean changeFile = true;
                        File file = new File(mFilePath);
                        if (file.exists()) {
                            if (file.length() < MAX_LOG_FILE_SIZE) {
                                changeFile = false;
                            }
                        }

                        if (changeFile) {
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSSS", Locale.getDefault());
                            mFilePath = mRootDirPath + File.separator + simpleDateFormat.format(new Date()) + ".txt";
                            file = new File(mFilePath);
                        }

                        FileWriter fr = null;
                        BufferedWriter bw = null;
                        try {
                            fr = new FileWriter(file, true);
                            bw = new BufferedWriter(fr);
                            bw.append(log);
                            bw.newLine();
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            if (bw != null) {
                                try {
                                    bw.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                            if (fr != null) {
                                try {
                                    fr.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Thread.sleep(1);
            }
        } catch (InterruptedException e) {
            //ignore
        }

        DLog.adbLog(Debug, TAG, "end");
    }
}
