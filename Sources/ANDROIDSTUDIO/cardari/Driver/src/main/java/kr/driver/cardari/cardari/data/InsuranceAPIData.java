package kr.driver.cardari.cardari.data;

public class InsuranceAPIData {
	private String Message;
	private String return_num;
	private String biz_driving_id;
	private String api_driving_id;
	private String premiums;

	public InsuranceAPIData() {
	}

	public InsuranceAPIData(String message, String return_num, String biz_driving_id, String api_driving_id, String premiums) {
		Message = message;
		this.return_num = return_num;
		this.biz_driving_id = biz_driving_id;
		this.api_driving_id = api_driving_id;
		this.premiums = premiums;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public String getReturn_num() {
		return return_num;
	}

	public void setReturn_num(String return_num) {
		this.return_num = return_num;
	}

	public String getBiz_driving_id() {
		return biz_driving_id;
	}

	public void setBiz_driving_id(String biz_driving_id) {
		this.biz_driving_id = biz_driving_id;
	}

	public String getApi_driving_id() {
		return api_driving_id;
	}

	public void setApi_driving_id(String api_driving_id) {
		this.api_driving_id = api_driving_id;
	}

	public String getPremiums() {
		return premiums;
	}

	public void setPremiums(String premiums) {
		this.premiums = premiums;
	}
}
