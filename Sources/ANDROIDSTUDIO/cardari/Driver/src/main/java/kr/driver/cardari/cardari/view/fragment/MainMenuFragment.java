package kr.driver.cardari.cardari.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.Util;
import kr.driver.cardari.cardari.logger.DLog;

import static kr.driver.cardari.cardari.common.Enums.MainMenuType;

/**
 * Created by eklee on 2017. 11. 21..
 */

public class MainMenuFragment extends Fragment {
    public interface EventListener {
        void onClickMainMenu(@MainMenuType int mainMenuType);
    }

    private static final String TAG = "MainMenuFragment";

    @BindView(R.id.driver_balance_tv) TextView mDriverBalanceTV;

    private Unbinder mUnbinder;
    private EventListener mEventListener;

    public MainMenuFragment() {
    }

    public static MainMenuFragment createInstance() {
        return new MainMenuFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = null;
        try {
            rootView = inflater.inflate(R.layout.fragment_main_menu, container, false);
            mUnbinder = ButterKnife.bind(this, rootView);
        } catch (Exception e) {
            DLog.e(TAG, "onCreateView exception", e);
        }

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            if (Build.VERSION.SDK_INT >= 23) {
                mEventListener = (EventListener) context;
            }
        } catch (Exception e) {
            DLog.e(TAG, "onAttach - " + context.toString() + " not implemented event listener", e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            if (Build.VERSION.SDK_INT < 23) {
                mEventListener = (EventListener) activity;
            }
        } catch (Exception e) {
            DLog.e(TAG, "onAttach - " + activity.toString() + " not implemented event listener", e);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mEventListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @OnClick({
        R.id.quick_setting_button,
        R.id.setting_button,
        R.id.settlement_button})
    public void onClickMainMenu(View view) {
        try {
            switch (view.getId()) {
                case R.id.quick_setting_button: {
                    mEventListener.onClickMainMenu(MainMenuType.QUICK_SETTING);
                }
                break;
                case R.id.setting_button: {
                    mEventListener.onClickMainMenu(MainMenuType.SETTING);
                }
                break;
                case R.id.settlement_button: {
                    mEventListener.onClickMainMenu(MainMenuType.SETTLEMENT);
                }
                break;
                default:
                    throw new RuntimeException("OnClickMainMenu - not implemented main menu button");
            }
        } catch (Exception e) {
            DLog.e(TAG, "OnClickTab exception", e);
        }
    }

    public void updateDriverBalance(int balance) {
        try {
            if(mDriverBalanceTV != null) {
                mDriverBalanceTV.setText(Util.convertPriceToThousandString(balance));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
