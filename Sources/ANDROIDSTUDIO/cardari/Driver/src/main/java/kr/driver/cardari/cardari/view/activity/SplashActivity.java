package kr.driver.cardari.cardari.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import kr.driver.cardari.cardari.MyApp;
import kr.driver.cardari.cardari.R;

import static kr.driver.cardari.cardari.common.Enums.DriverState;

public class SplashActivity extends AppCompatActivity {
    private static final String TAG = "SplashActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_splash);

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            // 로그인 연동?
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent;
                    if (MyApp.getInstance().getDriverData().getmDriverCallInfoState() == DriverState.IDLE) {
                        intent = new Intent(SplashActivity.this, OrderListActivity.class);
                    } else {
                        intent = new Intent(SplashActivity.this, AllocationActivity.class);
                    }
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                }
            }, 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
