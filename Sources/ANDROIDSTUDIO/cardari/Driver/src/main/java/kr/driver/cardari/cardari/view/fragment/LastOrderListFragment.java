package kr.driver.cardari.cardari.view.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import kr.driver.cardari.cardari.MyApp;
import kr.driver.cardari.cardari.R;
import kr.driver.cardari.cardari.common.DialogHelper;
import kr.driver.cardari.cardari.common.Enums;
import kr.driver.cardari.cardari.common.Util;
import kr.driver.cardari.cardari.data.LastOrderData;
import kr.driver.cardari.cardari.logger.DLog;
import kr.driver.cardari.cardari.network.NetClient;
import kr.driver.cardari.cardari.network.NetResponseCallback;
import kr.driver.cardari.cardari.network.ack.BaseAck;
import kr.driver.cardari.cardari.network.ack.LastOrderListAck;
import kr.driver.cardari.cardari.view.adapter.LastOrderListAdapter;

/**
 * Created by eklee on 2017. 11. 21..
 */

public class LastOrderListFragment extends Fragment {
    public interface EventListener {
        void needToShowLoadingProgressbar();

        void needToHideLoadingProgressbar();
    }

    private static final String TAG = "LastOrderListFragment";

    @BindView(R.id.date_from_button) Button mDateFromButton;
    @BindView(R.id.date_to_button) Button mDateToButton;
    @BindView(R.id.last_order_list_recycler_view) RecyclerView mLastOrderListRecyclerView;
    @BindView(R.id.office_name_text_view) TextView mOfficeNameTextView;
    @BindView(R.id.phone_number_text_view) TextView mPhoneNumberTextView;


    private Unbinder mUnBinder;
    private EventListener mEventListener;

    private LastOrderListAdapter mLastOrderListAdapter;

    private Calendar mCalendarFrom;
    private Calendar mCalendarTo;

    private GestureDetector mGestureDetector;

    private int mPage;

    public static LastOrderListFragment createInstance() {
        return new LastOrderListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_last_order_list, container, false);
        mUnBinder = ButterKnife.bind(this, rootView);

        initialize();
        initializeView();

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            if (Build.VERSION.SDK_INT >= 23) {
                mEventListener = (EventListener) context;
            }
        } catch (Exception e) {
            DLog.e(TAG, "onAttach - " + context.toString() + " not implemented event listener", e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            if (Build.VERSION.SDK_INT < 23) {
                mEventListener = (EventListener) activity;
            }
        } catch (Exception e) {
            DLog.e(TAG, "onAttach - " + activity.toString() + " not implemented event listener", e);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mEventListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnBinder.unbind();
    }

    private void initialize() {
        mPage = 0;

        mCalendarTo = Calendar.getInstance();
        mCalendarTo.setTime(new Date()); // 현재 시간으로 초기화

        mCalendarFrom = Calendar.getInstance();
        mCalendarFrom.setTime(mCalendarTo.getTime());
        mCalendarFrom.add(Calendar.DAY_OF_MONTH, -7); // 1주일 전으로 초기화

        mGestureDetector = createGestureDetector();

        mLastOrderListAdapter = new LastOrderListAdapter();
        mLastOrderListRecyclerView.setAdapter(mLastOrderListAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mLastOrderListRecyclerView.setLayoutManager(layoutManager);
        mLastOrderListRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                View childView = rv.findChildViewUnder(e.getX(), e.getY());

                if (childView != null && mGestureDetector.onTouchEvent(e)) {
                    int position = rv.getChildAdapterPosition(childView);

                    LastOrderData orderData = mLastOrderListAdapter.getItem(position);

                    mOfficeNameTextView.setText(orderData.getCenterName());
                    mPhoneNumberTextView.setText(orderData.getCenterPhoneNumber());

                    return true;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    private GestureDetector createGestureDetector() {
        return new GestureDetector(getContext(), new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(MotionEvent e) {
                return false;
            }

            @Override
            public void onShowPress(MotionEvent e) {

            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                return false;
            }

            @Override
            public void onLongPress(MotionEvent e) {

            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                return false;
            }
        });
    }

    private void initializeView() {
        applyDateFromDataToUI();
        applyDateToDataToUI();
        mOfficeNameTextView.setText(null);
        mPhoneNumberTextView.setText(null);
    }

    private void applyDateFromDataToUI() {
        mDateFromButton.setText(Util.convertYearMonthDayString(mCalendarFrom));
    }

    private void applyDateToDataToUI() {
        mDateToButton.setText(Util.convertYearMonthDayString(mCalendarTo));
    }

    public void needToShowLoadingProgressbar() {
        try {
            mEventListener.needToShowLoadingProgressbar();
        } catch (Exception e) {
            DLog.e(TAG, "needToShowLoadingProgressbar exception", e);
        }
    }

    public void needToHideLoadingProgressbar() {
        try {
            mEventListener.needToHideLoadingProgressbar();
        } catch (Exception e) {
            DLog.e(TAG, "needToHideLoadingProgressbar exception", e);
        }
    }

    @OnClick(R.id.date_from_button)
    public void onClickDateFrom() {
        DialogHelper.showDatePickerDialog(getContext(), mCalendarFrom, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mCalendarFrom.set(year, month, dayOfMonth);
                applyDateFromDataToUI();
            }
        });
    }

    @OnClick(R.id.date_to_button)
    public void onClickDateTo() {
        DialogHelper.showDatePickerDialog(getContext(), mCalendarTo, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mCalendarTo.set(year, month, dayOfMonth);
                applyDateToDataToUI();
            }
        });
    }

    @OnClick(R.id.query_button)
    public void onClickQuery() {
        requestLastOrderList();
    }

    private void requestLastOrderList() {
        RequestParams requestParams = new RequestParams();
        requestParams.put("driverId", MyApp.getInstance().getDriverData().getDriverId());
        requestParams.put("from", mDateFromButton.getText().toString());
        requestParams.put("to", mDateToButton.getText().toString());
        requestParams.put("page", mPage);
        NetClient.post(getContext(), NetClient.URL_LAST_ORDER_LIST, LastOrderListAck.class, requestParams, new NetResponseCallback(new NetResponseCallback.NetResponseListener() {
            @Override
            public void onResponse(BaseAck result) {
                processLastOrderListResponse(result);
            }
        }));

        // test code begin
        List<LastOrderData> dataList = new ArrayList<>();
//        dataList.add(new LastOrderData(mCalendarFrom.getTimeInMillis(), 10000, "강남역", "서울역", "일반", "상황실1", "010-1234-5678"));
//        dataList.add(new LastOrderData(mCalendarFrom.getTimeInMillis(), 15000, "서울역", "서현역", "경매", "상황실2", "010-1234-5678"));
//        dataList.add(new LastOrderData(mCalendarFrom.getTimeInMillis(), 10000, "강남역", "삼성역", "경매", "상황실3", "010-1234-5678"));
//        dataList.add(new LastOrderData(mCalendarFrom.getTimeInMillis(), 20000, "코엑스", "양재역", "일반", "상황실4", "010-1234-5678"));
//        dataList.add(new LastOrderData(mCalendarFrom.getTimeInMillis(), 25000, "서현역", "양재역", "일반", "상황실5", "010-1234-5678"));
//        dataList.add(new LastOrderData(mCalendarFrom.getTimeInMillis(), 20000, "코엑스", "양재역", "일반", "상황실6", "010-1234-5678"));
//        dataList.add(new LastOrderData(mCalendarFrom.getTimeInMillis(), 10000, "코엑스", "양재역", "일반", "상황실7", "010-1234-5678"));
//        dataList.add(new LastOrderData(mCalendarFrom.getTimeInMillis(), 15000, "수원역", "양재역", "일반", "상황실8", "010-1234-5678"));
//        dataList.add(new LastOrderData(mCalendarFrom.getTimeInMillis(), 25000, "강남역", "서울역", "일반", "상황실9", "010-1234-5678"));
//        dataList.add(new LastOrderData(mCalendarFrom.getTimeInMillis(), 20000, "코엑스", "양재역", "일반", "상황실1", "010-1234-5678"));
//        dataList.add(new LastOrderData(mCalendarFrom.getTimeInMillis(), 35000, "코엑스", "양재역", "일반", "상황실2", "010-1234-5678"));
//        dataList.add(new LastOrderData(mCalendarFrom.getTimeInMillis(), 25000, "서울역", "수원역", "경매", "상황실3", "010-1234-5678"));
//        dataList.add(new LastOrderData(mCalendarFrom.getTimeInMillis(), 12000, "코엑스", "양재역", "일반", "상황실4", "010-1234-5678"));
//        dataList.add(new LastOrderData(mCalendarFrom.getTimeInMillis(), 10000, "코엑스", "양재역", "일반", "상황실5", "010-1234-5678"));
//        dataList.add(new LastOrderData(mCalendarFrom.getTimeInMillis(), 40000, "코엑스", "양재역", "일반", "상황실6", "010-1234-5678"));
//        processLastOrderListResponse(new LastOrderListAck(new NetResultData(Enums.NetResultCode.SUCCESS, null), dataList));
        // test code end
    }

    private void processLastOrderListResponse(BaseAck result) {
        if (result != null && result.getResult().getCode() == Enums.NetResultCode.SUCCESS) {
            LastOrderListAck ack = (LastOrderListAck) result;

            mLastOrderListAdapter.setDataList(ack.getLastOrderDataList());
            mLastOrderListRecyclerView.scrollToPosition(0);
        } else {
            DialogHelper.showAlertDialog(getContext(), "알림", "입출금 내역 조회 요청이 실패했습니다.\n" + "code: " + result.getResult().getCode() + "detail: " + result.getResult().getDetail(), "확인", null, null);
        }
    }

    @OnClick(R.id.call_office_button)
    public void onClickCallOffice() {
        try {
            Util.makePhoneCall(getContext(), (String) mPhoneNumberTextView.getText());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}